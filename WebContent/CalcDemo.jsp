<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LoanGali : Calculators Demo</title>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		function round(value, decimals) {
		    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
		}
		
		
		$("#calcLoanEligibility").click(function(){
			var income = $("#lincome").val();
			var commitments = $("#lcommitments").val();
			var tenure = $("#ltenure").val();
			$.ajax({
				type : "GET",
				url : "/investhunt/investhunt/calc/loanEligibilityAmount/"+"HOME_LOAN/"+income+"/"+commitments+"/"+tenure,
				contentType : 'application/json',
				dataType : "text",
				success : function(eligi) {
				//alert(eligi);
					$("#loanEligible").append(eligi);
				},
				error : function(xhr) {
					alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "
							+ xhr.status);
				}
			});
		});
		
		
		$("#calcEmiEligibility").click(function(){
			var income = $("#income").val();
			var commitments = $("#commitments").val();
			$.ajax({
				type : "GET",
				url : "/investhunt/investhunt/calc/emiEligibility/"+"HOME_LOAN/"+income+"/"+commitments,
				contentType : 'application/json',
				dataType : "text",
				success : function(eligi) {
				//alert(eligi);
					$("#emiEligible").append(round(eligi,4));
				},
				error : function(xhr) {
					alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "
							+ xhr.status);
				}
			});
		});
		
		$("#calcEmi").click(function(){
			
			var loanAmount = $("#loanAmount").val();
			var roi = $("#roi").val();
			var tenure = $("#tenure").val();
			//alert(loanAmount+"-"+roi+"-"+tenure);
			
			var interestPerMonth = roi/100;
			var IPerMonthPerLakh = interestPerMonth * 100000;
			
			var ratio = (Math.pow((1+interestPerMonth), tenure))/(Math.pow((1+interestPerMonth), tenure)-1);
			
			var emi = ratio * IPerMonthPerLakh * loanAmount/100000;
			var totalPayable = emi * tenure;
			var interestPayable = totalPayable - loanAmount;
			
			$("#emi").html("");
			$("#interestPayable").html("");
			$("#totalPayable").html("");
			
			$("#emi").append(round(emi,2));
			$("#interestPayable").append(round(interestPayable,2));
			$("#totalPayable").append(round(totalPayable,2));
			
		});
		
		$("#calcAmort").click(function(){
		
			
		});
		
	});
</script>
</head>
<body>
	</br>
	<table width='90%'>
		<tr>
		<tr>
			<th colspace='4'></th>
			<h4>Emi Eligibility</h4>
		</tr>
		</tr>
		<tr>
			<td align='right'>Gross Monthly Salary : <input type='text'
				id='income' value='' />Rs
			</td>
			<td align='right'>Monthly Commitments : <input type='text'
				id='commitments' value='' />Rs
			</td>
			<td align='right'><button id='calcEmiEligibility'>Calc
					Emi Eligibilty</button></td>
		</tr>
		<tr height="30">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr height="30">
			<td align='right' colspace='3'>Emi Eligible Per Month : Rs <b
				id='emiEligible'></b></td>
		</tr>
	</table>
	</br>
	</br>
	<table width='90%'>
		<tr>
		<tr>
			<th colspace='4'></th>
			<h4>Loan Eligibility</h4>
		</tr>
		</tr>
		<tr>
			<td align='right'>Gross Monthly Salary : <input type='text'
				id='lincome' value='' />Rs
			</td>
			<td align='right'>Monthly Commitments : <input type='text'
				id='lcommitments' value='' />Rs
			</td>
			<td align='right'>Tenure : <input type='text' id='ltenure'
				value='' /></td>
			<td align='right'><button id='calcLoanEligibility'>Calc
					Loan Eligibilty</button></td>
		</tr>
		<tr height="30">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr height="30">
			<td align='right' colspace='3'>Your Loan Eligibility : Rs <b
				id='loanEligible'></b></td>
		</tr>
	</table>
	</br>
	</br>
	<table width='90%'>
		<tr>
		<tr>
			<th colspace='4'></th>
			<h4>Interest Payable</h4>
		</tr>
		</tr>
		<tr>
			<td align='right'>Loan : <input type='text' id='loanAmount'
				value='' />Rs
			</td>
			<td align='right'>ROI : <input type='text' id='roi' value='' />%
			</td>
			<td align='right'>Tenure : <input type='text' id='tenure'
				value='' />months
			</td>
			<td align='right'><button id='calcEmi'>Calc Interest
					Payable</button></td>
		</tr>
		<tr height="30">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr height="30">
			<td align='right'>Emi Per Month : Rs <b id='emi'></b></td>
			<td align='right'>Total Interest Payable : Rs <b
				id='interestPayable'></b></td>
			<td align='right'>Total Amount Payable : Rs <b id='totalPayable'></b></td>
		</tr>
	</table>
	</br>
	</br>

</body>
</html>