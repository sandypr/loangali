
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				FAQs<br> <span>Check for the most Frequently Asked
					Questions on Personal Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>1. For what purposes can I seek a personal loan?</h2>
				<p>Personal loans are usually preferred to serve an urgent need
					(like health emergency), functions at home (marriages, renovation
					of furniture), pleasure (foreign trips), buy white goods or
					jewellery etc.</p>
				<p>Personal loans are advisable instead of taking loans from
					Baniyas and funds who charge exorbitant interests and put undue
					pressure for repayment. Banks on the other hand ensure smooth
					repayments and charge lesser compared to them.</p>

				<h2>2. How will your bank decide your personal loan
					eligibility?</h2>
				<p>Personal loan eligibility is based on your current salary,
					and financial commitments. While some banks do consider the income
					of other members in the family, personal loans are largely limited
					to the borrower's income only</p>

				<h2>3. Can I take insurance for personal loan?</h2>
				<p>Yes, you can. In fact we strongly recommend insurance for
					personal loans since its a loan that is more of an expenditure than
					an investment. To avoid burdening the family in case of death its
					advised to take insurance on personal loans. Most banks have it as
					a compulsory norm; if not make sure you ask for it.</p>

				<h2>4. Why do banks ask for guarantors when I give my salary
					certificate?</h2>
				<p>Personal loans are largely based on the trust of repayment
					and banks request for guarantors to ensure at least 2 parties vouch
					for the trust in you to repay. In case of non repayment of loans,
					the guarantors are liable to repay the loan on your behalf.</p>

				<h2>5. Do I get tax exemption for availing a personal loan for
					house construction?</h2>
				<p>No, personal loans do not have tax exemption irrespective of
					the purpose of usage. Its advised to pursure house loan and
					education loan respectively for construction and education purposed
					as in those cases the interest rates are lesser and also there is a
					scope for tax exemption.</p>
				<p>However note that housing and education loans take some time
					and hence should be planned well ahead of the requirement.</p>

				<h2>6. I started working 3 months back. Can I take a personal
					loan?</h2>
				<p>Yes, you can. There are some banks which will offer personal
					loan from the third month of employment. You can get the rules for
					personal loans of various banks here. Go through the rules
					carefully before making a decision. But we think that's a little
					too early to take a loan.</p>
				<p class="applyLoanBlueBtn"><a href="/web/personalLoan/">Click here for Instant loan !</a></p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$("#headingFaqs").addClass('activeSubSubNav');
	</script>
		