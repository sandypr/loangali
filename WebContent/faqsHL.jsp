	
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				FAQs<br> <span>Check for the most Frequently Asked
					Questions on Home Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>1. For what purposes can I seek a first time home loan?</h2>
				<p>You can take home loan first time for either purchasing a
					new/old house/flat, for construction of a new house, rennovation/
					extension of an existing house.</p>

				<h2>2. How will your bank decide your home loan eligibility?</h2>
				<p>Bank will decide your loan eligibility based on your existing
					salary, current commitments, age and type of employment.</p>

				<h2>3. What is an EMI?</h2>
				<p>EMI stands for Equated Monthly Installment. It denotes the
					amount you should pay to the bank every month to ensure your loan
					gets repaid on time.</p>

				<h2>4. What documents are generally sought for a loan approval?</h2>
				<p>Documents sought in general are: Salary certificates, Form
					16, Identity and age proofs along with documents about the proposed
					property.</p>

				<h2>5. What are the different interest rate options offered by
					banks?</h2>
				<p>Banks offer fixed, floating and fixed+floating interest
					rates. Fixed interest rates largely remain unvaried during the loan
					period; while floating interest rates every year based on the
					regulations of RBI on interest rates. In case of fixed+floating the
					interest rates remain fixed for a given period after which they
					become floating.</p>

				<h2>6. What is an amortization schedule?</h2>
				<p>Ammortization schedule gives a month wise chart of how the
					EMI is distributed among principal and interest every month. It
					describes by how much the principal reduces every month and the
					tentative month of loan closure if you chose to increase your
					monthly EMI.</p>
					<p class="applyLoanBlueBtn"><a href="/web/homeLoan/">Click here for Instant loan !</a></p>
			</div>
		</div>
		
	</div>
	<script type="text/javascript">
		$("#headingFaqs").addClass('activeSubSubNav');
	</script>
		