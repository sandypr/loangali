
<!-- add header -->
<%@ include file="header.html"%>
<%@ include file="errorPopup.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihFeedBack.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon">
				<span>makes you smile</span> with unique features
			</div>
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock homeLoansIcon">Home Loan</div>
			<div id='subsublevelnavblock'><%@ include
					file="subsublevelnavblock.html"%></div>
		</div>
	</div>
	
	<div class="checkListPage">
		<div class="wrapper">
		<div class="stepThreeThanksBlock" id="thanku" style="display: none">
					<h3>Thanks for writing to us!</h3>
					<span>We value your feedback and suggestions/questions would
						be responded to at the earliest</span>
					<h3>1800-XXX-XXXX</h3>
				</div>
				 <div id="progress_bar" class="ui-progress-bar ui-container">
                	<div class="ui-progress" style="width: 49%;">
                		<span class="ui-label" style="display:none;">Processing <b class="value">79%</b></span>
                	</div>
                </div>
		<form id="feedbackFrm" action="/web/submitFeedback"
				method="post">
			<h1 class="pageHeading" style='padding-top: 315px;'>
				Feedback<br> <span>We value your feedback. Do write to us!</span>
			</h1>

			<!-- <div class="feedbackBlock">
				<p>"Everyone I met or spoke with was wonderful. I give Apex my
					highest recommendation to anyone seeking mortgage services. Your
					company is the exception, not the rule. A large number of US
					businesses would learn a lot from your professionalism, client
					management, and overall level of services"</p>

				<div class="feedBackButtons">
					<span class="feedBackName"> Timothy.M <br>Bethesda, MD
					</span> <span class="facebookPostComments"> <a href="#"
						class="facebookLike"><img
							src="${pageContext.request.contextPath}/images/facebookLikeBtn.png"
							alt=""></a> <a href="#" class="postCommentsBtn">Post your
							comments</a>
					</span>
				</div>
			</div> -->
			
				<div class="feedBackForm" id="feedbackForm">

					<ul>
						<li><label for="name"><span>Name</span></label> <input id="name"
							type="text" value="" class="textbox feedBackFormTextBox" name="name">
						</li>
						<li><label for="email"><span>Email ID</span></label> <input id="email"
							type="email" value="" class="textbox feedBackFormTextBox" name="email">
						</li>
						<li><label for="company">Company Name</label> <input
							id="company" type="text" value=""
							class="textbox feedBackFormTextBox ignore" name="company"></li>
						<li><label for="designation">Designation</label> <input
							id="designation" type="text" value=""
							class="textbox feedBackFormTextBox ignore" name="designation"></li>
						<li class="fullWidthList"><label for="comments"><span>Comments</span></label>
							<textarea id="comments" name="comments"
								class="textarea feedBackFormTextBox" id="" cols="30" rows="10"></textarea>
						</li>
						<li class="fullWidthList"><input type="button"
							class="submitButton" value="Submit" id="submitbtn" name="submitbtn"/></li>
					</ul>
				</div>
				
				<script>
			$(document).ready(function(){
				$('#progress_bar').hide();
				$.validator.setDefaults({
					
				});
				 $("#feedbackFrm").validate({
					rules: {
						comments: "required",
						name: "required",
						email: "required",
					  },
					  
					  errorPlacement: function(error, element) {
							// Append error within linked label
							$("span", element.parent()).addClass("error_show");
							element.attr("title",error.text());
					  },
					  
					  highlight: function(element) {
						  $("span", $(element).parent()).addClass("error_show");
					  },
					  
					  unhighlight: function(element) {
							 $("span", $(element).parent()).removeClass("error_show");
							 $(element).removeAttr("title");
					  },
					  
					  errorElement: "span",
						messages: {
							comments: "Comments is Mandatory",
							name:  "Name is Mandatory",
							email: {
					             required: "Email is Mandatory",
					             email: "Please Enter a valid Email address"
							},
						},
				});
				$('#submitbtn').click(function(e){
					 
					 if($("#feedbackFrm").valid()){
						//$("#feedbackFrm").hide();
						$.ajax({
							type : "POST",
							url : $('#feedbackFrm').attr('action'),
							data : JSON.stringify($('#feedbackFrm').serializeObject()),
							contentType : 'application/json',
							dataType : "text",
							success : function() {
								showPopUp("Information","Thanks for writing to us! We value your feedback and suggestions/questions would be responded to at the earliest");
								
							},
							error : function(xhr) {
								if($('#shadow').hasClass('blocker'))
								       $('#shadow').removeClass('blocker');
								redirect2Error(xhr.status);
								//alert("Oops !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
							}
						  });
								  
						$("#feedbackFrm :input").prop("disabled", true);
						$('#progress_bar').show();
						 $('#progress_bar .ui-progress').css('width', '4%').
					        animateProgress(43, function() {
					        	
					          $(this).animateProgress(79, function() {
					            setTimeout(function() {
					            $('#progress_bar .ui-progress').animateProgress(100, function() {
						             $('#progress_bar').hide();
					                $('#main_content').slideDown();
					                //$("#feedbackFrm").show();
					                $("#feedbackFrm :input").prop("disabled", false);
									$(this).closest('form').find("input[type=text], textarea").val("");
									$("#feedbackFrm").trigger('reset');
									
					              });
					            }, 2000);
					          });
					      });
					      /* $.ajax({
							type : "POST",
							url : $('#feedbackFrm').attr('action'),
							data : JSON.stringify($('#feedbackFrm').serializeObject()),
							contentType : 'application/json',
							dataType : "text",
							success : function() {
								showPopUp("Information","Thanks for writing to us! We value your feedback and suggestions/questions would be responded to at the earliest");
								$("#feedbackFrm").show();
								$(this).closest('form').find("input[type=text], textarea").val("");
								$("#feedbackFrm").trigger('reset');
							},
							error : function(xhr) {
								if($('#shadow').hasClass('blocker'))
								       $('#shadow').removeClass('blocker');
								redirect2Error(xhr.status);
								//alert("Oops !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
							}
						  }); */
						      
					}else{
								showPopUp("Error Message","Please fill valid data in fields highlighted in RED to proceed !!");
								event.preventDefault(); 
					}
				});
			});
			</script>


			</form>
			
		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		$("#headingFeedback").addClass('activeSubSubNav');
	});
</script>

</body>
</html>