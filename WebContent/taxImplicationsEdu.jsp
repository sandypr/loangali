	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				Tax Implications<br> <span>All about loans and tax savings for Education Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>Tax implications on first Housing Loan</h2>
				<p>In case you are opting to stay in the first house purchased
					on loan, the tax exemption is equal to the total interest payable
					or Rs. 2,00,000 whichever is lesser. In case of principal the tax
					exemption amount is equal to the principal payable or Rs. 1,00,000
					whichever is lower.</p>
				<h2>Tax implications on consecutive Housing Loans</h2>
				<p>In this case you can chose one house as occupied by you; and
					in the case of the second or third homes you can get a tax
					exemption as follows <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>'Tax exemption = Annual rent on the house - 30%
					of Annual rent on the house (For depreciation) - Interest Payable
					on the loan'</i>. <br>The interesting fact here is that you don't have a
					upper limit on the amount. However principal repaid on N number of
					houses is limited by Rs. 1,50,000</p>

				<h2>Tax implications on the principle portion:</h2>
				<p>The tax exemption on principle portion repaid is restricted
					to Rs. 2,00,000 independent on the number of housing loans taken by
					a person.</p>

				<h2>Tax implications on the interest portion</h2>
				<p>In case of interest portion; the tax exemption is limited to
					the lower values of Rs. 2,00,000 or the total interest payable in
					an year (whichever is lower) if its the first housing loan.
					However, in case of more than one housing loans; <i>'tax exemption =
					70% of annual rent - Interest payable on the loan'</i> with no cap.</p>
				<p>One important thing to note here is that, notional rent from
					the second house should be added to income and due tax has to be
					paid on it.</p>

				<h2>Disclaimer</h2>
				<p>All information shared here is purely for information purpose
					only. LoanGully and and is not a substitute for advice from a tax
					consultant. We strongly advice that you do a self analysis of your
					financial sources and commitments; and ensure you abide by the
					rules of the Indian Government for taxation of income which can be
					checked here (Link to Tax rules). Users are solely held responsible
					for any decision made by them using the information on this site
					and LoanGully shares no liability towards the same.</p>
			</div>

			<div class="taxImplicationBlock" style="text-align: right;">
				<a href="/web/calc/" class="calculateTaxLiability">Know
					your tax liability</a> <a href="/web/eduLoan/"
					class="applyLoanBtn">Apply Loan</a>
			</div>
		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
		$(function(){
			$("#headingTax").addClass('activeSubSubNav');
			$("#headerEL").addClass('activeTopNav');
		});
	</script>
