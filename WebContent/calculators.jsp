
<!-- add header -->
<%@ include file="header.html"%>
<%@ include file="errorPopup.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihCalculator.css">
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihDealsOffers.css"> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-calendar.css">
<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/js/globalize.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/cultures/globalize.cultures.js" type="text/javascript"></script>

	<div class="innerPageBannerBlock">
		<div class="wrapper">
		    
			<div class="bannerText homeLoanVeryBigIcon" id="bannerText">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div> 
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon" id='subnavblock_heading'>Home Loan</div>
			<div id='subsublevelnavblock'><%@ include file="subsublevelnavblock.html"%></div>  
		</div>
	</div>

	<div class="calculatorPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				Calculator<br> <span>Simplified calculators for your financial planning.</span>
			</h1>

			<div class="calcualtorStepsBlock">
				<div class="calcualtorStepsTabs" id="applyHomeStepsTabs">
					<ul class="calcualtorStepsTabs" id="calculatorSteps">
						<li><a href="#step1"> <strong>EMI Calculator</strong> <br>
								Know EMI eligibility
						</a></li>

						<li><a href="#step2"> <strong>Loan Eligibility</strong> <br>
								Know Loan eligibility
						</a></li>

						<li><a href="#step3"> <strong>Interest Payable</strong> <br>
								Know Interest payable
						</a></li>
						<li><a href="#step4"> <strong>Amortization</strong> <br>
								Amortization schedule
						</a></li>
						<li id='taxTab'><a href="#step5"> <strong>Tax Calculator</strong> <br>
								Know your Tax liability
						</a></li>
					</ul>

					<div class="calculatorTabsContent stepOne" id="step1">
						<div class="calculatorForm">
						     
							<form action="" id="emiEligi" name="emiEligi">
								<div class="formEachLabel">
									<label for="emi_income"> <span>Gross Monthly Salary </span>
									 <input
										type="text" name="Income" value=""
										class='textBox calculatorTextBox numberinput' id='emi_Income' maxlength="12">
									 <input
										type="hidden" value="" class='textBox calculatorTextBox'
										id='eincome' name=>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="emi_commitments"> <span>Monthly Commitments</span>
									 <input
										type="text" name="Commitments" value=""
										class='textBox calculatorTextBox numberinput valid' id='emi_Commitments' maxlength="12">
										<input type="hidden" value=""
										class='textBox calculatorTextBox' id='ecommitments'>
									</label>
								</div>

								<div class="formButton" style="float: left; margin-top: 21px;">
									<input type="button" value="Calculate" class='calculatorButton' id='calcEmiEligibility'>
								</div>

								<div class="calculatorAmountBlocks singleCol">
									<div class="calcualtorAmountCol">
										<h3>Your Max EMI Eligibility per month</h3>
										<div class="calculatorAmount">
											Rs.  <strong id='emiEligible'> </strong>
										</div>
									</div>

								</div>
								
							</form>
						</div>
						<div id="dealsNOffers" style="width: 100%; text-align:center; margin-bottom: 10px;"><a id="Check-exiting" href='/web/homeLoan' class='Check-exciting'>Check exciting Deals on Home Loans !</a></div>
						
					</div>

					<script type="text/javascript">
					
					$(document).ready(function() {
						
					$("#emi_Income").bind("keyup keydown change",function(){
						$("#eincome").val("");
						var emiIncome = $(this).val();
						if(convertNumberToCurrency(emiIncome,this.id,'Income')){
							$("#eincome").val(currencyToNumbers(emiIncome));
						}
						
					});
					
					$("#emi_Commitments").bind("keyup keydown change",function(){
						$("#ecommitments").val("");
						var emiCommitments = $(this).val();
						if(convertNumberToCurrency(emiCommitments,this.id,'Commitments')){
							$("#ecommitments").val(currencyToNumbers(emiCommitments));
						}
						removeHighlightOnField("emi_Commitments");
						
					});
					
					$.validator.methods.range = function (value, element, param) {
					    return this.optional(element) || (Globalize.parseFloat(value) >= param[0] && Globalize.parseFloat(value) <= param[1]);
					}
					
					 $("#emiEligi").validate({
						rules: {
								Income: {
									required: true,
									range : [0, 999999999]
								},
							},
						messages: {
							Income: {
				                required: "Required field.",
				                range: ""
							}
						},
						errorPlacement: function (error, element) {
				           // alert(error.text());
							$("span", element.parent().parent()).removeClass("error").addClass("error_show");
							element.removeClass("error");
							element.attr("title",error.text());
				        },
					});
					
					$('#calcEmiEligibility').click(function(){
						$("#emiEligible").html("");
						
						if($("#emiEligi").valid()){
							var income = $("#eincome").val();
							var commitments = $("#ecommitments").val();
							if(!commitments.length > 0 ){ commitments = 0;}
							
							if(Number(income) <= Number(commitments)){
								showPopUp("Error Message", "Sorry ! Commitments cannot be greater than Salary/Income. Please enter valid values.");
							}else{
								$.ajax({
									type : "GET",
									url : "/web/calc/emiEligibility/"+cat+"/"+income+"/"+commitments,
									contentType : 'application/json',
									dataType : "text",
									success : function(eligi) {
										if($.isNumeric(eligi) && !(Math.ceil(eligi) === 0)){
											$("#emiEligible").append(currencyFormatter(String(Math.ceil(eligi))));
										}else if(Math.ceil(eligi) === 0 || ! $.isNumeric(eligi)){
											showPopUp("Error Message","Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change values to check your eligibility !");
										}else{
											$("#emiEligible").append(String(eligi));
										}
									},
									error : function(xhr) {
										//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
										redirect2Error(xhr.status);
									}
								});
							}
						}else{
							showPopUp("Error Message","Please fill valid data in the fields highlighted in RED to proceed !");
							event.preventDefault(); 
						}
					});
				});
					

				</script>

					<div class="calculatorTabsContent stepTwo" id="step2">
						<div class="calculatorForm">
							<form action="" id="loanEligi">
								<div class="formEachLabel">
									<label for="loanEligi_Income"> <span>Gross Monthly Salary</span> 
									<input
										id='loanEligi_Income' type="text" value="" name="Income"
										class='textBox calculatorTextBox numberinput' maxlength="12">
									 <input
										type="hidden" value="" class='textBox calculatorTextBox'
										id='h_lincome'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="loanEligi_lcommitments"> <span>Monthly Commitments</span> 
									<input
										type="text" value="" class='textBox calculatorTextBox numberinput valid'
										id='loanEligi_Commitments' name="Commitments" maxlength="12" >
										<input type="hidden" value=""
										class='textBox calculatorTextBox' id='h_lcommitments'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="loanEligi_ltenure"> <span>Tenure (Years)</span> 
									<input
										type="text" value="" class='textBox calculatorTextBox numberinput tenure'
										id='loanEligi_Tenure' maxlength="2" name="Tenure">
									</label>
								</div>

								<div class="calculatorAmountBlocks singleCol">
									<div class="calcualtorAmountCol">
										<h3>Your Max Loan Eligibility</h3>
										<div class="calculatorAmount">
											Rs. <strong id='loanEligible'> </strong>
										</div>
									</div>
								</div>

								<div class="formButton">
									<input type="button" id='calcLoanEligibility' value="Calculate"
										class='calculatorButton'>
								</div>
							</form>

						</div>
<div id="dealsNOffers2" style="width: 100%; text-align:center; margin-bottom: 10px;"><a href='/web/homeLoan' class='Check-exciting'>Check exciting Deals on Home Loans !</a></div>
					</div>

					<script type="text/javascript">
					$(document).ready(function() {
						
						$("#loanEligi_Income").bind("keyup keydown change",function(){
							$("#h_lincome").val("");
							var val = $("#loanEligi_Income").val();
							if(convertNumberToCurrency(val,this.id, "Income")){
								$("#h_lincome").val(currencyToNumbers(val));
							}
						});
						
						$("#loanEligi_Commitments").bind("keyup keydown change",function(){
							$("#h_lcommitments").val("");
							var val = $("#loanEligi_Commitments").val();
							if(convertNumberToCurrency(val,this.id,"Commitments")){
								$("#h_lcommitments").val(currencyToNumbers(val));
							}
							removeHighlightOnField("loanEligi_lcommitments");
						});
						
						$.validator.methods.range = function (value, element, param) {
						    return this.optional(element) || (Globalize.parseFloat(value) >= param[0] && Globalize.parseFloat(value) <= param[1]);
						}
						
						$.validator.addMethod("validatetenure", function(value) {
							if (cat == 2 && value <= 30)
								return true;
							else if (cat == 3 && value <= 10)
								return true;
							else if (cat == 4 && value <= 15)
								return true;
							else
								return false;
							//return (value.length == 10  && value.substring(0,1) == "7" || value.substring(0,1) == "8" || value.substring(0,1) == "9")
						},'Sorry, home loans  are limited to 30 years! Personal/Lap loans  are limited to 5 years!  Please enter a tenure between 1-30 for Home loans and  1- 5 for Personal/LAP loans');
						
						
						 $("#loanEligi").validate({
							rules: {
								Income: {
									required: true,
									range : [0, 999999999]
								},
								Tenure:{
									required: true,
									validatetenure: true
								},
								
							},
							messages: {
								Income: {
				                required: "Required field.",
				                range: ""
								},
								Commitments: {
									lessThanEqual: "Sorry ! Commitments cannot be greater than Salary/Income. Please enter valid values."
								},
								Tenure: {
									required: "Required. ",
							        range : "Sorry, home loans  are limited to 30 years! Personal/Lap loans  are limited to 5 years!  Please enter a tenure between 1-30 for Home loans and  1- 5 for Personal/LAP loans"
								},
								
							},
							errorPlacement: function (error, element) {
								$("span", element.parent().parent()).removeClass("error").addClass("error_show");
								element.removeClass("error");
								element.attr("title",error.text());
							},
							
							 
						   highlight: function(element) {
						   		$("span", $(element).parent()).addClass("error_show");
						   },
							  
							unhighlight: function(element) {
						    	$("span", $(element).parent()).removeClass("error_show");
								$(element).removeAttr("title");
							},
						});
						$("#calcLoanEligibility").click(function(){
							
							$("#loanEligible").html("");
							
							if($("#loanEligi").valid()){
								
							var income = $("#h_lincome").val();
							var commitments = $("#h_lcommitments").val();
							var tenure = $("#loanEligi_Tenure").val();
							
							if(!commitments.length > 0 ){ commitments = 0;}
							
							if(Number(income) <= Number(commitments)){
								showPopUp("Error Message", "Sorry ! Commitments cannot be greater than Salary/Income. Please enter valid values.");
							}else{
								if (cat == 2 && tenure > 30)
									showPopUp("Error Message", "Sorry ! For Home Loans, tenure cannot be more than 30 years.");
								else if (cat == 3 && tenure > 10)
									showPopUp("Error Message", "Sorry ! For Personal Loans, tenure cannot be more than 10 years.");
								else if (cat == 4 && tenure > 15)
									showPopUp("Error Message", "Sorry ! For Mortgage Loans, tenure cannot be more than 15 years.");
								else {
									$.ajax({
										type : "GET",
										url : "/web/calc/loanEligibilityAmount/"+cat+"/"+income+"/"+commitments+"/"+tenure,
										contentType : 'application/json',
										dataType : "text",
										success : function(eligi) {
											$("#loanEligible").html("");
											if($.isNumeric(eligi) && !(Math.ceil(eligi) === 0)){												
												$("#loanEligible").append(currencyFormatter(String(Math.floor(eligi))));
											}else if(Math.ceil(eligi) === 0 || !$.isNumeric(eligi)){
												showPopUp("Error Message","Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change values to check your eligibility !");
											}
											else{
												$("#loanEligible").append(String(eligi));
											}
										},
										error : function(xhr) {
											//alert("Opps !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
											redirect2Error(xhr.status);
										}
								});
								}
							}
							}
							 else{
								 showPopUp("Error Message","Please fill valid data in the fields highlighted in RED to proceed !");
								 event.preventDefault(); 
							} 
						});
						
					});
		
				</script>

					<div class="calculatorTabsContent stepThree" id="step3">
						<div class="calculatorForm">
							<form action="" id="interestPayForm">
								<div class="formEachLabel">
									<label for="interestPay_LoanAmount"> <span>Loan Amount</span>
									 <input
										id='interestPay_LoanAmount' type="text" value="" name="LoanAmount"
										class='textBox calculatorTextBox numberinput' maxlength='14'> 
									<input
										type="hidden" value="" class='textBox calculatorTextBox numberinput'
										id='h_loanAmt'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="interestPay_ROI"> <span>ROI (Rate of Interest)</span> 
									<input
										type="text" id='interestPay_ROI' value="" name="ROI"
										class='textBox calculatorTextBox numberinput' maxlength="5">
									</label>
								</div>

								<div class="formEachLabel">
									<label for="gms"> <span>Tenure (Years)</span> 
									<input
										type="text" id='interestPay_Tenure' value="" name="Tenure"
										class='textBox calculatorTextBox numberinput tenure' maxlength="2">
									</label>
								</div>

								<div class="calculatorAmountBlocks threeCol">
									<div class="calcualtorAmountCol">
										<h3>Your EMI per month</h3>
										<div class="calculatorAmount">
											Rs. <strong id='emi'> </strong>
										</div>
									</div>

									<div class="calcualtorAmountCol">
										<h3>Total Interest Payable</h3>
										<div class="calculatorAmount">
											Rs. <strong id='interestPayable'> </strong>
										</div>
									</div>

									<div class="calcualtorAmountCol">
										<h3>Total Amount Payable</h3>
										<div class="calculatorAmount">
											Rs. <strong id='totalPayable'> </strong>
										</div>
									</div>
								</div>

								<div class="formButton">
									<input type="button" id='calcInterest' value="Calculate"
										class='calculatorButton'>
								</div>

							</form>
	
						</div>
<div id="dealsNOffers3" style="width: 100%; text-align:center; margin-bottom: 10px;"><a href='/web/homeLoan' class='Check-exciting'>Check exciting Deals on Home Loans !</a></div>
					</div>

					<script type="text/javascript">
							
							$(document).ready(function(){

								$("#interestPay_LoanAmount").bind("keyup keydown change",function(){
									var val = $("#interestPay_LoanAmount").val();
									if(convertNumberToCurrency(val,this.id,'Loan Amount')){
										$("#h_loanAmt").val(currencyToNumbers(val));
									}
								
								});
								
								$('#interestPay_ROI').bind("keyup keydown change", function() {
									var input=$(this);
									var is_tenure=input.val();
									if(is_tenure){
										input.removeClass("invalid").addClass("valid");
										$(this.id).attr("title", "Required field.");
									}
									else{
										input.removeClass("valid").addClass("invalid");
										$(this.id).removeAttr("title");
									}
								});
								
								$('#interestPay_Tenure').bind("keyup keydown change", function() {
									var input=$(this);
									var is_tenure=input.val();
									if(is_tenure){
										input.removeClass("invalid").addClass("valid");
										$(this.id).attr("title", "Required field.");
									}
									else{
										input.removeClass("valid").addClass("invalid");
										$(this.id).removeAttr("title");
									}
								});
							
								$("#calcInterest").click(function(){			
									var loanAmount = $("#h_loanAmt").val();
									var roi = $("#interestPay_ROI").val();
									var tenure = $("#interestPay_Tenure").val();
									
									$("#emi").html("");
									$("#interestPayable").html("");
									$("#totalPayable").html("");
									
									var error_free = validateFormFields("#interestPayForm", "#interestPay_");
									
									if (error_free){
									
										if(validLoanAmtROINTenure(loanAmount, roi, tenure, cat)){
											var emi = calculateEMI(loanAmount,roi,tenure);
											var totalPayable = emi * 12 * tenure;
											var interestPayable = totalPayable - loanAmount;
											
											$("#emi").append(currencyFormatter(String(emi)));
											$("#interestPayable").append(currencyFormatter(String(Math.ceil(interestPayable))));
											$("#totalPayable").append(currencyFormatter(String(Math.ceil(totalPayable))));
										}
									}else{
										showPopUp("Error Message","Please fill valid data in the fields highlighted in RED to proceed !!");
										event.preventDefault(); 
									}
									
								});
								
							});
							
							function calculateEMI(loanAmount,roi,tenure){
								
								var interestPerMonth = roi/12/100;
								var IPerMonthPerLakh = interestPerMonth * 100000;
								
								var ratio = (Math.pow((1+interestPerMonth), 12*tenure))/(Math.pow((1+interestPerMonth), 12*tenure)-1);
								
								var emi = ratio * IPerMonthPerLakh * loanAmount/100000;
								
								return Math.ceil(emi);									
							}
							
						</script>

					<div class="calculatorTabsContent stepThree" id="step4">
						<div class="calculatorForm">
							<form action="" id="amortForm">
								<div class="formEachLabel">
									<label for="amort_LoanAmount"> <span>Loan Amount</span>
									 <input
										id="amort_LoanAmount" name='LoanAmount' type="text" value=""
										class='textBox calculatorTextBox numberinput' maxlength='14'> 
									<input
										type="hidden" value="" class='textBox calculatorTextBox'
										id='h_a_loanAmt'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="amort_ROI"> <span>ROI (Rate of Interest)</span>
									 <input
										type="text" id="amort_ROI" value="" name="ROI"
										class='textBox calculatorTextBox numberinput' maxlength="5">
									</label>
								</div>

								<div class="formEachLabel">
									<label for="amort_Tenure"> <span>Tenure (Years)</span> <input
										type="text" id="amort_Tenure" name="Tenure" value=""
										class='textBox calculatorTextBox numberinput tenure' maxlength="2">
									</label>
								</div>


								<div class="formEachLabel">
									<label for="amort_DateOfCommencement"> <span>Date Of Commencement Of
											loan</span> <input type="text" id="amort_DateOfCommencement" name="DateOfCommencement" value=""
										 class='textBox calculatorTextBox' readonly>
										<script>
									$(function() {
								    var d = new Date();
    								var n = d.getFullYear();
								    $("#amort_DateOfCommencement").datepicker({
									      showOtherMonths: true,
									      selectOtherMonths: true,
									      dateFormat : "dd MM,yy",
									      changeMonth: true,
      									  changeYear: true,
      									  yearRange: (n-25)+":"+n
									    }
								    );
								    });
									</script>


									</label>
								</div>

								<div class="formEachLabel">
									<label for="gms"> <span>Part Payments</span> <input
										type="text" id="amort_PartPayments" name="PartPayments" value=""
										class='textBox calculatorTextBox numberinput' maxlength='12'> <input
										type="hidden" value="" class='textBox calculatorTextBox'
										id='h_partPayments'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="amort_Frequency"> <span>Frequency</span> <select
										id="amort_Frequency" name="Frequency"
										class="selectBox stepsTextBox" style="width: 284px;">
											<option value="">Select a Frequency</option>
											<option value="monthly">Monthly</option>
											<option value="quarterly">Quarterly</option>
											<option value="halfyearly">Half Yearly</option>
											<option value="yearly">Yearly</option>
											<option value="once">Once. This Month</option>
									</select>
									</label>
								</div>
								<span id='savedMonths'></span>
								<div class="formButton">
									<input type="button" value="Calculate" class='calculatorButton'
										id='calcAmortization'>
								</div>

							</form>

							<script type="text/javascript">
							$(document).ready(function(){
								$("#amort_LoanAmount").bind("keyup keydown change",function(){
									var val = $("#amort_LoanAmount").val();
									if(val.length < 1){
											highlightErrorFieldForLoans(this.id,"Required field.");
											$("#h_a_loanAmt").val(0);
										}else {
											var newVal = currencyFormatter(val);
											var unformattedNewVal = currencyToNumbers(newVal);
											removeHighlightOnField(this.id);
											$("#amort_LoanAmount").val(newVal);											
											$("#h_a_loanAmt").val(unformattedNewVal);
										}
								
								});
								
								$('#amort_ROI').bind("keyup keydown change", function() {
									var input=$(this);
									var is_tenure=input.val();
									if(is_tenure){
										input.removeClass("invalid").addClass("valid");
										$(this.id).attr("title", "Required field.");
									}
									else{
										input.removeClass("valid").addClass("invalid");
										$(this.id).removeAttr("title");
									}
								});
								
								$('#amort_Tenure').bind("keyup keydown change", function() {
									var input=$(this);
									var is_tenure=input.val();
									if(is_tenure){
										input.removeClass("invalid").addClass("valid");
										$(this.id).attr("title", "Required field.");
									}
									else{
										input.removeClass("valid").addClass("invalid");
										$(this.id).removeAttr("title");
									}
								});
								
								$("#amort_PartPayments").bind("keyup keydown change",function(){
									var val = $("#amort_PartPayments").val();
									if(convertNumberToCurrency(val,this.id)){
										$("#h_partPayments").val(currencyToNumbers(val));
									}
									removeHighlightOnField("amort_PartPayments");
								});
								
								$("#calcAmortization").click(function(){
									
									if(!$("#amort_PartPayments").val().length > 0){
										$("#h_partPayments").val("");
									}
									
									var a_loanAmount = Number($("#h_a_loanAmt").val());
									var a_roi = Number($("#amort_ROI").val());
									var a_tenure = Number($("#amort_Tenure").val());
									var a_doc = $("#amort_DateOfCommencement").val();
									var a_partPayments = Number($("#h_partPayments").val());
									var a_frequency = $("#amort_Frequency").val();
									
									$("#savedMonths").html("");
									$("#savedMonths").removeAttr('style');
											
									//all mandatory fields are valid
									if(a_doc.length > 0){
										removeHighlightOnField("amort_DateOfCommencement");
									}else{
										highlightErrorField("amort_DateOfCommencement",forMandatory,"Date Of Commencement");
									}
									
									if(a_partPayments > 0 ){
										if(!a_frequency.length > 0){
											highlightErrorFieldForLoans("amort_Frequency","Required field.");
										}else{
											removeHighlightOnField("amort_Frequency");
										}
									}else{
										if(a_frequency.length > 0){
											//highlightErrorField("amort_PartPayments",forMandatory,"Part Payments");
											highlightErrorFieldForLoans("amort_PartPayments","Required field.");
											removeHighlightOnField("amort_Frequency");
											$("#amort_Frequency").attr("style","width : 284px");
										}else{
											removeHighlightOnField("amort_PartPayments");
											removeHighlightOnField("amort_Frequency");
											$("#amort_Frequency").attr("style","width : 284px");
										}
									}
									
									$('#amortTable').html("");
									var error_free = validateFormFields("#amortForm", "#amort_");
									
									if (error_free){
									
										if(validLoanAmtROINTenure(a_loanAmount, a_roi, a_tenure, cat) && validPartPayments(a_partPayments, a_loanAmount, a_frequency)){
											$('#amortTable').append("<tr id='amort_header'> </tr>");
											$('#amort_header').append("<th width='14%'> Month </th>");
											$('#amort_header').append("<th width='14%'> EMI</th>");
											$('#amort_header').append("<th width='14%'> Interest</th>");
											$('#amort_header').append("<th width='14%'> Principal</th>");
											$('#amort_header').append("<th width='14%'> Part payments</th>");
											$('#amort_header').append("<th width='14%'> Actual Amount</th>");
											$('#amort_header').append("<th width='14%'> Balance Amount</th>");
											
											$('#amortTable').append("<tr id='amortRow0'> </tr>");
											
											var emi = calculateEMI(a_loanAmount,a_roi,a_tenure);
											var monthlyEmi = emi;
											var principleBal = currencyToNumbers($("#amort_LoanAmount").val());
											var freq;
											if(a_frequency == 'once')
												freq = 0;
											else if(a_frequency == 'monthly')
												freq = 1;
											else if(a_frequency == 'quarterly')
												freq = 3;
											else if(a_frequency == 'halfyearly')
												freq = 6;
											else if(a_frequency == 'yearly')
												freq = 12;
											
											var commensementDate = new Date(a_doc);
											var cMonth = commensementDate.getMonth()+1;
											var cYear = commensementDate.getUTCFullYear();
											var tenureMonths = a_tenure * 12;
											
											var i = 0;
											for(; i <= tenureMonths && principleBal > 0; i= i+1,cMonth++){
												
												var month = cMonth%12;
												var a_month = month;
												var a_year = cYear;
												if(month == 1 && i > 0){
													cMonth = 1;
													cYear++;
												}										
												a_month = cMonth;
												a_year = cYear;
												var interestComponent = 0.0;
												var principleComponent = 0.0;
												interestComponent = Math.ceil((principleBal*a_roi)/12/100);
												if(principleBal < emi){
													emi = principleBal + interestComponent;
													principleComponent = principleBal;
												}else {
													principleComponent = emi - interestComponent;
												}
												
												$("#amortRow"+i).append("<td align='center'>"+getShortMonth(a_month-1)+", "+a_year+"</td>");
												$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(Math.ceil(emi)))+"</td>");
												$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(interestComponent))+"</td>");
												$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(principleComponent))+"</td>");
	
												var partPayementApplicable = 0.0;
												var remainingBalance = principleBal - principleComponent;
												
												if(freq == 0){
													var currDate = new Date();
													if(a_month == currDate.getMonth()+1 && a_year == currDate.getUTCFullYear() && a_partPayments > 0){
														partPayementApplicable = a_partPayments;
														remainingBalance = remainingBalance - partPayementApplicable;
														$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(round(partPayementApplicable,0)))+"</td>");
													}else{
														$("#amortRow"+i).append("<td></td>");
													}
												}else if(freq != 0 && (i+1)%freq == 0 && a_partPayments > 0){
													if(remainingBalance > a_partPayments){
														partPayementApplicable = a_partPayments;
														remainingBalance = remainingBalance - partPayementApplicable;
														$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(round(partPayementApplicable,0)))+"</td>");
													}else if(remainingBalance > 0 && remainingBalance < a_partPayments){
														partPayementApplicable = remainingBalance;
														remainingBalance = remainingBalance - partPayementApplicable;
														$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(round(partPayementApplicable,0)))+"</td>");
													}else{
														$("#amortRow"+i).append("<td></td>");
													}													
												}else{
													$("#amortRow"+i).append("<td></td>");
												}							
												principleBal = remainingBalance;
												
												$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(round(a_loanAmount,0)))+"</td>");
												$("#amortRow"+i).append("<td>Rs. "+currencyFormatter(String(round(principleBal,0)))+"</td>");
												 
												var newRow = i+1;
												$("#amortTable").append("<tr id='amortRow"+newRow+"'></tr>");
											}
											
											if(freq){
												if(freq >= 1 && tenureMonths >= i){
													$("#savedMonths").append("With Part payments you would save   <strong>  Rs. "+currencyFormatter(String(Number(((tenureMonths-i)*monthlyEmi)+(monthlyEmi-emi)))) +" </strong><br/>");
													$("#savedMonths").attr('style',"float: left; font-size:14px; border: 1px solid #ccc;background: #f1f1f1; padding: 10px;");
												}
											}
										}
										
									}else{
									
										if(a_partPayments < 1 && a_frequency.length > 0){
											showPopUp("Error Message",'Sorry ! Part Payments is necessary when a Frequency is provided.');
										}else if(a_partPayments > 0 && a_frequency.length < 1){
											showPopUp("Error Message",'Sorry ! Frequency is necessary when a Part Payments is provided.');
										}else if(a_partPayments > 0 && a_partPayments > a_loanAmount){
											showPopUp("Error Message",'Sorry ! Part Payments cannot be greater than Loan Amount.');
										}else
											showPopUp("Error Message","Please fill valid data in the fields highlighted in RED to proceed !");
									}
								});
							});
							
						</script>

							<div class="emiAmountTableBlock">
								
								<table cellspacing="0" cellpadding="0" width="100%" class="emiAmountTable" id='amortTable'>

								</table>
								<!-- <a href="#" class="tableCollapse">Down Arrow</a> -->
							</div>
						</div>
						<div id="dealsNOffers4" style="width: 100%; text-align:center; margin-bottom: 10px;"><a href='/web/homeLoan' class='Check-exciting'>Check exciting Deals on Home Loans !</a></div>
					</div>

					<div class="calculatorTabsContent stepOne" id="step5" style="display:none;">
						<div class="calculatorForm">
							<form action="" id="taxPaybale">
								<div class="formEachLabel">
									<label for="taxP_income"> <span>Gross Monthly Salary</span>
									 <input
										id="taxP_Income" type="text" value=""
										class='textBox calculatorTextBox numberinput' name="Income" maxlength='12'>
										 <input
										type="hidden" value="" class='textBox calculatorTextBox '
										id='t_inc'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="taxP_Component"> <span>Principal Component on
											Home Loan</span> 
										<input type="text" id='taxP_PrincipalComponent' value=""
										class='textBox calculatorTextBox numberinput' name="PrincipalComponent" maxlength='14'>
										 <input
										type="hidden" value="" class='textBox calculatorTextBox '
										id='p_comp'>
									</label>
								</div>

								<div class="formEachLabel">
									<label for="taxP_iComponent"> <span>Interest Component on
											Home Loan</span> 
									<input type="text" id='taxP_InterestComponent' value=""
										class='textBox calculatorTextBox numberinput' name="InterestComponent" maxlength='14'> 
									<input
										type="hidden" value="" class='textBox calculatorTextBox'
										id='i_comp'>
									</label>
								</div>

								<div class="calculatorAmountBlocks singleCol">
									<div class="calcualtorAmountCol">
										<h3>Your Tax Liability</h3>
										<div class="calculatorAmount">
											Rs. <strong id='taxLiability'></strong>
										</div>

									</div>
								</div>

								<div class="formButton">
									<input type="button" value="Calculate" class='calculatorButton'
										id='calcTaxLiability'>
								</div>

							</form>
						</div>
			<div style="width: 100%; text-align:center; margin-bottom: 10px;"><a href='/web/homeLoan' class='Check-exciting'>Check exciting Deals on Home Loans !</a></div>
					</div>
					
				</div>

				<script type="text/javascript">
				$(document).ready(function(){
					$("#taxP_Income").bind("keyup keydown change",function(){
						var val = $("#taxP_Income").val();
						if(convertNumberToCurrency(val,this.id,"Income")){
							$("#t_inc").val(currencyToNumbers(val));
						}
						
					});
					
					$("#taxP_PrincipalComponent").bind("keyup keydown change",function(){
						var val = $("#taxP_PrincipalComponent").val();
						if(convertNumberToCurrency(val,this.id,"Principal Component")){
							$("#p_comp").val(currencyToNumbers(val));
						}
						
					});
					
					$("#taxP_InterestComponent").bind("keyup keydown change",function(){
						var val = $("#taxP_InterestComponent").val();
						if(convertNumberToCurrency(val,this.id, "Interest Component")){
							$("#i_comp").val(currencyToNumbers(val));
						}
						
					});
					
					$("#calcTaxLiability").click(function(){
						
						$("#taxLiability").html("");
						var income = $("#t_inc").val();
						var pComp = $("#p_comp").val();
						var iComp = $("#i_comp").val();
						
						var pCompExemptionLimit = 150000;
						var iCompExemptionLimit = 200000;
						var error_free = validateFormFields("#taxPaybale", "#taxP_") ;
						
						if (!error_free){
							showPopUp("Error Message","Please fill valid data in the fields highlighted in RED to proceed !!");
							event.preventDefault(); 
						}else if(income*12 >= pCompExemptionLimit){
						
							var pCompExemption = pCompExemptionLimit;
							var iCompExemption = iCompExemptionLimit;
							
							if(pComp < pCompExemptionLimit)
								pCompExemption = pComp;
							
							if(iComp < iCompExemptionLimit)
								iCompExemption = iComp;
							
							var taxLiable = (income*12) - (Number(pCompExemption) + Number(iCompExemption));
							
							$("#taxLiability").html("");
							$("#taxLiability").append(currencyFormatter(String(round(taxLiable,2))));
							
						}else{
							$("#taxLiability").html("");
							$("#taxLiability").append(0);
						}
					});
				});
			</script>

				<%-- <div class="calculatorDealsOffersBlock">
					<h2>
						Best Deals &amp; Offers <br> <!-- <span>Your contact
							information is secured and will not be shared without your
							concern in next stage</span> -->
					</h2>

					<div class="dealsOffersSlider owl-carousel" id="dealsOffersSlider">
						<div class="banksDealCol item">
							<div class="bankOfferBanner">
								<img
									src="${pageContext.request.contextPath}/images/axisBankOffer.jpg"
									alt="">
							</div>
							<div class="bankOfferInfo">
								<h3>12% Cash Bank Offer</h3>
								<p>Your contact information is secure and will not be shared
									without your concern in next stage</p>
								<a href="/web/homeLoan/" class="availButton">Avail</a>
							</div>
						</div>

						<div class="banksDealCol item">
							<div class="bankOfferBanner">
								<img
									src="${pageContext.request.contextPath}/images/iciciBankOffer.jpg"
									alt="">
							</div>
							<div class="bankOfferInfo">
								<h3>12% Cash Bank Offer</h3>
								<p>Your contact information is secure and will not be shared
									without your concern in next stage</p>
								<a href="/web/homeLoan/" class="availButton">Avail</a>
							</div>
						</div>

						<div class="banksDealCol item">
							<div class="bankOfferBanner">
								<img
									src="${pageContext.request.contextPath}/images/kotakBankOffer.jpg"
									alt="">
							</div>
							<div class="bankOfferInfo">
								<h3>12% Cash Bank Offer</h3>
								<p>Your contact information is secure and will not be shared
									without your concern in next stage</p>
								<a href="/web/homeLoan/" class="availButton">Avail</a>
							</div>
						</div>

						<div class="banksDealCol item">
							<div class="bankOfferBanner">
								<img
									src="${pageContext.request.contextPath}/images/hdfcBakOffer.jpg"
									alt="">
							</div>
							<div class="bankOfferInfo">
								<h3>12% Cash Bank Offer</h3>
								<p>Your contact information is secure and will not be shared
									without your concern in next stage</p>
								<a href="/web/homeLoan/" class="availButton">Avail</a>
							</div>
						</div>
						<div class="banksDealCol item">
							<div class="bankOfferBanner">
								<img
									src="${pageContext.request.contextPath}/images/iciciBankOffer.jpg"
									alt="">
							</div>
							<div class="bankOfferInfo">
								<h3>12% Cash Bank Offer</h3>
								<p>Your contact information is secure and will not be shared
									without your concern in next stage</p>
								<a href="/web/homeLoan/" class="availButton">Avail</a>
							</div>
						</div>
					</div>
				</div> --%>
			</div>

		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>

	<script type="text/javascript">
	$(document).ready(function(){
		
		$(".numberinput").forceNumeric();
		
		 $('.tenure').bind('keyup', function(event) {
		        var currValue = $(this).val();

		        if(currValue.search(/[^0-9]/) != -1)
		        {
		            // Change this to something less obnoxious
		            showPopUp("Error Message",'Only numerical inputs please');
		        }

		        $(this).val(currValue.replace(/[^0-9]/, ''));
		    });
		
		$('#applyHomeStepsTabs').tabs({
			beforeActivate: function( event, ui ) {
			   $(this).find('form').trigger('reset');
			   $(this).find('input:text').val('');
			   $(this).find('input:text').removeClass('valid');
			   $("span", $(this).parent().parent()).removeClass("error_show");
			   
			   $("#eincome").val("");
			   $("#loanEligible").html("");
			   $("#emiEligible").html("");
			   $("#interestPayable").html("");
			   $("#totalPayable").html("");
			   $("#taxLiability").html("");
			   $('#amortTable').html("");
			   $('#emi').html("");
			   
			}
		});
		
	});
</script>

	<script>
	$(function() {

		$('#elgibilitySlider').slider({
			value : 60,
			animate : true
		});

		$('#tenureSlider').slider({
			value : 50,
			animate : true
		});

		$('#applyHomeStepsTabs').tabs({
			heightStyle : 'content'
		});

		$('#dealsOffersSlider').owlCarousel({
			items : 2,
			pagination : false,
			autoPlay : false,
			navigation : true
		});

	});
</script>
	<script>
	var cat ;
	$(function(){
		// making decision based on category what to display
	    cat = (localStorage.getItem("sourceCategory") !== null )? localStorage.getItem("sourceCategory") : 2;
		if(cat != "2"){
			$("#taxTab").attr("style","display:none");
		}

		if(cat == "3" || cat == "4" || cat == "5"){
			/* $("#bannerText").html("");
			$("#subLevelNav").html(""); */
			
			var div = $("<div id='subsublevelnavblock'></div>");
			if(cat == "3"){
				/* $("#bannerText").append('<div class="bannerText personelLoansVeryBigIcon"><span>Celebrate being</span> smarter !	</div>');
				$("#subLevelNav").append('<div class="subLevelNavBlock personelLoanIcon">Personal Loan</div>');
				$("#subLevelNav").append(div); */
				$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockPL.html");
				$('.bankOfferInfo a').attr('href','/web/personalLoan/');
				$('a.Check-exciting').attr('href','/web/personalLoan/');
				$('a.Check-exciting').text('Check exciting Deals on Personal Loans !');
				
				$("#bannerText").html("");
				$("#bannerText").append('<span>Celebrate being</span> smarter !');
				$("#bannerText").removeClass("homeLoanVeryBigIcon");
				$("#bannerText").addClass("personelLoansVeryBigIcon");
				$("#subnavblock_heading").html("");
				$("#subnavblock_heading").append("Personal Loan");
				$("#subnavblock_heading").removeClass("homeLoansIcon");
				$("#subnavblock_heading").addClass("personelLoanIcon");
				
				$("#headerPL").addClass('activeTopNav');
			}else if(cat == "4"){
				/* $("#bannerText").append('<div class="bannerText personelLoansVeryBigIcon"><span>Give wings </span>to your dreams !</div>');
				$("#subLevelNav").append('<div class="subLevelNavBlock personelLoanIcon"><span>Mortgage Loan</span></div>');
				$("#subLevelNav").append(div); */
				
				$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockLAP.html");
				$('a.Check-exciting').attr('href','/web/lapLoan/');
				$('a.Check-exciting').text('Check exciting Deals on Mortgage Loans !');
				$('.bankOfferInfo a').attr('href','/web/lapLoan/');
				
				$("#bannerText").html("");
				$("#bannerText").append('<span>Give wings </span>to your dreams !');
				$("#bannerText").removeClass("homeLoanVeryBigIcon");
				$("#bannerText").addClass("personelLoansVeryBigIcon");
				$("#subnavblock_heading").html("");
				$("#subnavblock_heading").append("Mortgage Loan");
				$("#subnavblock_heading").removeClass("homeLoansIcon");
				$("#subnavblock_heading").addClass("personelLoanIcon");
				
				$("#headerLAP").addClass('activeTopNav');
			}else if(cat == "5"){
				/* $("#bannerText").append('<div class="bannerText personelLoansVeryBigIcon"><span>Give wings </span>to your dreams !</div>');
				$("#subLevelNav").append('<div class="subLevelNavBlock personelLoanIcon"><span>Mortgage Loan</span></div>');
				$("#subLevelNav").append(div); */
				
				$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockEdu.html");
				$('a.Check-exciting').attr('href','/web/eduLoan/');
				$('a.Check-exciting').text('Check exciting Deals on Education Loans !');
				$('.bankOfferInfo a').attr('href','/web/eduLoan/');
				
				$("#bannerText").html("");
				$("#bannerText").append('<span>Education is</span> LIFE itself !');
				$("#bannerText").removeClass("homeLoanVeryBigIcon");
				$("#bannerText").addClass("eduLoansVeryBigIcon");
				$("#subnavblock_heading").html("");
				$("#subnavblock_heading").append("Education Loan");
				$("#subnavblock_heading").removeClass("homeLoansIcon");
				$("#subnavblock_heading").addClass("eduLoanIcon");
				
				$("#headerEL").addClass('activeTopNav');
			}
			
		}else if(cat == '2'){
			$("#headerHL").addClass('activeTopNav');
		}
		
		$("#headingCalc").addClass('activeSubSubNav');
	});
	
</script>
</body>
</html>
