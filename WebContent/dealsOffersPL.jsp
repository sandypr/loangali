
	<div class="dealsOffersPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				Deals &amp; Offers<br> <span>Check for exciting deals
					and offers on Personal loans.</span>
			</h1>

			<div class="banksDealsOffersBlock">
				<div class="banksDealCol">
					<div class="bankOfferBanner">
						<img
							src="${pageContext.request.contextPath}/images/axisBankOffer.jpg" alt="">
					</div>
					<div class="bankOfferInfo">
						<h3>12% Cash Bank Offer</h3>
						<p>Your contact information is secure and will not be shared
							without your concern in any stage</p>
						<a href="/web/personalLoan/" class="availButton">Avail</a>
					</div>
				</div>

				<div class="banksDealCol">
					<div class="bankOfferBanner">
						<img
							src="${pageContext.request.contextPath}/images/iciciBankOffer.jpg"
							alt="">
					</div>
					<div class="bankOfferInfo">
						<h3>12% Cash Bank Offer</h3>
						<p>Your contact information is secure and will not be shared
							without your concern in any stage</p>
						<a href="/web/personalLoan/" class="availButton">Avail</a>
					</div>
				</div>

				<div class="banksDealCol">
					<div class="bankOfferBanner">
						<img
							src="${pageContext.request.contextPath}/images/kotakBankOffer.jpg"
							alt="">
					</div>
					<div class="bankOfferInfo">
						<h3>12% Cash Bank Offer</h3>
						<p>Your contact information is secure and will not be shared
							without your concern in any stage</p>
						<a href="/web/personalLoan/" class="availButton">Avail</a>
					</div>
				</div>

				<div class="banksDealCol">
					<div class="bankOfferBanner">
						<img
							src="${pageContext.request.contextPath}/images/hdfcBakOffer.jpg"
							alt="">
					</div>
					<div class="bankOfferInfo">
						<h3>12% Cash Bank Offer</h3>
						<p>Your contact information is secure and will not be shared
							without your concern in any stage</p>
						<a href="/web/personalLoan/" class="availButton">Avail</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function(){
			$("#headingDeals").addClass('activeSubSubNav');
		});
	</script>
	