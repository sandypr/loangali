<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Loan Galli - Simpler than others !</title>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
</head>
<body>

	<!-- add header -->
	<%@ include file="admin_header.html"%>

	<div class="dealsOffersPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				<span> <br>Add/Modify Products
				</span>
			</h1>

			<div class="applyHomeLoansSteps">
				<div class="applyHomeStepsTabs" id="applyHomeStepsTabs">
					<ul class="stepsTabsList">
						<li><a href="#step1" class="active" id='step-1'> <span
								class="stepNumber">+ </span> <span class="stepNameInfo">
									<strong>Add</strong> <br> New Product
							</span>
						</a></li>

						<li><a href="#step2" class="active" id='step-2'> <span
								class="stepNumber">/</span> <span class="stepNameInfo"> <strong>Edit</strong>
									<br> Existing Product
							</span>
						</a></li>

						<li><a href="#step3" class="active" id='step-3'> <span
								class="stepNumber">-</span> <span class="stepNameInfo"> <strong>Delete</strong>
									<br> Existing Product
							</span>
						</a></li>
					</ul>

					<form action="" method="" id=''>
						<div class="stepsTabContent stepOne" id="step1">
							<div class="formBlock">
								<ul class="formElementList">
									<li><label for="#">Label</label> <input type="text" id=""
										class="textBox stepsTextBox" value="" required></li>
									<li><label for="#">Label</label> <input type="text" id=""
										class="textBox stepsTextBox" value="" required></li>
									<li><label for="">Label</label> <select name="" id=""
										class="selectBox stepsTextBox">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label</label> <select
										class="selectBox stepsTextBox" name="employmentType"
										id="employmentType">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label</label> <input type="text" id=""
										class="textBox stepsTextBox" value="" required></li>
									<li><label for="#">Label</label> <input type="text" id=""
										class="textBox stepsTextBox" value="" required></li>

									<li class="checkBoxBlock"><input type="button"
										value="Submit Changes" class="stepsSubmitButton" id='' name=''>
									</li>
								</ul>
							</div>
						</div>
					</form>
					<form action="" method="" id=''>
						<div class="stepsTabContent stepOne" id="step2">
							<div class="formBlock">
								<ul class="formElementList">
									<li><label for="#">Label Tab2</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="#">Label Tab2</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="">Label Tab2</label> <select name="" id=""
										class="selectBox stepsTextBox">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label Tab2</label> <select
										class="selectBox stepsTextBox" name="employmentType"
										id="employmentType">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label Tab2</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="#">Label Tab2</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li class="checkBoxBlock"><input type="button"
										value="Submit Changes" class="stepsSubmitButton" id='' name=''>
									</li>
								</ul>
							</div>
						</div>
					</form>
					<form action="" method="" id=''>
						<div class="stepsTabContent stepOne" id="step3">
							<div class="formBlock">
								<ul class="formElementList">
									<li><label for="#">Label Tab3</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="#">Label Tab3</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="">Label Tab3</label> <select name="" id=""
										class="selectBox stepsTextBox">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label Tab3</label> <select
										class="selectBox stepsTextBox" name="employmentType"
										id="employmentType">
											<option value="">Option 1</option>
											<option value="">Option 1</option>
									</select></li>
									<li><label for="#">Label Tab3</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li><label for="#">Label Tab3</label> <input type="text"
										id="" class="textBox stepsTextBox" value="" required>
									</li>
									<li class="checkBoxBlock"><input type="button"
										value="Delete" class="stepsSubmitButton" id='' name=''>
									</li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>

	<script>
		$(function() {

			$('#applyHomeStepsTabs').tabs({
				heightStyle : 'content'
			});

		});
	</script>
	<!-- <div id="errorAlert" title="LoanGalli - Alert !!"></div> -->
</body>
</html>