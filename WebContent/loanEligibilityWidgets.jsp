<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon" id='widgetsPageTagline'>
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div>
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock" id='widgetsPageTypeHeading'></div>
			<div id='subsublevelnavblock'><%@ include
					file="subsublevelnavblock.html"%></div>
		</div>
	</div>

	<div class="dealsOffersPage">
		<div class="wrapper">
			<!-- <h1 class="pageHeading"> <span>Your contact information is secured and will not be shared without your concern in any stage</span>
		</h1> -->

			<div class="applyHomeLoansSteps">
				<div class="applyHomeStepsTabs" id="applyHomeStepsTabs">
					<input type='hidden' id='widgetsView' name='widgetsView'
						value='true' /> <input type='hidden' id='categoryType'
						name='categoryType' value='' /> <input type='hidden' id='tenure'
						value="" /> <input type='hidden' id='defaultTenure' value="" /> <input
						type='hidden' id='reqEligibility' value="0.0" /> <input
						type='hidden' id='income' value="" /> <input type='hidden'
						id='monthlyIncome' value="" /> <input type='hidden'
						id='commitments' value="0" /> <input type='hidden' id='resStatus'
						value="" /> <input type='hidden' id='employmentType' value="" /> <input
						type='hidden' id='name' value="Guest" />

					<div class="stepsTabContent stepTwo" id="step2">
						<div class="stepOneInfo">
							<div class="stepOneNameInfo">
								Welcome <strong id='prospectDisplayName'></strong><br> Your
								Maximum Eligibility details
							</div>

							<div class="stepOneLoanAmounts">
								<div class="stepOneMaxLoanAmount loanAmountBlck">
									<span>Max Loan Amount</span><br> <span
										class="loanAmountNumbers" id='topLoanAmount'><span
										class="blueSmallText">Lakhs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount tenureBlck">
									<span>Tenure</span><br> <span class="loanAmountNumbers"
										id='topTenure'> <span class="blueSmallText">Years</span></span>
								</div>

								<div class="stepOneMaxLoanAmount emiBlck">
									<span>EMI</span><br> <span class="loanAmountNumbers"
										id='topEmi'><span class="blueSmallText">Rs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount interestRates">
									<span>Interest Rate</span><br> <span
										class="loanAmountNumbers" id='topRoi'> <span
										class="blueSmallText">%</span></span>
								</div>
							</div>
						</div>

						<div class="loainAmountGraph">
							<table>
								<tr>
									<td><div id="gaugeAmt" style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeTenure"
											style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeEmi" style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeRoi" style="width: 230px; height: 180px"></div></td>
								</tr>
							</table>
						</div>

						<div class="elgibilityTentureBlock">
							<div class="elgibilityBlock">
								<div class="elgibilityText">Eligibility</div>
								<div class="elgibilityProgressBar" id="eligibilitySlider"></div>
								<div class="elgibilityNumber" id="eligibilityAmountForSlider"></div>
							</div>

							<div class="elgibilityBlock">
								<div class="elgibilityText">Tenure</div>
								<div class="elgibilityProgressBar" id="tenureSlider"
									style='float: left;'></div>
								<div class="elgibilityNumber" id="tenureForSlider"></div>
							</div>
						</div>

						<div class="termLoanEMITableBlck">
							<table cellpadding="0" cellspacing="0" width="100%"
								class="termLoanEMITable">
								<tr>
									<th width="20%" valign="middle">Term</th>
									<td width="16%" valign="middle" id='col11'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col21'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col31'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col41'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col51'><span
										class="blueSmallText">Yrs</span></td>
								</tr>
								<tr>
									<th>Loan Amount</th>
									<td id='col12'><span class="blueSmallText">Lks</span></td>
									<td id='col22'><span class="blueSmallText">Lks</span></td>
									<td id='col32'><span class="blueSmallText">Lks</span></td>
									<td id='col42'><span class="blueSmallText">Lks</span></td>
									<td id='col52'><span class="blueSmallText">Lks</span></td>
								</tr>
								<tr>
									<th>EMI</th>
									<td id='col13'></td>
									<td id='col23'></td>
									<td id='col33'></td>
									<td id='col43'></td>
									<td id='col53'></td>
								</tr>
							</table>
						</div>

						<div class="bestDealsBlock" style='border-top: 0px solid;'>
							<div class="bestDealsTextButton">
								<div class="bestDealsTxt"></div>

								<div class="bestDealsSearchButton">
									<a href="javascript:void(0)" id='collectContactDetails'>Continue & Get Quote</a>
								</div>
							</div>
						</div>
						<script>
						$(document).ready(function(){
							$("#collect_and_compare").on('click',function(){
								window.location.href = "/web/homeLoan/";
							});
						});
					</script>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<input type='hidden' id='defaultTenure' value="" />
	<script>
	var emiEligibility = 0.0;
	$(function() {
		
		// retrieve values
		if (typeof localStorage.getItem("income") !== 'undefined' && localStorage.getItem("income") !== null){
			$("#income").val(localStorage.getItem("income"));
			$("#monthlyIncome").val(localStorage.getItem("income"));
		}
		
		if (typeof localStorage.getItem("commitments") !== 'undefined' && localStorage.getItem("commitments") !== null){
			$("#commitments").val(localStorage.getItem("commitments"));
		}
		
		if (typeof localStorage.getItem("categoryType") !== 'undefined' && localStorage.getItem("categoryType") !== null)
			$("#categoryType").val(localStorage.getItem("categoryType"));
			
		if (typeof localStorage.getItem("resStatus") !== 'undefined' && localStorage.getItem("resStatus") !== null)
			$("#resStatus").val(localStorage.getItem("resStatus"));
		
		if (typeof localStorage.getItem("employmentType") !== 'undefined' && localStorage.getItem("employmentType") !== null)
			$("#employmentType").val(localStorage.getItem("employmentType"));
			
		if (typeof localStorage.getItem("defaultTenure") !== 'undefined' && localStorage.getItem("defaultTenure") !== null){
			$("#defaultTenure").val(localStorage.getItem("defaultTenure"));
		}
		
		// initiate paining
		paintHeadings();
		paintStep2();

		$("#collectContactDetails").on('click',function(){
			var cat = $("#categoryType").val();
			if(cat == 2){
				window.location.href = "/web/homeLoan";
			}else if(cat == 3){
				window.location.href = "/web/personalLoan/";
			}else if(cat == 4){
				window.location.href = "/web/lapLoan/";
			}
		});
	});
	
	function paintHeadings(){
			var cat = $("#categoryType").val();
			if(cat == 2){
				$("#widgetsPageTagline").addClass("homeLoanVeryBigIcon");
				$("#widgetsPageTypeHeading").addClass("homeLoansIcon");
				$("#widgetsPageTypeHeading").html("");
				$("#widgetsPageTypeHeading").html("Home Loan");
				//$("#defaultTenure").val(25);
				//$("#tenure").val(20);
			}else if(cat == 3){
				$("#widgetsPageTagline").addClass("personalLoanVeryBigIcon");
				$("#widgetsPageTypeHeading").addClass("personalLoansIcon");
				$("#widgetsPageTypeHeading").html("");
				$("#widgetsPageTypeHeading").html("Personal Loan");
				//$("#defaultTenure").val(10);
				//$("#tenure").val(5);
			}else if(cat == 4){
				$("#widgetsPageTagline").addClass("personalLoanVeryBigIcon");
				$("#widgetsPageTypeHeading").addClass("personalLoansIcon");
				$("#widgetsPageTypeHeading").html("");
				$("#widgetsPageTypeHeading").html("Mortgage Loan");
				//$("#defaultTenure").val(15);
				//$("#tenure").val(5);
			}
		}
</script>
<%@ include file="errorPopup.html"%>
</body>
</html>