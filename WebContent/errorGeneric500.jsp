<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>LoanGully - Simpler than others !</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ihHome.css">
<style>
@import
	url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,600);

body {
	font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 13px;
	color: #676A6C;
	overflow-x: hidden;
	text-align: center;
}

.middle-box {
	height: 400px;
	width: 400px;
	position: absolute;
	top: 50%;
	left: 50%;
	margin-top: -250px;
	margin-left: -200px;
	z-index: 100;
}

.middle-box h1 {
	font-size: 170px;
	font-weight: 100;
	color: ##E48701;
}

.middle-box h3 {
	font-size: 20px;
	font-weight: 80;
	color: ##E48701;
}

h1, .h1, h2, .h2, h3, .h3 {
	margin-top: 20px;
	margin-bottom: 10px;
}

.form-control, .single-line {
	background-color: #FFF;
	background-image: none;
	border: 1px solid #E5E6E7;
	border-radius: 1px;
	color: inherit;
	display: block;
	padding: 6px 12px;
	transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s
		ease-in-out 0s;
	width: 100%;
	font-size: 14px;
}

.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid rgba(0, 0, 0, 0);
	border-radius: 4px;
}

.form-inline .form-group {
	display: inline-block;
	margin-bottom: 0;
	vertical-align: middle;
}

input, button, select, textarea {
	font-family: inherit;
	font-size: inherit;
	line-height: inherit;
}

.btn-primary:hover, .btn-primary:focus, .btn-primary:active,
	.btn-primary.active, .open .dropdown-toggle.btn-primary {
	background-color: #18A689;
	border-color: #18A689;
	color: #FFF;
}

.m-t {
	margin-top: 15px;
}
</style>
</head>

<body class="gray-bg">

	<div class="middle-box text-center animated fadeInDown">
		<h1>500</h1>
		<h3 class="font-bold">Internal Server Error</h3>
		<!-- Mainly scripts -->
		<script>
    		function redirect2Home(){
    			window.location.href='/';
    		}
    </script>
		<div class="error-desc">
			Our sincere apologies ! <br>The server encountered a temporary
			error and could not server your request.<br>
			<h2>Kindly try checking again in sometime.</h2>
			<div class="form-group">
				Take me back to
				<button class="btn btn-primary" onclick="redirect2Home()">&nbsp;&nbsp;&nbsp;Home
					Page&nbsp;&nbsp;&nbsp;</button>
			</div>
		</div>
	</div>

	<!-- Mainly scripts -->
</body>
</html>