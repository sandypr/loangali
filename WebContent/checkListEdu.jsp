	<div class="checkListPage">

		<div class="wrapper">
			<h1 class="pageHeading">
				Checklist<br> <span>Please ensure you have following
					documents ready for Education Loans.</span>
			</h1>

			<div class="banksDealsOffersBlock">
				<table cellpadding="0" cellspacing="0" width="100%"
					class="checkListTable">
					<tr>
						<th align="center" valign="midddle" width="50%">Documents</th>
						<th width="16%" align="center" valign="midddle">Salaried</th>
						<th width="16%" align="center" valign="midddle">Self
							Employeed Professionals</th>
						<th width="18%" align="center" valign="midddle">Self
							Employeed Non-Professionals</th>
					</tr>
					<tr>
						<td align="center" valign="middle">Duly signed Application
							and Photograph</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Proof of Education
							qualifications of student</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Proof of relationship between student and co-applicant</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Last 8 months bank
							statements of co-applicant</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					
					<tr>
						<td align="center" valign="middle">Proof of business of co-applicant</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					
					<tr>
						<td align="center" valign="middle">Form 16/IT Returns of co-applicant</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle">NA</td>
					</tr>
					<tr>
						<td align="center" valign="middle">Last 2 years IT Returns
							with computation of Income</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>

					<tr>
						<td align="center" valign="middle">Last 2 years CA Certified
							/ Audited Balance Sheet and Profit &amp; Loss Account</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
				</table>

				<div class="tableBelowBlock">
					<span class="applicable"><img
						src="${pageContext.request.contextPath}/images/greenTickMark.png"
						alt="Yes">&nbsp; Applicable</span> <span class="notApplicable"><strong>NA</strong>&nbsp;
						Not Applicable</span> <a href="/web/eduLoan/"
						class="applyLoanBlueBtn">Apply Loan</a>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(function(){
			$("#headingCheckList").addClass('activeSubSubNav');
		});
	</script>
</body>
</html>
