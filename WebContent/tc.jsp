<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihTaxImplication.css">

	<div class="innerPageBannerBlock">
		<div class="wrapper" id="bannerText">		    
			<div class="bannerText homeLoanVeryBigIcon">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div> 
		</div>
	</div>

	<%-- <div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon">T & C</div>
			<div id='subsublevelnavblock'><%@ include file="subsublevelnavblock.html"%></div>  
		</div>
	</div> --%>
	<br>
<div id='faqContent'> 
<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				Terms & Conditions
			</h1>

			<div class="taxImplicationBlock">
				<p>www.LoanGully.Com Website (the "Site") - a financial advisory e-portal operated by Thincovate Software Solutions ("Etech") which operates subjective to the following conditions.</p>
				<p>1)    It's a must that you read and accept to the terms and condition as specified in Terms of Use and Privacy of Policy. Using any functionality in this site implies that you accept the Terms of Use and Privacy Policy. If you do not agree to the above, you may not use the site in any which way.</p>
				<p>2) Thincovate Software Solutions owns the sole discretion to modify/ delete/add any new terms without prior notice and should you access it multiple times it signifies you accept the most latest Terms of Usage</p>
				
				<h2>1. Description of Services</h2>
				<p>In the Site, loanGully provides users with access to informabout loans and interest rates offered by various banks. All the usage of equipment, internet facilities etc are the responsibility of the user only. By providing the necessary details you are herewith communicating your interest to take a loan and hence agree to be called over phone/mailed by relevant banks/bank agents to receive further information. You also agree to receive promotional materials and/or special offers from loanGully through email, SMS, etc.</p>

				<h2>2. License and Site Access</h2>
				<p>The license to access and make personal use of site is limited and does not include copying/downloading information  - for another individual or third party.
				<br>
					By using the Site you agree not to:  
					<ul style='list-style: disc;list-style-position: inside;'>
						<li>a. Use its contents for commercial purpose</li>
						<li>b. Make speculations or frauds</li>
						<li>c. Make unauthorized access to restricted areas</li>
					</ul>
				</p>

				<h2>3. Disclaimer of Liability and Warranty</h2>
				<p>The content, Products, and Services Published on this Site may include inaccuracies or errors, and hence we disclaim all liability for errors or inaccuracies and advice you to check with respective banks in detail before applying for a loan.</p>

				<h2>4. Local Laws</h2>
				<p>LoanGully controls and operates this Site from its headquarters in Hyderabad, India and makes no representation that the materials on the Site are appropriate or available for use in other locations. If you use this Site from other locations, you are responsible for compliance with applicable local laws including but not limited to the export and import regulations of other countries. Unless otherwise explicitly stated, all marketing or promotional materials found on this Website are solely directed to individuals, companies or other entities located in India and comply with the laws prevailing for the time being in force in India. Disputes if any shall be subject to the exclusive jurisdiction of Courts at Hyderabad.</p>

				<h2>5. Site-Provided Email and Postings</h2>
				<p>loanGully is not under no obligation to review and respond to messages posted by users on the website. Also you understand and agree not to post any content that is
					 <ul style='list-style: disc;list-style-position: inside;'>
						<li>a. Any unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, hateful, racially, ethnically or otherwise objectionable material of any kind</li>
						<li>b. Advertisements or solicitations of any kind; (iii) impersonate others or provide any kind of false information;</li>
					</ul>
				</p>

				<h2>6. Access To Password Protected/Secure Areas</h2>
				<p>Unauthorized individuals attempting to access areas restricted by use of passwords are subject to prosecution. </p>
				
				<h2>7. Trademarks</h2>
				<p>The trademarks, logos and service marks ("Marks") displayed on the Site are the property of loanGully and other parties. All information and content including any software programs available on or through the Site ("Content") is protected by copyright. Users are prohibited from modifying, copying, distributing, transmitting, displaying, publishing, selling, licensing, creating derivative works or using any Content available on or through the Site for commercial or public purposes.</p>
			</div>
		</div>
		
	</div>
</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
</body>
</html>