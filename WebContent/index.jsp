
<!-- add header -->
<%@ include file="header.html"%>

<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/gallery_scroller.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/homepagebanner.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihProfile.css">

<script type="text/jscript">
    
var tid = setInterval(tagline_vertical_slide, 2500);

// vertical slide
function tagline_vertical_slide() {
    var curr = $("#tagline ul li.active");
    curr.removeClass("active").addClass("vs-out");
    setTimeout(function() {
        curr.removeClass("vs-out");
    }, 500);

    var nextTag = curr.next('li');
    if (!nextTag.length) {
        nextTag = $("#tagline ul li").first();
    }
    nextTag.addClass("active");
}

function abortTimer() { // to be called when you want to stop the timer
    clearInterval(tid);
}
    </script>

	<div class="bannersLoansTabs">
		<div class="tagline" id="tagline">
			<span>It's time for</span>
			<ul>
				<li class="active">Being smart, Choosing smart !</li>
				<li>Sharing responsibilities !</li>
				<li>Building your nest !</li>
				<li>Hassle-free, simple sense !</li>
			</ul>
		</div>

		<!-- Start WOWSlider.com BODY section -->
		<div id="wowslider-container1">
		<!-- Sticker for financial planing : starts -->
		<div id="sticon">
				<p><a href='/web/finPlanning'>For Free <br>Financial Advice <br> <br>Click Here !</a></p>
		</div>
		<!-- Sticker for financial planing : ends -->
			<div class="ws_images">
				<ul>
					<li><img src="images/banner1.jpg" alt="Homeloans_image_1" title="" id="wows1_0" /></li>
					<li><img src="images/banner2.jpg" alt="Mortgage_image_1" title="" id="wows1_1" /></li>
					<li><img src="images/banner3.jpg" alt="Homeloans_image_2" title="" id="wows1_2" /></li>
					<!-- <li><img src="images/banner4.jpg" alt="" title="" id="wows1_3" /></li> -->
				</ul>
			</div>

			<div class="ws_bullets">
				<div>
					<a href="#" title="">1</a> <a href="#" title="">2</a> <a href="#"
						title="">3</a> <a href="#" title="">4</a>
				</div>
			</div>

		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/wowslider.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/script.js"></script>
		<!-- End WOWSlider.com BODY section -->
		<div class="bannerTopTabs">

			<div class="wrapper">
				<div class="bannersTabsBlock">
					<div class="bannersList" id="bannersList">
						<ul>
							<li><a href="#homeLoansBanner" class="bannerHomeIcon">Home
									Loan</a></li>
							<li><a href="#personalLoansBanner" class="bannerPLIcon">Personal
									Loan</a></li>
							<li><a href="#mutualFundsBanner" class="bannerMFIcon">Mortgage Loan</a></li>
							<li><a href="#insuranceBanner" class="bannerInsuranceIcon">Discounts
									& Offers</a></li>
						</ul>
						<input type='hidden' id='defaultTenure' value='' />
						<div class="bannerListContent">
							<div class="bannerEachList" id="homeLoansBanner">
								<form id='homeLoanForm'>
									<h3>Check and avail hassle free Home Loans</h3>
									<p>For best deals on Home Loans, please enter your details
										!</p>
									<div class="bannerFullForm">
										<ul class="bannerColsForm">
											<li><input type="text" class="bannerTextField numberinput" value=""
												id="monthlyIncome" name='monthlyIncome'
												placeholder="Monthly Salary/Income" maxlength="8"></li>
											<li><input type="text" id="commitments"
												name='commitments' class="bannerTextField numberinput" value=""
												placeholder="Commitments" maxlength="8"></li>
											<li><select name="resStatus" id="resStatus">
													<option value='Indian'>Indian</option>
													<option value='NRI'>NRI</option>
											</select></li>
											<li><select name="employmentType" id="employmentType">
													<option value='selectNone'>Type of Employment</option>
													<option value='salaried'>Salaried</option>
													<option value='selfEmployed'>Self Employed</option>
											</select></li>


										</ul>
										<div class="homebtn-new">
											<input type="button" id='getBestHomeDeals'
												class="bannerSendButton" value="Check Your Eligibility">
										</div>
									</div>
								</form>
							</div>
							<script type="text/javascript">
							function validatePlusRedirect(income, commitments, resStatus, empType, cat){
							
									if(income < 10000 || !$.isNumeric(Number(income))){
										//alert("Sorry ! Please enter 'Monthly Salary/Income' greater than Rs 10,000 to check your eligibility.");
										showPopUp("Error Message","Sorry ! Please enter 'Monthly Salary/Income' greater than Rs 10,000 to check your eligibility.");
									}else{
									
											if(commitments > 1 && Number(income) <= Number(commitments))
												showPopUp("Error Message","Sorry ! Commitments cannot be greater than Salary/Income. Please enter valid values to check your eligibility.");
											else{
												if(!$.isNumeric(commitments))
													commitments = 0.0;
												
												$.when(calculateEmiEligi(cat, income, commitments)).done(function(estimatedEmi){
													if(estimatedEmi > 0){
														if(isHtml5StorageSupported()){
															localStorage.setItem("income", income);
															
															if(commitments !== null && (commitments.length > 1 || $.isNumeric(Number(commitments))))
																localStorage.setItem("commitments", commitments);
																
															localStorage.setItem("resStatus", resStatus);
															localStorage.setItem("employmentType", empType);
															localStorage.setItem("categoryType", cat);
															localStorage.setItem("source","home");
															if(cat == '2')
																localStorage.setItem("defaultTenure","20");
															else if(cat == '3')
																localStorage.setItem("defaultTenure","5");
															else if(cat == '4')
																localStorage.setItem("defaultTenure","10");
															
															window.location.href = "/web/widgetsView/";
														}
													}else{
														showPopUp("Error Message",'Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change values to check your eligibility');
													}
												});
											}
									}
								}
								
							$(document).ready(function(){
								//clear
								localStorage.clear();
								
								$("#monthlyIncome").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#monthlyIncome").val());
									if(formatted !== 'Invalid Number')
										$("#monthlyIncome").val(formatted);
								});
								
								$("#commitments").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#commitments").val());
									if(formatted !== 'Invalid Number')
										$("#commitments").val(formatted);
								});
								
								$("#getBestHomeDeals").on('click', function(){
									validatePlusRedirect(currencyToNumbers($("#monthlyIncome").val()), currencyToNumbers($("#commitments").val()), $("#resStatus").val(), $("#employmentType").val(), 2);
								});
							});
						</script>
							<div class="bannerEachList" id="personalLoansBanner">
								<form id='personalLoanForm'>
									<h3>Check and avail hassle free Personal Loans</h3>
									<p>For best deals on Personal Loans, please enter your
										details !</p>
									<div class="bannerFullForm">
										<ul class="bannerColsForm">
											<li><input type="text" class="bannerTextField numberinput" value=""
												id="plMonthlyIncome" name='plMonthlyIncome'
												placeholder="Monthly Salary/Income" maxlength="8"></li>
											<li><input type="text" id="plCommitments"
												name='plCommitments' class="bannerTextField numberinput" value=""
												placeholder="Commitments" maxlength="8"></li>
											<li><select name="plResStatus" id="plResStatus"
												disabled="disabled">
													<option value='Indian'>Indian</option>
											</select></li>
											<li><select name="plEmploymentType"
												id="plEmploymentType">
													<option value='selectNone'>Type of Employment</option>
													<option value='salaried'>Salaried</option>
													<option value='selfEmployed'>Self Employed</option>
											</select></li>

										</ul>
										<div class="homebtn-new">
											<input type="button" id='getBestPersonalDeals'
												class="bannerSendButton" value="Check Your Eligibility">
										</div>
									</div>
								</form>
							</div>
							<script type="text/javascript">
							$(document).ready(function(){
								//clear
								localStorage.clear();
								
								$("#plMonthlyIncome").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#plMonthlyIncome").val());
									if(formatted !== 'Invalid Number')
										$("#plMonthlyIncome").val(formatted);
								});
								
								$("#plCommitments").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#plCommitments").val());
									if(formatted !== 'Invalid Number')
										$("#plCommitments").val(formatted);
								});
								
								$("#getBestPersonalDeals").on('click', function(){
									validatePlusRedirect(currencyToNumbers($("#plMonthlyIncome").val()), currencyToNumbers($("#plCommitments").val()), $("#plResStatus").val(), $("#plEmploymentType").val(), 3);
								});
							});
						</script>
							<div class="bannerEachList" id="mutualFundsBanner">
								<form id='mutualFundForm'>
									<h3>Check and avail hassle free Mortgage Loans</h3>
									<p>For best deals on Mortgage Loans, please enter
										your details !</p>
									<div class="bannerFullForm">
										<ul class="bannerColsForm">
											<li><input type="text" class="bannerTextField numberinput" value=""
												id="lapMonthlyIncome" name='lapMonthlyIncome'
												placeholder="Monthly Salary/Income" maxlength="8"></li>
											<li><input type="text" id="lapCommitments"
												name='lapCommitments' class="bannerTextField numberinput" value=""
												placeholder="Commitments" maxlength="8"></li>
											<li><select name="lapResStatus" id="lapResStatus" disabled="disabled">
													<option value='Indian'>Indian</option>
													
											</select></li>
											<li><select name="lapEmploymentType"
												id="lapEmploymentType">
													<option value='selectNone'>Type of Employment</option>
													<option value='salaried'>Salaried</option>
													<option value='selfEmployed'>Self Employed</option>
											</select></li>

										</ul>
										<div class="homebtn-new">
											<input type="button" id='getBestLapDeals'
												class="bannerSendButton" value="Check Your Eligibility">
										</div>
									</div>
								</form>
							</div>
							<script type="text/javascript">
							$(document).ready(function(){
								//clear
								localStorage.clear();
								
								$("#lapMonthlyIncome").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#lapMonthlyIncome").val());
									if(formatted !== 'Invalid Number')
										$("#lapMonthlyIncome").val(formatted);
								});
								
								$("#lapCommitments").on("keyup keydown change",function(){								
									var formatted = currencyFormatter($("#lapCommitments").val());
									if(formatted !== 'Invalid Number')
										$("#lapCommitments").val(formatted);
								});
								
								$("#getBestLapDeals").on('click', function(){								
									validatePlusRedirect(currencyToNumbers($("#lapMonthlyIncome").val()), currencyToNumbers($("#lapCommitments").val()), $("#lapResStatus").val(), $("#lapEmploymentType").val(), 4);
								});
							});
						</script>
							<div class="bannerEachList" id="insuranceBanner">
								<h3>Check exciting Discounts & Offers</h3>

								<p>For best deals, discounts and offers, subscribe with us !</p>
								<div class="bannerFullForm">
								<!-- <input type="email" id="email"
												name='email' class="bannerTextField" value=""
												placeholder="Enter Email"> -->
									<ul class="bannerColsForm">
											<li></li>
											<li></li>
											<li><input type="email" id="email"
												name='email' class="bannerTextField" value=""
												placeholder="Enter Email" style='width:500px;'></li>
											<li></li>
											
									</ul>
									<input type="hidden" name="name" id="name">
									<input type="hidden" name="mobile" id="mobile">
									<input type="hidden" name="subscriptionType" id="subscriptionType">
									<div class="homebtn-new">
										<input type="button"
										id='lgSubscribe' class="bannerSendButton"
										value="Subscribe with LoanGully !">
									</div>
								</div>

							</div>
							<script type="text/javascript">
							$(document).ready(function(){
								var valid= true;
								
								$("#email").on("keyup",function(){
									var val = $("#email").val();
									//alert(val);
									if(!validateEmail(val)){
										//highlightWarningField(this.id,"Please provide a valid email address !");
										valid = false;
									}else{
										removeHighlightOnField(this.id);
										valid = true;
									}
								});
								
								$("#lgSubscribe").on('click', function(){
									
									if(!(validateEmail($("#email").val())))
									{
										alert("Please enter a valid email address to subscribe !");
										
									}else{
										var data = {"name": $("#name").val(), "mobile": $("#mobile").val(), "email": $("#email").val(), "subscriptionType" : $("#subscriptionType").val()};
										
										$.ajax({
											type : "POST",
											url : "/web/subscribe",
											data : JSON.stringify(data),    
											contentType : 'application/json',
											dataType : "json",
											success : function(returnVal) {
												 if(returnVal > 0){
													 showPopUp("Information",'You have successfully subscribed with LoanGully !');
													 //console.log('success');
													 $("#email").val("");
												 }
											},
											error : function(xhr) {
												if($('#shadow').hasClass('blocker'))
												       $('#shadow').removeClass('blocker');
												redirect2Error(xhr.status);
												//alert("Oops !!!! Somethings went wrong at the server. Please contact admin for further help : "+ xhr.status);
											}
										});
									}
								});							
							});
						</script>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bannersLoansTabs bestoffer">
		<div class="wrapper">
			<ul>
				<li><span class="home-loan-txt"><img src="images/bestagent.png" alt="mission"></span>
					<span><h4>Simpler, Faster & Easier</h4></span>
					<p>LoanGully assists you in making quicker and hassle free loan
						decisions with our simplified yet scientific process</p></li>
				<li><span class="home-loan-txt"><img src="images/trust.png" alt="promise"></span>
					<span><h4>Trust & Safety</h4></span>
					<p>LoanGully values your trust and protects it. We promise that
						all information collected here is purely for proposing the best
						solutions</p></li>
				<li><span class="home-loan-txt"><img src="images/rupee.png" alt="offers"></span>
					<span><h4>Best Deals</h4></span>
					<p>LoanGully strives to serve you better and the best - with
						the latest and best deals that are apt for your dreams and needs</p></li>
			</ul>
		</div>
	</div>

	<!-- <div class="wrapper">
		<div class="bannerBottomColsBlock">
			<div class="bannerBottomCol dealsOffersBlock">
				<img src="images/dealsLatestOffers.jpg" alt="">
			</div>

			<div class="bannerBottomCol emiColBlock">
				<h2>EMI ki happy ending</h2>
				<p>Get you last 12 EMIs waived off* needs are, our personal
					loans</p>
				<span class="bankLogo"> <img src="images/axisBank.jpg" alt="">
				</span> <a href="/web/dealOffers" class="readMoreLink">read more... </a>
			</div>

			<div class="bannerBottomCol cashBackColBlock">
				<h2>Cash back</h2>
				<p>On home loans EMIs needs are, our personal loans</p>
				<span class="bankLogo"> <img src="images/icicibank.jpg"
					alt="">
				</span> <a href="/web/dealOffers" class="readMoreLink">read more... </a>
			</div>

			<div class="bannerBottomCol personalLoanColBlock">
				<h2>Personal Loans</h2>
				<p>No matter what your financial needs are, our personal loans</p>
				<span class="bankLogo"> <img src="images/kotakBank.jpg"
					alt="">
				</span> <a href="/web/dealOffers" class="readMoreLink">read more... </a>
			</div>
		</div>
	</div> -->
	
	<div style="clear:both;" ></div>
<!-- 	<hr> -->
<div class="profilePage">
		<div class="wrapper">
<div class="pageRowBlock specializationBlock">
				<h2 style='margin-top:17px;'>Our specialized Services</h2>
				<div class="specializationCol homeLoansBlock">
					<h3>Home loans</h3>
					<ol>
						<li>Purchase of Flat/house</li>
						<li>Resale</li>
						<li>Open Plot</li>
						<li>Composite Loan</li>
						<li>Top-up Loan</li>
						<li>Balance Transfer</li>
					</ol>
					<a href="/web/homeLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol personelLoansBlock">
					<h3>Personal Loans</h3>
					<ol>
						<li>House renovation</li>
						<li>Interior decoration</li>
						<li>Medical Expenses</li>
						<li>Foreign tour with family</li>
						<li>Children's education</li>
						<li>Marriage</li>
					</ol>
					<a href="/web/personalLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol mutualFundsBlock">
					<h3>Mortgage Loans</h3>
					<ol>
						<li>Residential</li>
						<li>Commercial</li>
						<li>Starting of New business</li>
						<li>Working capital</li>
						<li>LRD</li>
						<li>Balance Transfer</li>
					</ol>
					<a href="/web/lapLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol insurance">
					<h3>Calculators</h3>
					<ol>
						<li>EMI Calculator</li>
						<li>Loan Eligibility</li>
						<li>Interest Payable</li>
						<li>Amortization</li>
						<li>Tax Implications</li>
						<li>Many more</li>
					</ol>
					<a href="/web/calc/" class="readMoreButton">Explore...</a>
				</div>
			</div>
</div>
</div>
<div class="wrapper">   
      <!-- STARTS: GAl -->
      <div class="galleryScroller">
        <h1>Testimonials</h1>
        <div id="gallery_scrolle2" class="gallery_scroller">
          <div class="gallery_scroller_mask">
            <ul style="width:880px;" class="testimoni">
              <li>
                <p>Tried a few sites, before landing up here. Good and prompt response to my questions. Impressed</p>
                <p class="colorb"> <a href="#">- Santhi P,<br />
                  Kondapur, Hyderabad</a></p>
              </li>
              <li>
                <p>True to its name, its simple and its quick. At times Gully shops are better than the AC showrooms indeed</p>
                <p class="colorb"><a href="#l">- Zulfeqar Ali, <br />
                  Afzal Gunj, Hyderabad</a></p>
              </li>
              <li>
                <p>I approached them for free financial planning assistance. For a starter like me it was real good gyaan. Thanks Gully</p>
                <p class="colorb"><a href="#">- Ramya k <br />
                  Madhapur, Hyderabad</a></p>
              </li>
              <li>
                <p>Good gully to walk down for getting the best deals. Quick and efficient.</p>
                <br>
                <p class="colorb"> <a href="#">- Laxmi,<br />
                  Ramanthapur, Hyderabad</a></p>
              </li>
              <li>
                <p>I dreaded loans all my life.But now, my perspective stands changed. Thnx loanGully for making a proud owner of a nest.</p>
                <p class="colorb"><a href="#">- Aravind ch, <br />
                  ECIL, Hyderabad</a></p>
              </li>
              <li>
                <p>The kids love their new nest. Thanks loanGully for giving me the chance to own my home</p>
                <p class="colorb"><a href="#">- Bhogavalli srinivas<br />
                  Mehdipatnam, Hyderabad</a></p>
              </li>
            </ul>
          </div>
          <ul class="gallery_scroller_nav">
            <li class="left_arrow"></li>
            <li class="right_arrow"></li>
          </ul>
        </div>
      </div>
      <!-- ENDS Gal -->
</div>
<!-- 	<hr> -->
	<!-- <div class="wrapper bank-logos">
		<h1>We Partner with</h1>
		<ul>
			<li><img src="images/icicibank-bank.png" alt=""></li>
			<li><img src="images/hdfc-bank.png" alt=""></li>
			<li><img src="images/idbi-bank.png" alt=""></li>
			<li><img src="images/hdfc-home.png" alt=""></li>

			<li><img src="images/axis-bank.png" alt=""></li>
			<li><img src="images/punjab-bank.png" alt=""></li>
			<li><img src="images/indiabulls-bank.png" alt=""></li>
			<li><img src="images/federal-bank.png" alt=""></li>
		</ul>
	</div> -->
	
	<!-- <div style="height: 20px"></div> -->
	
	<!-- add header -->
	<%@ include file="footer.html"%>

	<script>
	$(function() {
		$(".numberinput").forceNumeric();
		/* $("#homeBanners").owlCarousel({
				navigation : true,
				singleItem : true
		}); */

		$('#bannersList').tabs({
			beforeActivate: function( event, ui ) {
			   $(this).find('form').trigger('reset');
			}
		});

		//.loanBigBlockOpen

		$('.loanBlockEach').on('click', function() {
			var openClass = $(this).parent().find('.loanEachBlockOpen');
			$('.loanBlocks').find('.loanEachBlockOpen').removeClass('loanEachBlockOpen');
			$(this).addClass('loanEachBlockOpen');
		});

	});
</script>
	<script>
	$(function(){
		$("#headerHome").addClass('activeTopNav');
	});
</script>
<%@ include file="errorPopup.html"%>
</body>
</html>