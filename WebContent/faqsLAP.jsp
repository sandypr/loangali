	
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				FAQs<br> <span>Check for the most Frequently Asked
					Questions on Mortgage Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>1. For what purposes can I seek a LAP?</h2>
				<p>Loan against property is similar to personal loan in the
					sense that you can take it for any purpose you desire. However, you
					should provide a commercial property that is at least 2 times the
					value of the loan you are requesting for.</p>

				<h2>2. Can I take LAP against land?</h2>
				<p>You can take LAP against commercial land but not agricultural
					land. The definition of commercial land is subject to the location
					of the land. We suggest you to talk to the concerned bank personnel
					for more details.</p>

				<h2>3. Does LAP assist in tax matters?</h2>
				<p>Usually; NO. LAP and the interest paid on LAP is not exempt
					from taxes unless used for the purpose of construction/rennovation
					of the house for which proper proof has to be submitted.</p>

				<h2>4. What is reverse mortgage loan?</h2>
				<p>Reverse mortgage loan is the loan option made available for
					senior citizens in India to ensure them a monthly/quarterly return
					on their property. This comes with a cap of Rs. 50,000 per month;
					however the good news is that this money is exempt from taxations.</p>

				<h2>5. I have availed a mortgage loan for opening a retail
					shop. Will I get any tax exemption?</h2>
				<p>While you will not get any direct tax exemption here, you can
					reduce the interest paid component of this loan from the income
					from shop in your annual returns.</p>

				<h2>6. Is it ok to buy a property mortgaged in bank?</h2>
				<p>Yes, its fine as long as you ensure the mortgage is released
					from the bank after closing the due loan completely. Make sure you
					check the EC (Encumberance certificate) and it has your name, the
					bank's name and the said owner's bank name on it.</p>

				<h2>7. What is an encumberance certificate?</h2>
				<p>An encumberance certificate usually available at the
					Registrar office gives you the list of the past 5 owners of a given
					property and how the property got transferred from one to another.
					This ensures the transfer of property is legal and accepted in the
					court of law.</p>
				<p class="applyLoanBlueBtn"><a href="/web/lapLoan/">Click here for Instant loan !</a></p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$("#headingFaqs").addClass('activeSubSubNav');
	</script>
		