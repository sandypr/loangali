<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihChecklist.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper" >		    
			<div class="bannerText homeLoanVeryBigIcon" id="bannerText">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div> 
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon" id='subnavblock_heading'>Home Loan</div>
			<div id='subsublevelnavblock'><%@ include file="subsublevelnavblock.html"%></div>  
		</div>
	</div>
	<div id='checkListContent'> 
		<div class="checkListPage">

		<div class="wrapper">
			<h1 class="pageHeading">
				Checklist<br> <span>Please ensure you have following
					documents ready for Home Loans.</span>
			</h1>

			<div class="banksDealsOffersBlock">
				<table cellpadding="0" cellspacing="0" width="100%"
					class="checkListTable">
					<tr>
						<th align="center" valign="midddle" width="50%">Documents</th>
						<th width="16%" align="center" valign="midddle">Salaried</th>
						<th width="16%" align="center" valign="midddle">Self
							Employeed Professionals</th>
						<th width="18%" align="center" valign="midddle">Self
							Employeed Non-Professionals</th>
					</tr>
					<tr>
						<td align="center" valign="middle">Duly signed Application
							and Photograph</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Proof of Age, Residence
							and Identity</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Last 6 months bank
							stagtements</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Last 3 months
							Salary-slip/Salary certificates</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle">NA</td>
					</tr>
					<tr>
						<td align="center" valign="middle">Processing fee cheque</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Form 16/IT Returns</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle">NA</td>
					</tr>
					<tr>
						<td align="center" valign="middle">Proof of business</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Business profile</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
					<tr>
						<td align="center" valign="middle">Proof of Education
							qualifications</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle">NA</td>
					</tr>

					<tr>
						<td align="center" valign="middle">Last 3 years IT Returns
							with computation of Income</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>

					<tr>
						<td align="center" valign="middle">Last 3 years CA Certified
							/ Audited Balance Sheet and Profit &amp; Loss Account</td>
						<td align="center" valign="middle">NA</td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
						<td align="center" valign="middle"><img
							src="${pageContext.request.contextPath}/images/greenTickMark.png"
							alt="Yes"></td>
					</tr>
				</table>

				<div class="tableBelowBlock">
					<span class="applicable"><img
						src="${pageContext.request.contextPath}/images/greenTickMark.png"
						alt="Yes">&nbsp; Applicable</span> <span class="notApplicable"><strong>NA</strong>&nbsp;
						Not Applicable</span> <a href="/web/homeLoan/"
						class="applyLoanBlueBtn">Apply Loan</a>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$("#headingCheckList").addClass('activeSubSubNav');	
	</script>
		
	
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		// making decision based on category what to display
		var cat = localStorage.getItem("sourceCategory");
		var div = $("<div id='subsublevelnavblock'></div>");
		if(cat == "3"){	
			$("#bannerText").html("");
			$("#bannerText").append('<span>Celebrate being</span> smarter !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("personelLoansVeryBigIcon");
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Personal Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("personelLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockPL.html");		
			$("#checkListContent").load("../checkListPL.jsp");
			$("#headerPL").addClass('activeTopNav');
		}else if(cat == "4"){
			$("#bannerText").html("");
			$("#bannerText").append('<span>Give wings </span>to your dreams !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("personelLoansVeryBigIcon");
			
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Mortgage Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("personelLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockLAP.html");
			$("#checkListContent").load("../checkListLAP.jsp");
			$("#headerLAP").addClass('activeTopNav');
			
		}else if(cat == "5"){
			$("#bannerText").html("");
			$("#bannerText").append('<span>Education is</span> LIFE itself !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("eduLoansVeryBigIcon");
			
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Education Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("eduLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockEdu.html");
			$("#checkListContent").load("../checkListEdu.jsp");
			$("#headerEL").addClass('activeTopNav');
			
		}else if(cat == "2"){
			$("#headerHL").addClass('activeTopNav');
		}
	});
</script>
</body>
</html>