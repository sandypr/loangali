	
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				FAQs<br> <span>Check for the most Frequently Asked
					Questions on Education Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>1. When do I start paying my EMIs ?</h2>
				<p>EMIs i.e repayment of principal plus interest starts after the completion of studies and grace period.</p>

				<h2>2. How much loan does I get from banks ?</h2>
				<p>Today, most of the banks are not only offering finances up to 100% of the cost of education, but also gives loans over Rs 20 Lakhs.</p>
				
				<h2>3. What is the rate of interest for Education Loan ?</h2>
				<p>Banks offer Education Loans at a very attractive rate of interest that are comparable to other products.</p>

				
				<h2>4. Can I apply for an Education Loan before admissions ?</h2>
				<p>Luckily; Yes. But only with some banks sanction Education Loan even before admissions. </p>

				<h2>5.  Why should I get an Education Loan sanction before admission ? </h2>
				<p>Because it
					<ul style='list-style-type:circle;'>
						<li>is the preferred way to declare 'Assured Funds to Universities'</li>
						<li>creates a better acceptance of the sanction letter over the solvency letter</li>
						<li>increases your chances of getting admission to your dream college</li>
						<li>avoids any last minute surprises specially during Visa time</li>
						<li>will let you know your loan eligibility in advance</li>
						<li>gives you more time and space for a hassle-free process to get a sanction letter</li>
					</ul>
				</p>

				<h2>6. Is there a Tax benefit on the interest paid on Education Loan ? Who can avail of the Tax benefit ?</h2>
				<p>Yes, every Education borrower or co-borrower is entitled for a Tax benefit under Section 80E of Income Tax Act. An individual can avail Tax benefit on interest paid on Education Loan for self or a relative.</p>

				<p class="applyLoanBlueBtn"><a href="/web/eduLoan/">Click here for Instant loan !</a></p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$("#headingFaqs").addClass('activeSubSubNav');
	</script>
		