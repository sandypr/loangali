<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihTaxImplication.css">

	<div class="innerPageBannerBlock">
		<div class="wrapper" >		    
			<div class="bannerText homeLoanVeryBigIcon" id="bannerText">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div> 
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon" id='subnavblock_heading'>Home Loan</div>
			<div id='subsublevelnavblock'><%@ include file="subsublevelnavblock.html"%></div>  
		</div>
	</div>
<div id='faqContent'> 
	
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				FAQs<br> <span>Check for the most Frequently Asked
					Questions on Home Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>1. For what purposes can I seek a first time home loan?</h2>
				<p>You can take home loan first time for either purchasing a
					new/old house/flat, for construction of a new house, rennovation/
					extension of an existing house.</p>

				<h2>2. How will your bank decide your home loan eligibility?</h2>
				<p>Bank will decide your loan eligibility based on your existing
					salary, current commitments, age and type of employment.</p>

				<h2>3. What is an EMI?</h2>
				<p>EMI stands for Equated Monthly Installment. It denotes the
					amount you should pay to the bank every month to ensure your loan
					gets repaid on time.</p>

				<h2>4. What documents are generally sought for a loan approval?</h2>
				<p>Documents sought in general are: Salary certificates, Form
					16, Identity and age proofs along with documents about the proposed
					property.</p>

				<h2>5. What are the different interest rate options offered by
					banks?</h2>
				<p>Banks offer fixed, floating and fixed+floating interest
					rates. Fixed interest rates largely remain unvaried during the loan
					period; while floating interest rates every year based on the
					regulations of RBI on interest rates. In case of fixed+floating the
					interest rates remain fixed for a given period after which they
					become floating.</p>

				<h2>6. What is an amortization schedule?</h2>
				<p>Ammortization schedule gives a month wise chart of how the
					EMI is distributed among principal and interest every month. It
					describes by how much the principal reduces every month and the
					tentative month of loan closure if you chose to increase your
					monthly EMI.</p>
					<p class="applyLoanBlueBtn"><a href="/web/homeLoan/">Click here for Instant loan !</a></p>
			</div>
		</div>
		
	</div>
	<script type="text/javascript">
		$("#headingFaqs").addClass('activeSubSubNav');
	</script>
		
</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		// making decision based on category what to display
		var cat = localStorage.getItem("sourceCategory");
		var div = $("<div id='subsublevelnavblock'></div>");
		if(cat == "3"){	
			$("#bannerText").html("");
			$("#bannerText").append('<span>Celebrate being</span> smarter !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("personelLoansVeryBigIcon");
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Personal Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("personelLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockPL.html");		
			$("#faqContent").load("../faqsPL.jsp");
			$("#headerPL").addClass('activeTopNav');
		}else if(cat == "4"){
			$("#bannerText").html("");
			$("#bannerText").append('<span>Give wings </span>to your dreams !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("personelLoansVeryBigIcon");
			
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Mortgage Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("personelLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockLAP.html");
			$("#faqContent").load("../faqsLAP.jsp");
			$("#headerLAP").addClass('activeTopNav');
		}else if(cat == "5"){
			$("#bannerText").html("");
			$("#bannerText").append('<span>Education is</span> LIFE itself !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("eduLoansVeryBigIcon");
			
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Education Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("eduLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockEdu.html");
			$("#faqContent").load("../faqsEdu.jsp");
			$("#headerEL").addClass('activeTopNav');
		}else if(cat == "2"){
			$("#headerHL").addClass('activeTopNav');
		}
	});
</script>
</body>
</html>