<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">

<div class="innerPageBannerBlock">
	<div class="wrapper">
		<div class="bannerText eduLoansVeryBigIcon">
			<span>Education is</span> LIFE itself !
		</div>
	</div>
</div>

<div class="innerPageSubNav">
	<div class="wrapper">
		<div class="subLevelNavBlock eduLoanIcon">Education Loan</div>
		<div id='subsublevelnavblock'>
			<!-- <div class="subSubLevelNavBlock">
					<span class="subSubExtraSpace"></span>
					<div class="subSubNavigation">
						<a href="/web/dealOffersPL" class="dealsOffersIcon">Deals
							&amp; Offers</a> <a href="/web/calc/"
							class="calculatorsIcon ">Calculators</a>
						activeSubSubNav
						<a href="/web/checkListPL" class="checkListIcon">Checklist</a>
						<a href="/web/taxImplicationsPL"
							class="taxImplications">Tax implications</a> <a
							href="/web/faqs" class="faqsIcon">FAQ's</a> <a
							href="#" class="newsEventsIcon">News &amp; Events</a> <a
							href="/web/feedback" class="feedbackIcon">Feedback</a>
					</div>
				</div> -->
			<%@ include file="subsublevelnavblockEdu.html"%>
		</div>
	</div>
</div>

<div class="dealsOffersPage">
	<div class="wrapper">
		<h1 class="pageHeading">
			<!-- <span>Your contact information is secured and will not be shared without your concern in any stage</span> -->
			<span>Provide your information for us to suggest you the best deals</span>
		</h1>

		<div class="applyHomeLoansSteps">
			<div class="applyHomeStepsTabs" id="applyHomeStepsTabs">
				<ul class="stepsTabsList" id='application_steps'>
					<li id='step1Tab'><a href="#step1"> <span class="stepNumber">1</span> <span
							class="stepNameInfo"
						> <strong>Step</strong> <br> <!-- Know your eligibility --> Provide your details
						</span>
					</a></li>

					<!-- <li id='step2Tab' class="disabled"><a href="#step2"> <span
								class="stepNumber">2</span> <span class="stepNameInfo"> <strong>Step</strong>
									<br> Compare & Apply
							</span>
						</a></li> -->

					<li id='step3Tab' class="disabled"><a href="#step3"> <span class="stepNumber">2</span>
							<span class="stepNameInfo"> <strong>Step</strong> <br> <!-- Confirmation -->
								Request Confirmation
						</span>
					</a></li>
				</ul>
				<form action="/loanGali/web/addEduProspect" method="post" id='prospectsForm'>
					<input type='hidden' id='categoryType' name='categoryType' value='5' /> 
					<input type='hidden' id='hasCoApplicant'  name='hasCoApplicant' value='ON' /> 
					<input type='hidden' id='prospectId' name='prospectId' value="" /> 
					<input type='hidden' id='tenure' value="" /> 
					<input type='hidden' id='defaultTenure' value="5" /> 
					<input type="hidden" id='source' />
					<div class="stepsTabContent stepOne" id="step1">
						<div class="formBlock">
							<ul class="formElementList">
							
								<h3 class="subSubHeading" id='' 
									style='font-weight: bold; color: #014b94; padding-bottom: 10px;' >Co-borrower Details</h3>
										
								<li>
									<label for="#">
										<span>Monthly Salary</span>
									</label> 
									<input type="text" id="incomeForm" class="textBox stepsTextBox numberinput" value="" maxlength="12" required > 
									<input type="hidden" name="coApplicant[monthlyIncome]" id="coApplicant_monthlyIncome" class="textBox stepsTextBox" value="" >
								</li>
								
								<li>
									<label for="for_coapplication_commitments">Commitments</label> 
									<input type="text" id="commitmentsForm" class="textBox stepsTextBox numberinput" value="" maxlength="12"> 
									<input type="hidden" name="coApplicant[commitments]" id="coApplicant_commitments" class="textBox stepsTextBox" value="0" > 
									<input type="hidden" id="resStatus" name='resStatus' class="textBox stepsTextBox" value="Indian" required >
								</li>
								
								<li>
									<label for="for_coapplication_resStatus">Residential Status</label> 
									<select name="coApplicant[resStatus]" id="coApplicant[resStatus]" class="selectBox stepsTextBox" >
										<option value="Indian">Indian</option>
										<option value="NRI">NRI</option>
									</select>
								</li>
								
								<li>
									<label for="for_coapplication_occupation">Employment Type</label> 
									<select class="selectBox stepsTextBox" name="coApplicant[occupation]" id="coApplicant_occupation" >
										<option value='selectNone'>Select Employment Type</option>
										<option value='salaried'>Salaried</option>
										<option value='selfEmployed'>Self Employed</option>
									</select>
								</li>

								<li class="salaried hideForm formRow">
									<!-- <h2 class="subSubHeading" id='heading_employmentType'></h2> -->
									<ul class="formElementList">
										<li id='for_coapplication_relation'><label for="#"><span>Relationship</span></label> 
											<select
												class="selectBox stepsTextBox" name="coApplicant[relationWithProspect]" id="coApplicant_relationWithProspect" >
													<option value="selectNone">Select Relationship with Student</option>
													<option value="Father">Father</option>
													<option value="Mother">Mother</option>
													<option value="Brother">Brother</option>
													<option value="Sister">Sister</option>
													<option value="Spouse">Spouse</option>
													<option value="Cousin">Cousin</option>
													<option value="Uncle">Uncle</option>
													<option value="Aunt">Aunt</option>
													<option value="Relative">Relative</option>
											</select>
										</li>

										<li id='for_coapplication_city'><label for="#"><span>Permanent City</span></label> 
											<select class="selectBox stepsTextBox" name="coApplicant[city]" id="coApplicant_city" >
												<option value="selectNone">Select City</option>
												<option value="Hyderabad">Hyderabad</option>
												<option value="Bangalore">Bangalore</option>
												<option value="Chennai">Chennai</option>
												<option value="Ahmedabad">Ahmedabad</option>
											</select>
										</li>
										
										<li id='for_coapplication_name'>
											<label for="#"><span>Name</span></label> 
											<input type="text" class="textBox stepsTextBox" value="" name="coApplicant[name]" id="coApplicant_name" >
										</li>
										
										<li id='for_coapplication_mobile'><label for="#"><span>Mobile</span></label> 
											<input type="text" class="textBox stepsTextBox" value="" name="coApplicant[mobile]" id="coApplicant_mobile" min='8000000000' max='9999999999' maxlength="10">
										</li>
										<!-- Student Details -->
										<h3 class="subSubHeading" id='studentDetailsHeading'
											style='font-weight: bold; color: #014b94; padding-top: 40px; padding-bottom: 10px;'
										>Student Details</h3>

										<li id='for_studentName'><label for="#"> <span>Student's Name</span></label> 
											<input type="text" class="textBox stepsTextBox" value="" id='name' name='name'>
										</li>
										
										<li id='for_workLocation'><label for="#"><span>Current Location</span></label> 
											<select class="selectBox stepsTextBox" name="workLocation" id="workLocation">
												<option value="selectNone">Select Location</option>
												<option value="Hyd">Hyderabad</option>
												<option value="Bang">Bangalore</option>
												<option value="Chen">Chennai</option>
											</select>
										</li>
										
										<li id='for_email'><label for="emailID"><span>Email ID</span></label> 
											<input type="text" id="emailAddress" name='emailAddress' class="textBox stepsTextBox" value="" >
										</li>
										
										<li id='for_mobile'><label for="mobileNumber"><span>Mobile No</span></label> 
											<input type="text" id="mobile" name='mobile' class="textBox stepsTextBox numberinput" value="" min='8000000000' max='9999999999' maxlength="10" ></li>

										<!-- Course Details -->
										<h3 class="subSubHeading" id='courseDetailsHeading' style='font-weight: bold; color: #014b94; padding-top: 40px; padding-bottom: 10px;'>Course Details</h3>

										<li id='for_courseCountry'>
											<label for="#"><span>Country of Study</span></label> 
											<select class="selectBox stepsTextBox" id="courseCountry" name="course[country]" >
												<option value="selectNone">Select Country</option>
												<option value="AF">Afghanistan</option>
												<option value="AX">Aland Islands</option>
												<option value="AL">Albania</option>
												<option value="DZ">Algeria</option>
												<option value="AS">American Samoa</option>
												<option value="AD">Andorra</option>
												<option value="AO">Angola</option>
												<option value="AI">Anguilla</option>
												<option value="AQ">Antarctica</option>
												<option value="AG">Antigua and Barbuda</option>
												<option value="AR">Argentina</option>
												<option value="AM">Armenia</option>
												<option value="AW">Aruba</option>
												<option value="AU">Australia</option>
												<option value="AT">Austria</option>
												<option value="AZ">Azerbaijan</option>
												<option value="BS">Bahamas</option>
												<option value="BH">Bahrain</option>
												<option value="BD">Bangladesh</option>
												<option value="BB">Barbados</option>
												<option value="BY">Belarus</option>
												<option value="BE">Belgium</option>
												<option value="BZ">Belize</option>
												<option value="BJ">Benin</option>
												<option value="BM">Bermuda</option>
												<option value="BT">Bhutan</option>
												<option value="BO">Bolivia, Plurinational State of</option>
												<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
												<option value="BA">Bosnia and Herzegovina</option>
												<option value="BW">Botswana</option>
												<option value="BV">Bouvet Island</option>
												<option value="BR">Brazil</option>
												<option value="IO">British Indian Ocean Territory</option>
												<option value="BN">Brunei Darussalam</option>
												<option value="BG">Bulgaria</option>
												<option value="BF">Burkina Faso</option>
												<option value="BI">Burundi</option>
												<option value="KH">Cambodia</option>
												<option value="CM">Cameroon</option>
												<option value="CA">Canada</option>
												<option value="CV">Cape Verde</option>
												<option value="KY">Cayman Islands</option>
												<option value="CF">Central African Republic</option>
												<option value="TD">Chad</option>
												<option value="CL">Chile</option>
												<option value="CN">China</option>
												<option value="CX">Christmas Island</option>
												<option value="CC">Cocos (Keeling) Islands</option>
												<option value="CO">Colombia</option>
												<option value="KM">Comoros</option>
												<option value="CG">Congo</option>
												<option value="CD">Congo, the Democratic Republic of the</option>
												<option value="CK">Cook Islands</option>
												<option value="CR">Costa Rica</option>
												<option value="CI">C�te d'Ivoire</option>
												<option value="HR">Croatia</option>
												<option value="CU">Cuba</option>
												<option value="CW">Cura�ao</option>
												<option value="CY">Cyprus</option>
												<option value="CZ">Czech Republic</option>
												<option value="DK">Denmark</option>
												<option value="DJ">Djibouti</option>
												<option value="DM">Dominica</option>
												<option value="DO">Dominican Republic</option>
												<option value="EC">Ecuador</option>
												<option value="EG">Egypt</option>
												<option value="SV">El Salvador</option>
												<option value="GQ">Equatorial Guinea</option>
												<option value="ER">Eritrea</option>
												<option value="EE">Estonia</option>
												<option value="ET">Ethiopia</option>
												<option value="FK">Falkland Islands (Malvinas)</option>
												<option value="FO">Faroe Islands</option>
												<option value="FJ">Fiji</option>
												<option value="FI">Finland</option>
												<option value="FR">France</option>
												<option value="GF">French Guiana</option>
												<option value="PF">French Polynesia</option>
												<option value="TF">French Southern Territories</option>
												<option value="GA">Gabon</option>
												<option value="GM">Gambia</option>
												<option value="GE">Georgia</option>
												<option value="DE">Germany</option>
												<option value="GH">Ghana</option>
												<option value="GI">Gibraltar</option>
												<option value="GR">Greece</option>
												<option value="GL">Greenland</option>
												<option value="GD">Grenada</option>
												<option value="GP">Guadeloupe</option>
												<option value="GU">Guam</option>
												<option value="GT">Guatemala</option>
												<option value="GG">Guernsey</option>
												<option value="GN">Guinea</option>
												<option value="GW">Guinea-Bissau</option>
												<option value="GY">Guyana</option>
												<option value="HT">Haiti</option>
												<option value="HM">Heard Island and McDonald Islands</option>
												<option value="VA">Holy See (Vatican City State)</option>
												<option value="HN">Honduras</option>
												<option value="HK">Hong Kong</option>
												<option value="HU">Hungary</option>
												<option value="IS">Iceland</option>
												<option value="IN">India</option>
												<option value="ID">Indonesia</option>
												<option value="IR">Iran, Islamic Republic of</option>
												<option value="IQ">Iraq</option>
												<option value="IE">Ireland</option>
												<option value="IM">Isle of Man</option>
												<option value="IL">Israel</option>
												<option value="IT">Italy</option>
												<option value="JM">Jamaica</option>
												<option value="JP">Japan</option>
												<option value="JE">Jersey</option>
												<option value="JO">Jordan</option>
												<option value="KZ">Kazakhstan</option>
												<option value="KE">Kenya</option>
												<option value="KI">Kiribati</option>
												<option value="KP">Korea, Democratic People's Republic of</option>
												<option value="KR">Korea, Republic of</option>
												<option value="KW">Kuwait</option>
												<option value="KG">Kyrgyzstan</option>
												<option value="LA">Lao People's Democratic Republic</option>
												<option value="LV">Latvia</option>
												<option value="LB">Lebanon</option>
												<option value="LS">Lesotho</option>
												<option value="LR">Liberia</option>
												<option value="LY">Libya</option>
												<option value="LI">Liechtenstein</option>
												<option value="LT">Lithuania</option>
												<option value="LU">Luxembourg</option>
												<option value="MO">Macao</option>
												<option value="MK">Macedonia, the former Yugoslav Republic of</option>
												<option value="MG">Madagascar</option>
												<option value="MW">Malawi</option>
												<option value="MY">Malaysia</option>
												<option value="MV">Maldives</option>
												<option value="ML">Mali</option>
												<option value="MT">Malta</option>
												<option value="MH">Marshall Islands</option>
												<option value="MQ">Martinique</option>
												<option value="MR">Mauritania</option>
												<option value="MU">Mauritius</option>
												<option value="YT">Mayotte</option>
												<option value="MX">Mexico</option>
												<option value="FM">Micronesia, Federated States of</option>
												<option value="MD">Moldova, Republic of</option>
												<option value="MC">Monaco</option>
												<option value="MN">Mongolia</option>
												<option value="ME">Montenegro</option>
												<option value="MS">Montserrat</option>
												<option value="MA">Morocco</option>
												<option value="MZ">Mozambique</option>
												<option value="MM">Myanmar</option>
												<option value="NA">Namibia</option>
												<option value="NR">Nauru</option>
												<option value="NP">Nepal</option>
												<option value="NL">Netherlands</option>
												<option value="NC">New Caledonia</option>
												<option value="NZ">New Zealand</option>
												<option value="NI">Nicaragua</option>
												<option value="NE">Niger</option>
												<option value="NG">Nigeria</option>
												<option value="NU">Niue</option>
												<option value="NF">Norfolk Island</option>
												<option value="MP">Northern Mariana Islands</option>
												<option value="NO">Norway</option>
												<option value="OM">Oman</option>
												<option value="PK">Pakistan</option>
												<option value="PW">Palau</option>
												<option value="PS">Palestinian Territory, Occupied</option>
												<option value="PA">Panama</option>
												<option value="PG">Papua New Guinea</option>
												<option value="PY">Paraguay</option>
												<option value="PE">Peru</option>
												<option value="PH">Philippines</option>
												<option value="PN">Pitcairn</option>
												<option value="PL">Poland</option>
												<option value="PT">Portugal</option>
												<option value="PR">Puerto Rico</option>
												<option value="QA">Qatar</option>
												<option value="RE">R�union</option>
												<option value="RO">Romania</option>
												<option value="RU">Russian Federation</option>
												<option value="RW">Rwanda</option>
												<option value="BL">Saint Barth�lemy</option>
												<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
												<option value="KN">Saint Kitts and Nevis</option>
												<option value="LC">Saint Lucia</option>
												<option value="MF">Saint Martin (French part)</option>
												<option value="PM">Saint Pierre and Miquelon</option>
												<option value="VC">Saint Vincent and the Grenadines</option>
												<option value="WS">Samoa</option>
												<option value="SM">San Marino</option>
												<option value="ST">Sao Tome and Principe</option>
												<option value="SA">Saudi Arabia</option>
												<option value="SN">Senegal</option>
												<option value="RS">Serbia</option>
												<option value="SC">Seychelles</option>
												<option value="SL">Sierra Leone</option>
												<option value="SG">Singapore</option>
												<option value="SX">Sint Maarten (Dutch part)</option>
												<option value="SK">Slovakia</option>
												<option value="SI">Slovenia</option>
												<option value="SB">Solomon Islands</option>
												<option value="SO">Somalia</option>
												<option value="ZA">South Africa</option>
												<option value="GS">South Georgia and the South Sandwich Islands</option>
												<option value="SS">South Sudan</option>
												<option value="ES">Spain</option>
												<option value="LK">Sri Lanka</option>
												<option value="SD">Sudan</option>
												<option value="SR">Suriname</option>
												<option value="SJ">Svalbard and Jan Mayen</option>
												<option value="SZ">Swaziland</option>
												<option value="SE">Sweden</option>
												<option value="CH">Switzerland</option>
												<option value="SY">Syrian Arab Republic</option>
												<option value="TW">Taiwan, Province of China</option>
												<option value="TJ">Tajikistan</option>
												<option value="TZ">Tanzania, United Republic of</option>
												<option value="TH">Thailand</option>
												<option value="TL">Timor-Leste</option>
												<option value="TG">Togo</option>
												<option value="TK">Tokelau</option>
												<option value="TO">Tonga</option>
												<option value="TT">Trinidad and Tobago</option>
												<option value="TN">Tunisia</option>
												<option value="TR">Turkey</option>
												<option value="TM">Turkmenistan</option>
												<option value="TC">Turks and Caicos Islands</option>
												<option value="TV">Tuvalu</option>
												<option value="UG">Uganda</option>
												<option value="UA">Ukraine</option>
												<option value="AE">United Arab Emirates</option>
												<option value="GB">United Kingdom</option>
												<option value="US">United States</option>
												<option value="UM">United States Minor Outlying Islands</option>
												<option value="UY">Uruguay</option>
												<option value="UZ">Uzbekistan</option>
												<option value="VU">Vanuatu</option>
												<option value="VE">Venezuela, Bolivarian Republic of</option>
												<option value="VN">Viet Nam</option>
												<option value="VG">Virgin Islands, British</option>
												<option value="VI">Virgin Islands, U.S.</option>
												<option value="WF">Wallis and Futuna</option>
												<option value="EH">Western Sahara</option>
												<option value="YE">Yemen</option>
												<option value="ZM">Zambia</option>
												<option value="ZW">Zimbabwe</option>
										</select></li>


										<li id='for_courseType'><label for="courseType"><span>Course Type</span></label> 
											<select class="selectBox stepsTextBox" id="courseType" name="course[courseType]" >
													<option value='selectNone'>Select Course Type</option>
													<option value='Engineering'>Engineering</option>
													<option value='MBA'>MBA</option>
													<option value='MEDICAL'>MEDICAL</option>
													<option value='HOTEL MANAGEMENT'>HOTEL MANAGEMENT</option>
													<option value='VOCATIONAL'>VOCATIONAL</option>
													<option value='CERTIFICATE'>CERTIFICATE</option>
													<option value='OTHERTYPE'>OTHERTYPE</option>
											</select>
										</li>

										<li id='for_courseDuration'><label for="courseDuration"><span>Course Duration</span></label> 
											<input type="text" class="textBox stepsTextBox employedSinceVal numberinput" style='width: 135px; float: left; margin-right: 10px;' value="" 
											name='course[courseDuration]' id='courseDuration' vpattern="^\d{1,3}$" min='1' max='9' maxlength='1' > 
												<label style="text-align: left"> (in years)</label>
										</li>

										<li id='for_courseName'><label for="courseName"><span>Course Name</span></label> 
											<input type="text" name="course[courseName]" id="courseName" maxlength="20" class="textBox stepsTextBox" value="" >
										</li>

										<li id='for_university'><label for="university"><span>University / College</span></label> 
											<input type="text" name="course[university]" id='university' class="textBox stepsTextBox" value="" >
										</li>

										<li id='for_loanAmoun'><label for="expectedSum"><span>Loan Amount</span></label> 
											<input type="text" id='loanAmountForm' class="textBox stepsTextBox numberinput" value="" >
											<input type="hidden" name="course[expectedSum]" id='loanAmount' class="textBox stepsTextBox numberinput" value="" >
										</li>

									</ul>
								</li>
								<script>
								$(document).ready(function(){
									$(".numberinput").forceNumeric();
									$(".employedSinceVal").bind("keypress keyup blur",function (event) {    
								    	$(this).val($(this).val().replace(/[^\d].+/, ""));
								            if ((event.which < 48 || event.which > 57)) {
								                event.preventDefault();
								            }
								        });
								
									// on change script
									var etype = $('#coApplicant_occupation');
									etype.on('change', function() {
										// hide all first 
										hideAll();
										clearAll();
										
										// now decide what fields have to be opened
										if (etype.val() == 'selectNone') {
											$('.formElementList').find('.showForm').removeClass('showForm').slideUp('fast');
										} else {
												
											var income = $("#coApplicant_monthlyIncome").val();
											var emis = $("#coApplicant_commitments").val();
											
											if(Number(income) < 10000){
												showPopUp("Error Message","Please enter 'Monthly Salary/Income' greater than Rs 10,000 to check your eligibility !");
												$('#coApplicant_occupation').val("selectNone");
											}else if(Number(income) <= Number(emis)){
												showPopUp("Error Message","Commitments cannot be greater than Salary/Income. Please enter valid values to check your eligibility !");
												$('#coApplicant_occupation').val("selectNone");
											}else {
												$('.formElementList').find('.showForm').removeClass('showForm').slideUp('fast');
												$('.salaried').slideDown('fast').addClass('showForm');
											}
										}
									});
									
									var forMandatory = " is a mandatory field !";
									var forNumbers = "Please enter only numbers !";
									var forEmail = "Invalid email format !";
									
									$("#incomeForm").bind("keyup keydown change",function(){
										var val = $("#incomeForm").val();
										
										if(val.length < 1){
											highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											$("#coApplicant_monthlyIncome").val(0);
										}else {
											var newVal = currencyFormatter(val);
											var unformattedNewVal = currencyToNumbers(newVal);
											if(unformattedNewVal < 10000){
												highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											}else{
												removeHighlightOnField(this.id);
												$("#incomeForm").val(newVal);											
												$("#coApplicant_monthlyIncome").val(unformattedNewVal);
											}
										}
									});
									
									$("#commitmentsForm").bind("keyup keydown change",function(){									
										$("#incomeForm").trigger("keyup");
										var val = $("#commitmentsForm").val();
										
										if(val.length > 0){
											var newVal = currencyFormatter(val);
											$("#commitmentsForm").val(newVal);											
											$("#coApplicant_commitments").val(currencyToNumbers(newVal));
										}else
											$("#coApplicant_commitments").val(0);
									});
									
									// introduced for first version	
									/* if (typeof localStorage.getItem("source") !== 'undefined' && localStorage.getItem("source") !== null && localStorage.getItem("source") !== 'NaN'){
										$("#source").val(localStorage.getItem("source"));
										$("#step2_container").html("");
										localStorage.removeItem("source");
									}else{
										$("#source").val('non_home');
										$("#application_steps").html("");
										$("#application_steps").append("<li id='step1Tab'><a href='#step1' id='step-1'> <span class='stepNumber'>1</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Provide your Details </span> </a></li><li id='step2Tab' class='disabled'><a href='#step2' id='step-2'> <span class='stepNumber'>2</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Know your Eligibility </span> </a></li> <li id='step3Tab' class='disabled'><a href='#step3' id='step-3'> <span class='stepNumber'>3</span> <span class='stepNameInfo'> <strong>Step</strong>  <br> Request Confirmation </span> </a></li>");
									} */
									
									
									/* $("#coApplicant_relationWithProspect").bind('change keyup blur', function(){
										var relation = $("#coApplicant_relationWithProspect").val();
										
										if(relation == 'selectedNone'){
											showPopUp("Error Message","Please specify a relationship with Student !");											
										}
									});
									
									$("#coApplicant_city").bind('change keyup blur', function(){
										var city = $("#coApplicant_city").val();
										
										if(city == 'selectedNone'){
											showPopUp("Error Message","Please specify Permanent City of Co-borrower !");											
										}
									}); */
									
									$("#coApplicant_relationWithProspect").on('keyup keydown change mouseup', function(){
										$("#for_coapplication_city").show();
									});
									
									$("#coApplicant_city").on('keyup keydown change mouseup', function(){
										$("#for_coapplication_name").show();
									});
									
									$("#coApplicant_name").on('keyup keydown change mouseup', function(){
										var val = $("#coApplicant_name").val();
										console.log(val);
										$("#for_coapplication_mobile").show();
										if(val.length > 2){
											removeHighlightOnField(this.id);
										}else{
											console.log("highlighting");
											highlightErrorFieldForLoans(this.id,"Required. Atleast 3 to 20 characters !");
										}
									});
									
									$("#coApplicant_mobile").bind('change keyup mouseup', function(){
										var val = $("#coApplicant_mobile").val();
										$("#studentDetailsHeading").show();
										$("#for_studentName").show();
										if(validateMobileNumber(val)){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Valid Indian mobile number !");
										}
									});
									
									// student details
									$("#name").bind('change keyup mouseup', function(){
										var val = $("#name").val();
										$("#for_workLocation").show();
										if(val.length > 2){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Atleast 3 to 20 characters !");
										}
									});
									
									$("#workLocation").bind('change keyup mouseup', function(){
										var val = $("#workLocation").val();
										$("#for_email").show();
										if(val !== 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please select a location !");
										}
									});
									
									$("#emailAddress").bind('change keyup mouseup', function(){
										var val = $("#emailAddress").val();
										$("#for_mobile").show();
										if(val.length > 7 && validateEmail(val)){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Valid Email Address !");
										}
									});
									
									
									$("#mobile").bind('change keyup mouseup', function(){
										var val = $("#mobile").val();
										$("#for_courseCountry").show();
										$("#courseDetailsHeading").show();
										if(validateMobileNumber(val)){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Valid Indian mobile number !");
										}
									});
									
									// course details									
									$("#courseCountry").bind('change keyup mouseup', function(){
										var val = $("#courseCountry").val();
										$("#for_courseType").show();
										if(val !== 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please select a Course Country !");
										}
									});
									
									$("#courseType").bind('change keyup mouseup', function(){
										var val = $("#courseType").val();
										$("#for_courseDuration").show();
										if(val !== 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please select a Course Type !");
										}
									});
									
									$("#courseDuration").bind('change keyup mouseup', function(){
										var val = $("#courseDuration").val();
										$("#for_courseName").show();
										if(val.length > 0 && val < 10){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please provide a Course Duration !");
										}
									});
									
									$("#courseName").bind('change keyup mouseup', function(){
										var val = $("#courseName").val();
										$("#for_university").show();
										if(val.length > 2){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please provide a Course Name !");
										}
									});
																		
									$("#university").bind('change keyup mouseup', function(){
										var val = $("#university").val();
										$("#for_loanAmoun").show();
										if(val.length > 2){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Please provide university or college details !");
										}
									});
									
									$("#loanAmountForm").on('change keyup mouseup',function(){
										
										var val = $("#loanAmountForm").val();
										$("#agreeTC").removeAttr("disabled");
										if(val.length < 1){
											highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											$("#loanAmount").val(0);
										}else {
											var newVal = currencyFormatter(val);
											var unformattedNewVal = currencyToNumbers(newVal);
											if(unformattedNewVal < 10000){
												highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											}else{
												removeHighlightOnField(this.id);
												$("#loanAmountForm").val(newVal);											
												$("#loanAmount").val(unformattedNewVal);
											}
										}
									});	
									

									$("#agreeTC").on('change', function(){
										if($('#agreeTC').prop('checked') == true){
											$("#getQuote").removeAttr("disabled");
											$("#getQuote").removeClass("dbSubmitButton");
											$("#getQuote").addClass("stepsSubmitButton");
										}else if($('#agreeTC').prop('checked') == false){
											$("#getQuote").removeClass("stepsSubmitButton");
											$("#getQuote").addClass("dbSubmitButton");
											$("#getQuote").attr("disabled",'disabled');
										}
									});

									$('#getQuote').click(function(){
									
										if(!$('#agreeTC').prop('checked')){
											showPopUp("Error Message",'Please read and agree to the terms and conditions !');
										}else{
											// validate all mandaroty fields again
											var commitments = currencyToNumbers($("#coApplicant_commitments").val());
											var income = currencyToNumbers($("#coApplicant_monthlyIncome").val());
											var coapplicantsName = $("#coApplicant_name").val();
											var coapplicantsMobile = $("#coApplicant_mobile").val();
											var studentsName = $("#name").val();
											var studentsLocation = $("#workLocation").val();
											var studentsEmail = $("#emailAddress").val();
											var studentsMobile = $("#mobile").val();
											var courseCountry = $("#courseCountry").val();
											var courseType = $("#courseType").val();
											var courseDuration = $("#courseDuration").val();
											var courseName = $("#courseName").val();
											var loanAmount = $("#loanAmount").val();
											var university = $("#university").val();
											
											if(coapplicantsName.length < 2 || !validateMobileNumber(coapplicantsMobile) ||
													studentsName.length < 2 || studentsLocation == 'selectNone' || !validateEmail(studentsEmail) || 
													!validateMobileNumber(studentsMobile) || courseCountry == 'selectNone' || courseType == 'selectNone' || 
													courseDuration > 9 || courseName.length < 2 || loanAmount < 10000 || university.length < 2
													){
													showPopUp("Error Message","Please fill mandatory fields highlighted in RED to proceed !");
											}else if(Number(income) < Number(commitments)){
												showPopUp("Error Message","Commitments cannot be greater than Salary/Income. Please enter valid values !");
											}else{
												//alert('submitting');
												// Commented for first version
												//sendProspectsForm();
												sendProspectAndConfirm();
											}										
										}
									});
									
									// Function to serve for lgs initial version. This will skip comparision and take forward.
									function sendProspectAndConfirm(){
										console.log(JSON.stringify($('#prospectsForm').serializeObject()));
										$.ajax({
											type : "POST",
											url : $('#prospectsForm').attr('action'),
											data : JSON.stringify($('#prospectsForm').serializeObject()),
											contentType : 'application/json',
											dataType : "text",
											success : function(returnVal) {
												 if(returnVal.length > 0)
												 	$("#prospectId").val(returnVal);
												 	
												 // activated step 2
												 $("#step3Tab").removeClass("disabled");
											 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
											 	 $("#step1Tab").addClass("disabled");
											},
											error : function(xhr) {
												if($('#shadow').hasClass('blocker'))
												       $('#shadow').removeClass('blocker');
												redirect2Error(xhr.status);
											}
										});
									
									}
									
									function sendProspectsForm(){
										var effectiveCommitments = currencyToNumbers($("#commitments").val());
										var effectiveIncome = currencyToNumbers($("#monthlyIncome").val());
										
										$.when(calculateEmiEligi($("#categoryType").val(), effectiveIncome, effectiveCommitments)).done(function(emiEligi){
											emiEligibility = emiEligi;
											if(emiEligibility < 1){
												showPopUp("Error Message","Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change the values to get a quote.");
											}else{
												//if($("#prospectId").val().length < 1){
													$.ajax({
														type : "POST",
														url : $('#prospectsForm').attr('action'),
														data : JSON.stringify($('#prospectsForm').serializeObject()),
														contentType : 'application/json',
														dataType : "text",
														success : function(returnVal) {
															 if(returnVal.length > 0)
															 	$("#prospectId").val(returnVal);
															 	
															 // activated step 2
															 $('ul.stepsTabsList li a[href="#step2"]').trigger('click');
															 // paint compare page
															 paintStep2();
														},
														error : function(xhr) {
															if($('#shadow').hasClass('blocker'))
															       $('#shadow').removeClass('blocker');
															redirect2Error(xhr.status);
														}
													});
												//}else{
													// activated step 2
												//	$("#step2Tab").removeClass("disabled");
												//	$('ul.stepsTabsList li a[href="#step2"]').trigger('click');
													// paint compare page
												//	paintStep2();
												//}
											};
										});
										
									}
									
									$("#search_byFixed").on('click',function(){
										paint(unprocessedData,emiEligibility,2);
										$("#search_byFloat").attr("style","display:block");
										$("#search_byFixed").attr("style","display:none");
									});
									
									$("#search_byFloat").on('click',function(){
										paint(unprocessedData,emiEligibility,1);
										$("#search_byFloat").attr("style","display:none");
										$("#search_byFixed").attr("style","display:block");
									});
									
									// for first version
									$("#confirm_lead").on('click',function(){
										// activated step 3
						              	 $("#step3Tab").removeClass("disabled");
									 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
									 	 $("#step1Tab").addClass("disabled");
									 	 $("#step2Tab").addClass("disabled");
									});
							});
								
								// convert user to lead with selected product
								function saveLead(name, bank, amt, tenure, product, emi, roi) {
								
									 fillProductDetailsOnStep3(bank, amt, tenure, product, emi, roi);
					              	 // activated step 2
								 	 $("#step3Tab").removeClass("disabled");
								 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
								 	 $("#step1Tab").addClass("disabled");
								 	 $("#step2Tab").addClass("disabled");
								 	 
									 var prospectId = $("#prospectId").val();
									 var categoryName = $("#categoryName").val();
									 var data = {"productName": name+' '+categoryName, "categoryName": categoryName, "amount": amt, "tenure": tenure, "productId": product, "prospectId" : prospectId, "entityName" : bank, "emi" : emi, "roi" : roi};
									 $.ajax({
									        type: "POST",
									        url: "/web/lead/addLead",
									        data: JSON.stringify(data),      // NOTE CHANGE HERE
									        contentType: "application/json; charset=utf-8",
									        dataType: "json",
									        success: function(msg) { 
									        	//nothing to do
									        	console.log('success');
									        },
									        error: function(msg) { // nothing to do
									        	console.log('failure');
									        }
									    });
								 }
								 
								 function fillProductDetailsOnStep3(bank, amt, tenure, product, emi, roi){
								 	  $("#topLoanBankStep3").html("");
				        			  $("#topLoanAmountStep3").html("");
									  $("#topTenureStep3").html("");
									  $("#topEmiStep3").html("");
									  $("#topRoiStep3").html("");
									  $("#topLoanBankStep3").append(bank);
									  $("#topLoanAmountStep3").append(round(amt,2)+" <span class='blueSmallText'>Lakhs</span>");
									  $("#topTenureStep3").append(tenure+" <span class='blueSmallText'>Years</span>");
									  $("#topEmiStep3").append(round(emi,0)+" <span class='blueSmallText'>Rs</span>");
									  $("#topRoiStep3").append(roi+" <span class='blueSmallText'>%</span>");
								 }
							</script>

								<li class="checkBoxBlock"><label for="agreeTC" class="agreeTC"> <input
										type="checkbox" id="agreeTC" class="checkbox" disabled> <span>I Agree to the <a href='/web/tc' target="_blank"
											style='text-decoration: none; font-style: italic;'
										>Terms &amp; Conditions</a></span>
								</label> <input type="button" value="Request Quote" class="dbSubmitButton" id='getQuote'
									name='getQuote' disabled="disabled"
								></li>
							</ul>
						</div>
					</div>
				</form>

				<div class="stepsTabContent stepThree" id="step3">
					<!-- <div class="stepOneInfo">
							<div class="stepOneNameInfo" style="width: 290px;">
								Welcome <strong id='prospectDisplayNameStep3'></strong><br>
								The product that interested you
							</div>

							<div class="stepOneLoanAmounts" style="width: 650px;">
								<div class="stepOneMaxLoanAmount loanAmountBlck">
									<span>Bank</span><br> <span class="loanAmountNumbers"
										id='topLoanBankStep3'></span>
								</div>
								<div class="stepOneMaxLoanAmount loanAmountBlck">
									<span>Max Loan Amount</span><br> <span
										class="loanAmountNumbers" id='topLoanAmountStep3'><span
										class="blueSmallText">Lakhs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount tenureBlck">
									<span>Tenure</span><br> <span class="loanAmountNumbers"
										id='topTenureStep3'> <span class="blueSmallText">Years</span></span>
								</div>

								<div class="stepOneMaxLoanAmount emiBlck">
									<span>EMI</span><br> <span class="loanAmountNumbers"
										id='topEmiStep3'><span class="blueSmallText">Rs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount interestRates">
									<span>Interest Rate</span><br> <span
										class="loanAmountNumbers" id='topRoiStep3'> <span
										class="blueSmallText">%</span></span>
								</div>
							</div>
						</div> -->

					<div class="stepThreeThanksBlock">
						<h3>Thanks for choosing LoanGully !</h3>
						<h4>We will reach out to you soon to process your request.</h4>
						<span>For any further assistance please drop us an email at</span>
						<h3>support@loanGully.com</h3>
						<!-- <span>For any further assistance please give us a missed call at</span>
							<h3>1800-XXX-XXXX</h3> -->
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- add footer -->
<%@ include file="footer.html"%>

<script>

		// clear all fields
		function clearAll(){
			$("#coApplicant_relationWithProspect").val('selectNone');
			$("#coApplicant_name").val("");
			removeHighlightOnField("coApplicant_name");
			$('#coApplicant_city').val('selectNone');
			$("#coApplicant_mobile").val("");
			removeHighlightOnField("coApplicant_mobile");
			
			$('#name').val('');
			removeHighlightOnField("name");
			$("#workLocation").val("selectNone");
			removeHighlightOnField("workLocation");
			$('#emailAddress').val('');
			removeHighlightOnField("emailAddress");
			$("#mobile").val("");
			removeHighlightOnField("mobile");
			
			$("#courseCountry").val("selectNone");
			removeHighlightOnField("courseCountry");
			$("#courseType").val("selectNone");
			removeHighlightOnField("courseType");
			$("#courseDuration").val("");
			removeHighlightOnField("courseDuration");
			$("#courseName").val("");
			removeHighlightOnField("courseName");
			$("#university").val("");
			removeHighlightOnField("university");
			$("#loanAmountForm").val("");
			removeHighlightOnField("loanAmount");
			
			$("#agreeTC").prop('checked', false);
			$("#getQuote").attr("disabled","disabled");
			$("#getQuote").removeClass("stepsSubmitButton");
			$("#getQuote").addClass("dbSubmitButton");
			
		}
		
		// hide all fields
		function hideAll(){
			/* $("#for_workLocation").hide();
			$("#for_yearsInCurrentLocation").hide();
			$("#for_companyName").hide();
			$("#for_designation").hide();
			$("#for_typeOfCompany").hide();
			$("#for_yearOfEstablishment").hide();
			$("#for_employedSince").hide();
			$("#for_PropertyLocation").hide();
			$("#for_purpose").hide();
			$("#for_name").hide();
			$("#for_mobile").hide();
			$("#for_age").hide();
			$("#for_email").hide(); */
			$("#for_coapplication_name").hide();
			$("#for_coapplication_mobile").hide();
			$("#for_coapplication_city").hide();
			$("#studentDetailsHeading").hide();
			$("#courseDetailsHeading").hide();
			$("#for_studentName").hide();
			$("#for_workLocation").hide();
			$("#for_email").hide();
			$("#for_mobile").hide();
			$("#for_courseCountry").hide();
			$("#for_courseType").hide();
			$("#for_courseDuration").hide();
			$("#for_courseName").hide();
			$("#for_loanAmoun").hide();
			$("#for_university").hide();
		}
	$(function() {

		$('#applyHomeStepsTabs').tabs({
			heightStyle : 'content'
		});
	});
</script>
<script>
	$(function(){
		$("#headerEL").addClass('activeTopNav');
	});
</script>
<%@ include file="errorPopup.html"%>
</body>
</html>