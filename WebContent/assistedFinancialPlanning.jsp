<!-- add header -->
<%@ include file="header.html"%>
<%@ include file="errorPopup.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihFeedBack.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon">
				<span>No dream is </span>either too big or too small !
			</div>
		</div>
	</div>
	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock homeLoansIcon">Fin Planning</div>
			<!-- <div class="subSubLevelNavBlock"></div> -->
		</div>
	</div>

<div class="checkListPage">
	<div class="wrapper">
				<div id="progress_bar" class="ui-progress-bar ui-container">
           	<div class="ui-progress" style="width: 49%;">
               	<span class="ui-label" style="display:none;">Processing <b class="value">79%</b></span>
               </div>
       	</div>
		<h1 class="pageHeading" style='padding-top:200px;'><br><!-- Financial Planning<br>
			<span> Promoted by women for women !</span> -->
		</h1>

		<div class="feedbackBlock">
			<p>As a privileged customer of LoanGully you're entitled to get free advice on financial planning.<br>We at LoanGully strive to give best possible insights for the women customers to plan their financial resources optimally for better tomorrow.</p>
            <p>Fill the below details  or Please send us your queries at <a href="#">freefinplanning@loangully.com</a></p>
		</div>
		
		<div class="feedBackForm">
		 	
		<form id="afpForm" action="/web/submitAFP"
							method="post">
			<ul>
				<li>
					<label for="afp_name"><span>Name</span></label>
					<input type="text" id="afp_name" value="" class="textbox feedBackFormTextBox" name="name">
				</li>
				<li>
					<label for="afp_email"><span>Email ID</span></label>
					<input type="email" id="afp_email" value="" class="textbox feedBackFormTextBox valid" name="email">
				</li>
				<li>
					<label for="afp_mobile"><span>Mobile Number</span></label>
					<input type="text" id="afp_mobile" value="" class="textbox feedBackFormTextBox numberinput" name="mobile" maxlength="10">
				</li>
				<li class="fullWidthList">
					<label for="afp_qns"><span>Questions</span></label>
					<textarea name="qns" id="afp_qns" class="textarea feedBackFormTextBox" id="" cols="30" rows="10" name="qns"></textarea>
				</li>
				<li class="fullWidthList">
					<input type="button" class="submitButton" value="Submit" id="afpBtn"/>
				</li>
			</ul>
			</form>
			  <script>
			  	$(document).ready(function(){
			  		
			  		$('#progress_bar').hide();
			  		
			  		$.validator.addMethod("validateMobile", function(value) {
						return (value.length == 10  && value.substring(0,1) == "7" || value.substring(0,1) == "8" || value.substring(0,1) == "9")
					},'Please Enter Valid Mobile Number');
					
					$("#afpForm").validate({
						rules: {
							name: "required",
							mobile: {
								required: true,
								maxlength:10,
								validateMobile: true
							},
							qns: "required",
							
						},
  					    errorPlacement: function(error, element) {
								// Append error within linked label
							$("span", element.parent()).addClass("error_show");
							element.attr("title",error.text());
						  },
						  
						  highlight: function(element) {
							  $("span", $(element).parent()).addClass("error_show");
						  },
						  
						  unhighlight: function(element) {
							  $("span", $(element).parent()).removeClass("error_show");
							  $(element).removeAttr("title");
						  },
						  
						  errorElement: "span",
							messages: {
								name: "Name is Mandatory",
								mobiel:  {
									required: "Mobile is Mandatory",
									validateMobile: "Please Enter Valid Mobile Number"
								},
								qns: "Questions is Mandatory"
							}
						
					});
					$('#afpBtn').click(function(e){
						if($("#afpForm").valid()){
							//$("#afpForm").hide();
							$.ajax({
								type : "POST",
								url : $('#afpForm').attr('action'),
								data : JSON.stringify($('#afpForm').serializeObject()),
								contentType : 'application/json',
								dataType : "text",
								success : function() {
									showPopUp("Information","Thanks for writing to us! Our expert team will respond to your request at the earliest !");
								},
								error : function(xhr) {
									if($('#shadow').hasClass('blocker'))
										$('#shadow').removeClass('blocker');
										redirect2Error(xhr.status);
								}
							});
							
							$("#afpForm :input").prop("disabled", true);
							$('#progress_bar').show();
							 $('#progress_bar .ui-progress').css('width', '4%').
						     	animateProgress(43, function() {
						          $(this).animateProgress(79, function() {
						            setTimeout(function() {
						                $('#progress_bar .ui-progress').animateProgress(100, function() {
						                $('#progress_bar').hide();
						                $('#main_content').slideDown();
						                //$("#afpForm").show();
						                $("#afpForm :input").prop("disabled", false);
										$(this).closest('form').find("input[type=text], textarea").val("");
										$("#afpForm").trigger('reset');
						              });
						            }, 2000);
						          });
						   });
							
						}else{
							showPopUp("Error Message","Please fill valid data in fields highlighted in RED to proceed !!");
							event.preventDefault(); 
						}
					});
				});
								
			  </script>					 
		</div>
	</div>
</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		$(".numberinput").forceNumeric();
		//$("#headerRefer").addClass('activeTopNav');
	});
</script>
</body>
</html>
