
<!-- add header -->
<%@ include file="header.html"%>
<%@ include file="errorPopup.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihFeedBack.css">

	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon">
				<span>No dream is </span>either too big or too small !
			</div>
		</div>
	</div>
	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock homeLoansIcon">Referral</div>
			<!-- <div class="subSubLevelNavBlock"></div> -->
		</div>
	</div>

<div class="checkListPage">
		<div class="wrapper">
		<div class="stepThreeThanksBlock" id="thanku" style="display: none">
					<h3>Thanks for writing to us!</h3>
					<span>We value your feedback and suggestions/questions would
						be responded to at the earliest</span>
					<h3>1800-XXX-XXXX</h3>
				</div>
				 <div id="progress_bar" class="ui-progress-bar ui-container">
                 	<div class="ui-progress" style="width: 49%;">
                 		<span class="ui-label" style="display:none;">Processing <b class="value">79%</b></span>
                 	</div>
                 </div>
		<form id="referralFrm" action="/web/submitReferrals" method="post">
			<!-- <h1 class="pageHeading">Referral Rewards<br>
				<span>Efficient and cost-effective way to your <b>Loans</b></span>
			</h1> -->
			<h1 class="pageHeading" style='padding-top: 200px;'><br><span>Efficient and cost-effective way to your <b>Loans</b></span></h1>
			
<!-- 			<div class="applyHomeLoansSteps">
			<div class="applyHomeStepsTabs" id="applyHomeStepsTabs">
				<div class="stepsTabContent stepTwo" id="step2" style="margin-top:-20px">
					<div class="termLoanEMITableBlck">
					
                    <h1 class="pageHeading">How does it work </h1>
						<table cellpadding="0" cellspacing="0" width="100%" class="termLoanEMITable">
							<tr>
								<th width="20%" valign="middle">Step 1</th>
								<td valign="middle">Share the details of your friend/relative/colleague who is looking for a loan with us !<span class="blueSmallText"></span></td>
							</tr>
							<tr>
								<th>Step 2</th>
								<td>As soon as he/she avails a loan through LoanGully, a confirmation email will be sent to you !<span class="blueSmallText"></span></td>
							</tr>
							<tr>
								<th>Step 3</th>
								<td>And, we would surprise you with an attractive gift at your doorsteps !</td>
							</tr>
						</table>
						</div>
						</div>
						</div>
						</div> -->
			
				<div class="feedBackForm" id="feedbackForm">

					<input type='hidden' id='ref_categoryType' name='categoryType'
								value='2' class="valid"/> 
                            <h1>Your friend's Details</h1>
                        
                        
							<ul>
                            <li>
                                <label for="referredName">
                                <span>Name</span></label>
                                <input id='ref_referredName' type="text" value="" class="textbox feedBackFormTextBox" name="referredName" >
                            </li> 
                            <li>
                                <label for="referredEmail"><span>Email ID</span></label>
                                <input id='ref_referredEmail'  type="email" value="" class="textbox feedBackFormTextBox" name="referredEmail">
                            </li>
                            <li>
                                <label for="referredMobile"><span>Mobile Number</span></label>
                                <input id='ref_referredMobile' type="text" value="" class="textbox feedBackFormTextBox numberinput" name="referredMobile" maxlength="10">
                            </li>
                            <li>
                                <label for="referredLocation"><span>Location</span></label>
                                <input id='ref_referredLocation' type="text" value="" class="textbox feedBackFormTextBox valid" name="referredLocation">
                            </li>
                            <br><br>
                            <h1>Your Details</h1>                            
                            <li>
                                <label for="referrer"><span>Name</span></label>
                                <input id="ref_referrer" type="text" value="" class="textbox feedBackFormTextBox" name="referrer">
                            </li>
                            <li>
                                <label for="referrerEmail"><span>Email ID</span></label>
                                <input id="ref_referrerEmail" type="email" value="" class="textbox feedBackFormTextBox valid" name="referrerEmail">
                            </li>
                            <li>
                                <label for="referrerMobile"><span>Mobile Number</span></label>
                                <input id="ref_referrerMobile" type="text" value="" class="textbox feedBackFormTextBox numberinput" name="referrerMobile" maxlength="10">
                            </li>
                            <li>
                                <label for="referrerLocation"><span>Location</span></label>
                                <input id="ref_referrerLocation" type="text" value="" class="textbox feedBackFormTextBox valid" name="referrerLocation">
                            </li>
                            
                            <li class="fullWidthList">
                                <label for="referrerAddress"><span>Address</span></label>
                                <textarea id="ref_referrerAddress" name="referrerAddress" class="textarea feedBackFormTextBox valid" cols="30" rows="10"></textarea>
                            </li>
                            <li class="fullWidthList">
                                <input type="button" class="submitButton" value="Submit" id="submitbtn"/>
                               
                            </li>
                             </ul>
				</div>
				
				<script>
						$(document).ready(function(){
								$('#progress_bar').hide();
								 
								$.validator.addMethod("validateMobile", function(value) {
									return (value.length == 10  && value.substring(0,1) == "7" || value.substring(0,1) == "8" || value.substring(0,1) == "9")
								},'Please Enter Valierd Mobile number');
								
								$("#referralFrm").validate({
									rules: {
										referredName: "required",
										referredEmail: "required",
										referredMobile: {
										  required:true,
										  maxlength:10,
										  validateMobile: true
										},
										referrer: "required",
										referrerMobile: {
									    	required:true,
											maxlength:10,
											validateMobile: true
										},
									  },
									  
									  errorPlacement: function(error, element) {
											// Append error within linked label
											$("span", element.parent()).addClass("error_show");
											element.attr("title",error.text());
									  },
									  
									  highlight: function(element) {
										  $("span", $(element).parent()).addClass("error_show");
									  },
									  
									  unhighlight: function(element) {
											 $("span", $(element).parent()).removeClass("error_show");
											 $(element).removeAttr("title");
									  },
									  
									  errorElement: "span",
										messages: {
											referredName: "Name is Mandatory",
											referrer:  "Referrer Name is Mandatory",
											referredMobile: {
									             required: "Referred Mobile is Mandatory",
									             validateMobile: "Please Enter valid Mobile number"
											},
											referredEmail: {
									             required: "Email is Mandatory",
									             email: "Please Enter a valid Email address"
											},
											referrerMobile: {
									             required: "Referrer Mobile is Mandatory",
									             validateMobile: "Please Enter valid Mobile number"
											},
										},
								});
								$('#submitbtn').click(function(e){
								    
									if($("#referralFrm").valid()){
									    //$("#referralFrm").hide();
									    $.ajax({
											type : "POST",
											url : $('#referralFrm').attr('action'),
											data : JSON.stringify($('#referralFrm').serializeObject()),
											contentType : 'application/json',
											dataType : "text",
											success : function() {
												showPopUp("Information","Thanks for writing to us! Our team would contact your relative/friend at the earliest !");
												//$("#referralFrm").show();
											},
											error : function(xhr) {
												if($('#shadow').hasClass('blocker'))
												    $('#shadow').removeClass('blocker');
													redirect2Error(xhr.status);
												}
										});
									    
									    $("#referralFrm :input").prop("disabled", true);
										$('#progress_bar').show();
										 $('#progress_bar .ui-progress').css('width', '4%').
									        animateProgress(43, function() {
									          $(this).animateProgress(79, function() {
									            setTimeout(function() {
									              $('#progress_bar .ui-progress').animateProgress(100, function() {
									                $('#progress_bar').hide();
									                $('#main_content').slideDown();
									                $("#referralFrm :input").prop("disabled", false);
													$(this).closest('form').find("input[type=text], textarea").val("");
													$("#referralFrm").trigger('reset');
									              });
									            }, 2000);
									          });
									      });
										
									}else{
										showPopUp("Error Message","Please fill valid data in fields highlighted in RED to proceed !!");
										event.preventDefault(); 
									}
								});
							});
			</script>


			</form>
			
		</div>
	</div>
	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		$(".numberinput").forceNumeric();
		$("#headerRefer").addClass('activeTopNav');
	});
</script>
	
</body>
</html>