<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.simplePopup.js"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"></script>
<script src="${pageContext.request.contextPath}/js/lg-methods.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">

<div class="innerPageBannerBlock">
	<div class="wrapper">
		<div class="bannerText homeLoanVeryBigIcon"><span>L.O.A.N - </span> L<span>ets</span>
			O<span>wn </span>A N<span>est</span></div>
</div>
</div>

<div class="innerPageSubNav">
	<div class="wrapper">
		<div class="subLevelNavBlock homeLoansIcon">Home Loan</div>
		<div id='subsublevelnavblock'><%@ include
				file="subsublevelnavblock.html"%></div>
</div>
</div>

<div class="dealsOffersPage">
	<div class="wrapper">
		<h1 class="pageHeading">
			<!-- <span>Your contact information is secured and will not be shared without your concern in any stage</span> -->
			<span>Provide your information for us to suggest you the best deals</span>
	</h1>

		<div class="applyHomeLoansSteps">
			<div class="applyHomeStepsTabs" id="applyHomeStepsTabs">
				<ul class="stepsTabsList" id='application_steps'>
					<li id='step1Tab'><a href="#step1" id='step-1'> <span
							class="stepNumber">1</span> <span class="stepNameInfo"> <strong>Step</strong>
								<br> <!-- Know your eligibility --> Provide your details
						</span>
					</a></li>

					<!-- <li id='step2Tab' class="disabled"><a href="#step2"
							id='step-2'> <span class="stepNumber">2</span> <span
								class="stepNameInfo"> <strong>Step</strong> <br>
									Compare & Apply
							</span>
						</a></li> -->

					<li id='step3Tab' class="disabled"><a href="#step3" id='step-3'> <span
							class="stepNumber">2</span> <span class="stepNameInfo"> <strong>Step</strong>
								<br> <!-- Confirmation --> Request Confirmation
						</span>
					</a></li>

			</ul> <script type="text/javascript">
						/* $(document).ready(function(){
							if (typeof localStorage.getItem("source") !== 'undefined' && localStorage.getItem("source") !== null && localStorage.getItem("source") !== 'NaN'){
								$("#source").val(localStorage.getItem("source"));
								$("#step2_container").html("");
								localStorage.clear();
							}else{
								$("#source").val('non_home');
								$("#application_steps").html("");
								$("#application_steps").append("<li id='step1Tab'><a href='#step1' id='step-1'> <span class='stepNumber'>1</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Provide your details </span> </a></li><li id='step2Tab' class='disabled'><a href='#step2' id='step-2'> <span class='stepNumber'>2</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Know your Eligibility </span> </a></li> <li id='step3Tab' class='disabled'><a href='#step3' id='step-3'> <span class='stepNumber'>3</span> <span class='stepNameInfo'> <strong>Step</strong>  <br> Request Confirmation </span> </a></li>");
							}
							//alert($("#source").val());
						}); */
					</script>
				<form action="/web/addProspect" method="post" id='prospectsForm'><input
					type='hidden' id='categoryType' name='categoryType' value='2' /> <input
					type='hidden' id='categoryName' value='Housing Loan' /> <input
					type='hidden' id='prospectId' name='prospectId' value="" /> <input
					type='hidden' id='tenure' value="" /> <input type='hidden'
					id='defaultTenure' value="20" /> <input type='hidden' id='reqEligibility'
					value="0.0" /> <input type="hidden" id='source' />
					<div class="stepsTabContent stepOne" id="step1">
						<div class="formBlock">
							<ul class="formElementList">
								<li><label for="#"><span>Monthly Salary</span></label> <input
									type="text" id="monthlyIncomeForm"
									class="textBox stepsTextBox  numberinput" value="" maxlength="12"
									required> <input type="hidden" id="monthlyIncome"
									name='monthlyIncome' class="textBox stepsTextBox" value=""></li>
								<li><label for="#">Commitments</label> <input type="text"
									id="commitmentsForm" class="textBox stepsTextBox numberinput" value=""
									maxlength="12"> <input type="hidden" id="commitments"
									name='commitments' class="textBox stepsTextBox" value="0"></li>
								<li><label for="resStatus">Residential Status</label> <select
									name="resStatus" id="resStatus" class="selectBox stepsTextBox">
										<option value="Indian">Indian</option>
										<option value="NRI">NRI</option>
								</select></li>
								<li><label for="#">Employment Type</label> <select
									class="selectBox stepsTextBox" name="employmentType"
									id="employmentType">
										<option value='selectNone'>Select Employment Type</option>
										<option value='salaried'>Salaried</option>
										<option value='selfEmployed'>Self Employed</option>
								</select></li>
								<li class="salaried hideForm formRow">
									<h2 class="subSubHeading" id='heading_employmentType'></h2>
									<ul class="formElementList">
										<li id='for_companyName'><label for="#">Company Name</label> <input
											type="text" name="company" id="company" class="textBox stepsTextBox"
											value=""></li>
										<li id='for_designation'><label for="#">Designation</label> <input
											type="text" class="textBox stepsTextBox" value="" id="designation"
											name="designation"></li>
										<li id='for_typeOfCompany'><label for="#">Type of Company</label>
											<select class="selectBox stepsTextBox" name="typeOfCompany"
											id="typeOfCompany">
												<option value="selectNone">Select Type of Company</option>
												<option value="proprietorship">Proprietorship</option>
												<option value="partnership">Partnership</option>
												<option value="privateltd">Pvt. Ltd.</option>
												<option value="ltdcompany">Ltd. Company</option>
												<option value="llp">LLP</option>
										</select></li>
										<li id='for_yearOfEstablishment'><label for="#">Year of
												Establishment</label> <input type="text"
											class="textBox stepsTextBox numberinput" value=""
											id='yearOfEstablishment' name='yearOfEstablishment' maxlength="4">
									</li>
										<li id='for_workLocation'><label for="#">Working Location</label>
											<select class="selectBox stepsTextBox" name="workLocation"
											id="workLocation">
												<option value="selectNone">Select Work Location</option>
												<option value="Hyd">Hyderabad</option>
												<option value="Bang">Bangalore</option>
												<option value="Chen">Chennai</option>
										</select></li>
										<li id='for_yearsInCurrentLocation'><label
											for="#yearsInCurrentLocation">Years In Current Loc</label> <input
											type="text" class="textBox stepsTextBox numberinput" value=""
											id='yearsInCurrentLocation' name='yearsInCurrentLocation'
											maxlength="2"></li>
										<li id='for_employedSince'><label for="employedSinceMonths">Employed
												Since</label> <input type="text" class="textBox stepsTextBox numberinput"
											style='width: 135px; float: left; margin-right: 10px;' value=""
											id='employedSinceMonths' name='employedSinceMonths'
											placeholder="Months" pattern="^\d{1,3}$" min='1' max='720'
											maxlength='3'><label style="text-align: left"> (in
												months)</label></li>
										<li id='for_PropertyLocation'><label for="#">Property
												Location</label> <select class="selectBox stepsTextBox" name="assetLocation"
											id="assetLocation">
												<option value="selectNone">Select Property Location</option>
												<option value="hyd">Hyderabad</option>
												<option value="bang">Bangalore</option>
												<option value="chen">Chennai</option>
										</select></li>
										<li id='for_purpose'><label for="purpose">Purpose</label> <select
											class="selectBox stepsTextBox" name="purpose" id="purpose">
												<option value="selectNone">Select Purpose</option>
												<option value="purchase_of_flat_house">Purchase of
													flat/House</option>
												<option value="purchase_of_open_plot">Purchase of open plot</option>
												<option value="resale">Resale</option>
												<option value="commercial">Commercial</option>
												<option value="balance_transfer">Balance Transfer</option>
										</select></li>
										<li id='for_propertyCost'><label for="propertyCost">Property
												Cost</label> <input type="text" id="propertyCostForm"
											class="textBox stepsTextBox numberinput"
											placeholder="Enter Property Cost in Rs" value="" min="1" maxlength="14">
											<input type="hidden" id="propertyCost" name="propertyCost"
											class="textBox stepsTextBox" placeholder="Enter Property Cost"
											value="" min="1"></li>
										<li id='for_name' style='clear:both;'><label for="name_label"><span>Name</span></label>
											<input type="text" id="name" name="name" class="textBox stepsTextBox"
											placeholder="Enter your name" maxlength="20" value=""></li>
										<li id='for_mobile'><label for="mobileNumber"><span>Mobile
													No</span></label> <input type="text" id="mobile" name='mobile'
											class="textBox stepsTextBox numberinput"
											placeholder="Enter your mobile number" value="" min='8000000000'
											max='9999999999' maxlength="10"></li>
										<li id='for_email'><label for="emailID"><span>Email
													ID</span></label> <input type="text" id="emailAddress" name='emailAddress'
											class="textBox stepsTextBox" placeholder="Enter your email id"
											value=""></li>
										<li id='for_age'><label for="age"><span>Age</span></label> <!--  <input
												type="text" id="dateOfBirth" name='dateOfBirth'
												class="textBox stepsTextBox"
												placeholder="Enter your date of birth" value=""> --> <input
											type="text" id="age" name='age'
											class="textBox stepsTextBox numberinput"
											style='width: 135px; float: left; margin-right: 10px;'
											placeholder="Enter your age" maxlength='2' value=""><label
											style="text-align: left;"> (in years)</label></li>

								</ul>
							</li>
								<script>
								$(document).ready(function(){
									$(".numberinput").forceNumeric();
									// on change script
									var etype = $('#employmentType');
									etype.on('change', function() {
										//alert('triggered');
										// hide all first 
										hideAll();
										clearAll();
										
										// now decide what fields have to be opened
										if (etype.val() == 'selectNone') {
											$('.formElementList').find('.showForm').removeClass('showForm').slideUp('fast');
											$('#hasCoApplicant').prop('checked',false);
										} else {												
											var income = $("#monthlyIncome").val();
											var emis = $("#commitments").val();
											
											if(Number(income) < 10000){
												showPopUp("Error Message","Please enter 'Monthly Salary/Income' greater than Rs 10,000 to check your eligibility !");
												$('#employmentType').val("selectNone");
											}else if(Number(income) <= Number(emis)){
												showPopUp("Error Message","Commitments cannot be greater than Salary/Income. Please enter valid values to check your eligibility !");
												$('#employmentType').val("selectNone");
											}else {
												$('.formElementList').find('.showForm').removeClass('showForm').slideUp('fast');
												$('.salaried').slideDown('fast').addClass('showForm');
												
												if (etype.val() == 'salaried') {
												
													// change the heading
													$("#heading_employmentType").html("");
													$("#heading_employmentType").html("Salaried");
													// start with companyname
													$("#for_companyName").show();
													
												}else if (etype.val() == 'selfEmployed') {
													
													// change the heading
													$("#heading_employmentType").html("");
													$("#heading_employmentType").html("Self Employed");
													// start with same fields
													$("#for_companyName").show();
													
												}
											}
										}
									});
									
									// on change for resdential status
									$('#resStatus').on('change', function() {
										//alert('chaning res status');
										$('.formElementList').find('.showForm').removeClass('showForm').slideUp('fast');
										hideAll();clearAll();
										$('#employmentType').val('selectNone');
									});
									
									// on click for coApplicant
									$("#hasCoApplicant").on("click", function(){
										if($('#hasCoApplicant').prop('checked') && $('#employmentType').val() != 'selectNone') {
											$('.coAplicant').slideDown('fast').addClass('showForm');
											$("#coApplicant").val("true");
										} else {
										   $('.coAplicant').removeClass('showForm').slideUp('fast');
										   $('#hasCoApplicant').prop('checked',false);
										   $("#coApplicant").val("false");
										   clearCoApplication();			   
										}
									});
									var forMandatory = " is a mandatory field !";
									var forNumbers = "Please enter only numbers !";
									var forEmail = "Invalid email format !";
									
									$("#monthlyIncomeForm").bind("keyup keydown change",function(){
										var val = $("#monthlyIncomeForm").val();
										
										if(val.length < 1){
											highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											$("#monthlyIncome").val(0);
										}else {
											var newVal = currencyFormatter(val);
											var unformattedNewVal = currencyToNumbers(newVal);
											if(unformattedNewVal < 10000){
												highlightErrorFieldForLoans(this.id,"Required. Atleast Rs 10,000");
											}else{
												removeHighlightOnField(this.id);
												$("#monthlyIncomeForm").val(newVal);											
												$("#monthlyIncome").val(unformattedNewVal);
											}
										}
									});
									
									$("#commitmentsForm").bind("keyup keydown change",function(){	
										$("#monthlyIncomeForm").trigger("keyup");
										var val = $("#commitmentsForm").val();
										
										if(val.length > 0){
											var newVal = currencyFormatter(val);
											$("#commitmentsForm").val(newVal);											
											$("#commitments").val(currencyToNumbers(newVal));
										}else
											$("#commitments").val(0);
									});
									
									if (typeof localStorage.getItem("income") !== 'undefined' && localStorage.getItem("income") !== null){
										$("#monthlyIncomeForm").val(localStorage.getItem("income"));
										$("#monthlyIncomeForm").trigger("keyup");
									}
										
									if (typeof localStorage.getItem("commitments") !== 'undefined' && localStorage.getItem("commitments") !== null){
										$("#commitmentsForm").val(localStorage.getItem("commitments"));
										$("#commitmentsForm").trigger("keyup");
									}
										
									if (typeof localStorage.getItem("resStatus") !== 'undefined' && localStorage.getItem("resStatus") !== null && localStorage.getItem("resStatus") !== 'NaN')
										$("#resStatus").val(localStorage.getItem("resStatus"));
									else
										$("#resStatus").val('Indian');
									
									if (typeof localStorage.getItem("defaultTenure") !== 'undefined' && localStorage.getItem("defaultTenure") !== null){
										$("#defaultTenure").val(localStorage.getItem("defaultTenure"));
									}
									
									if (typeof localStorage.getItem("employmentType") !== 'undefined' && localStorage.getItem("employmentType") !== null && localStorage.getItem("employmentType") !== 'NaN' ){
										$("#employmentType").val(localStorage.getItem("employmentType"));
										//alert('triggering');
										$("#employmentType").trigger('change');
									}else
										$("#employmentType").val('selectNone');
									
									// introduced for first version	
									if (typeof localStorage.getItem("source") !== 'undefined' && localStorage.getItem("source") !== null && localStorage.getItem("source") !== 'NaN'){
										$("#source").val(localStorage.getItem("source"));
										$("#step2_container").html("");
										$("#getQuote").attr("value", "Get Quote");
										localStorage.removeItem("source");
									}else{
										$("#source").val('non_home');
										$("#application_steps").html("");
										$("#application_steps").append("<li id='step1Tab'><a href='#step1' id='step-1'> <span class='stepNumber'>1</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Provide your details </span> </a></li><li id='step2Tab' class='disabled'><a href='#step2' id='step-2'> <span class='stepNumber'>2</span> <span class='stepNameInfo'> <strong>Step</strong> <br> Know your Eligibility </span> </a></li> <li id='step3Tab' class='disabled'><a href='#step3' id='step-3'> <span class='stepNumber'>3</span> <span class='stepNameInfo'> <strong>Step</strong>  <br> Request Confirmation </span> </a></li>");
									}
										
									$("#company").bind('change keyup blur', function(){
										var val = $("#company").val();										
										if(val.length > 1){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory);
										}
										if(( $('#resStatus').val() == 'Indian' || $('#resStatus').val() == 'NRI' ) && $('#employmentType').val() != 'selectNone'){
											if($('#employmentType').val() == 'salaried'){
												$("#for_workLocation").show();
											}else if($('#employmentType').val() == 'selfEmployed' ){
												$("#for_designation").show();
											}
										}									
									});		
									
									$("#workLocation").on('change',function(){
									
										var val = $("#workLocation").val();
										if(val != 'selectNone'){
											removeHighlightOnField(this.id);
											if( $('#employmentType').val() != 'selectNone'){
											
												if($('#employmentType').val() == 'salaried'){
												
													if($('#resStatus').val() == 'Indian' )
														$("#for_employedSince").show();
													else if($('#resStatus').val() == 'NRI')
														$("#for_yearsInCurrentLocation").show();
														
												}else if($('#employmentType').val() == 'selfEmployed'){
												
													if($('#resStatus').val() == 'Indian' )
														$("#for_PropertyLocation").show();
													else if($('#resStatus').val() == 'NRI')
														$("#for_yearsInCurrentLocation").show();
												}
											}
										}else{
											//highlightWarningField(this.id,forMandatory,"Working-Location");
										}
									});	
									
									
									$("#designation").bind('change keyup blur',function(){
									
										var val = $("#designation").val();
										if(( $('#resStatus').val() == 'Indian' || $('#resStatus').val() == 'NRI' ) 
													&& $('#employmentType').val() != 'selectNone'){
											if($('#employmentType').val() == 'selfEmployed' ){
												$("#for_typeOfCompany").show();
											}
										}
										if(val.length > 1){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Designation");
										}
									});
									
									$("#typeOfCompany").bind('change keyup blur',function(){
										
										var val = $("#typeOfCompany").val();
										if(( $('#resStatus').val() == 'Indian' || $('#resStatus').val() == 'NRI' ) 
													&& $('#employmentType').val() != 'selectNone' ){
											if($('#employmentType').val() == 'selfEmployed' ){
												$("#for_yearOfEstablishment").show();
											}
										}
										if(val != 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Type-Of-Company");
										}
									});	
									
									$("#yearOfEstablishment").bind('change keyup blur',function(){
										var val = $("#yearOfEstablishment").val();
										if(( $('#resStatus').val() == 'Indian' || $('#resStatus').val() == 'NRI' ) && $('#employmentType').val() == 'selfEmployed'){
											// show next row fields
											$("#for_workLocation").show();
										}
										if(val.length == 2 || val.length == 4){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Year-Of-Establishment");
											$("#"+this.id).removeAttr("title");
											$("#"+this.id).attr("title","Recommended values between 1800 - "+(new Date()).getFullYear()+" !");
										}
									});
									
									$("#employedSinceMonths").bind('change keyup blur',function(event){
										
										var val = $("#employedSinceMonths").val();
										
										if($('#employmentType').val() == 'salaried' && ( $('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian' ) ){
											$("#for_PropertyLocation").show();
										}
										
										if($.isNumeric(val) && (val.length < 4) && (val > 0 && val < 720)){
											//removeHighlightOnField(this.id);
											$("#"+this.id).removeAttr("style");
											$("#"+this.id).attr("style","width:135px; float:left; margin-right: 10px;");
											$("#"+this.id).removeAttr("title");
											
										}else{
											////highlightWarningField(this.id,forMandatory);
											//$("#"+this.id).attr("style","border-color : red; width:135px; float:left; margin-right: 10px;");
											$("#"+this.id).removeAttr("title");
											$("#"+this.id).attr("title","Recommended values between 1 - 720 !");
										}
									});
									
									$("#assetLocation").on('change',function(){
										var val = $("#assetLocation").val();
										if( ( $('#employmentType').val() == 'salaried' || $('#employmentType').val() == 'selfEmployed' ) 
																&& ( $('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian' )){
												$("#for_purpose").show();
										}
										if(val != 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Property-Location");
										}
									});	
									
									$("#yearsInCurrentLocation").bind('change keyup blur', function(){
										var val = $("#yearsInCurrentLocation").val();
										if($('#resStatus').val() == 'NRI' && $('#employmentType').val() != 'selectNone'){										
											if($('#employmentType').val() == 'selfEmployed'){
												$("#for_PropertyLocation").show();													
											}else if($('#employmentType').val() == 'salaried'){
												$("#for_employedSince").show();
											}
										}
										if(val.length == 1 || val.length == 2){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Years-In-Current-Location");
											$("#"+this.id).removeAttr("title");
											$("#"+this.id).attr("title","Recommended values between 1 - 65 !");
										}
									});		
									
									$("#purpose").change(function(){
										var val = $("#purpose").val();
										if( $('#employmentType').val() == 'salaried' || $('#employmentType').val() == 'selfEmployed' ) {
											if($('#resStatus').val() == 'NRI' ){
												$("#for_name").show();
											}else if($('#resStatus').val() == 'Indian' ){
												$("#for_propertyCost").show();
											}
										}
										if(val != 'selectNone'){
											removeHighlightOnField(this.id);
										}else{
											//highlightWarningField(this.id,forMandatory,"Purpose");
										}
									});
								
									$("#propertyCostForm").bind('change keyup keydown blur',function(){
										
										var formatted = currencyFormatter($("#propertyCostForm").val());
										if(formatted !== 'Invalid Number')
											$("#propertyCostForm").val(formatted);
										
										if($('#resStatus').val() == 'Indian' && ($('#employmentType').val() == 'salaried' || $('#employmentType').val() == 'selfEmployed') ){
											// show next row fields
											$("#for_name").show();
										}
										
										var val = currencyToNumbers($("#propertyCostForm").val());
										if($.isNumeric(val) && String(val).length > 4){
											removeHighlightOnField(this.id);
											$("#propertyCost").val(val);											
											//$("#propertyCostForm").val(val);											
										}else{
											//highlightWarningField(this.id,forMandatory,"Property-Cost");
										}
									});
									
									$("#name").bind('change keyup', function(){
										var val = $("#name").val();
										if($('#employmentType').val() != 'selectNone' && ($('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian')){
											$("#for_mobile").show();
										}
										if(val.length > 2){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Atleast 3 to 20 characters !");
										}
									});
									
									$("#mobile").bind('change keyup', function(){
										var val = $("#mobile").val();
										if($('#employmentType').val() != 'selectNone' && ($('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian')){
											$("#for_email").show();
										}
										if(validateMobileNumber(val)){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Valid Indian mobile number !");
										}
									});	
									
									$("#emailAddress").bind('change keyup', function(){
										var val = $("#emailAddress").val();
										if($('#employmentType').val() != 'selectNone' && ($('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian')){
											$("#for_age").show();
										}
										if(val.length > 7 && validateEmail(val)){
											removeHighlightOnField(this.id);
										}else{
											highlightErrorFieldForLoans(this.id,"Required. Valid Email Address !");
										}
									});	
									
									$("#age").bind('change keyup', function(){
										var val = $("#age").val();
										var error_element=$("span", $("#age").parent());
										if($.isNumeric(val) && val.length > 1 && val.length < 3){
											
											if(Number(val) < 18 || Number(val) > 65){ // limiting age between 18-65
												error_element.addClass("error_show");
												$(this).removeClass("valid").addClass("invalid");
												
												$("#"+this.id).removeAttr("title");
												$("#"+this.id).attr("title","Required. Numeric values between 18 - 65 !");
											}else{
												error_element.removeClass("error_show");
												$("#"+this.id).removeAttr("title");
												
												if($('#employmentType').val() != 'selectNone' && ($('#resStatus').val() == 'NRI' || $('#resStatus').val() == 'Indian')){
													$("#getQuote").removeAttr("disabled");
													$("#getQuote").removeClass("dbSubmitButton");
													$("#getQuote").addClass("stepsSubmitButton");
												}
											}
										}else{
											error_element.addClass("error_show");
											$(this).removeClass("valid").addClass("invalid");
											
											$("#"+this.id).removeAttr("title");
											$("#"+this.id).attr("title","Required. Numeric values between 18 - 65 !");
										}
									});
										
									$('#getQuote').click(function(){
										
										if(!$('#agreeTC').prop('checked')){
											showPopUp("Error Message",'Please read and agree to the terms and conditions !');
										}else{
											// validate all mandaroty fields again
											var age = $("#age").val();
											var email = $("#emailAddress").val();
											var mobile = $("#mobile").val();
											var name = $("#name").val();
											var commitments = currencyToNumbers($("#commitments").val());
											var income = currencyToNumbers($("#monthlyIncome").val());

											if((age.length < 2 || age < 18 || age > 65) || !validateEmail(email) || !validateMobileNumber(mobile)
																|| name.length < 3 || commitments.length < 1 || !$.isNumeric(Number(commitments))
																|| income.length < 1 || !$.isNumeric(Number(income)) )
												showPopUp("Error Message","Please fill the mandatory fields highlighted in RED to proceed !!");
											else if(Number(income) < Number(commitments)){
												showPopUp("Error Message","Commitments cannot be greater than Salary/Income. Please enter valid values to get quote  !");
											}else{
												if($('#hasCoApplicant').prop('checked')){
													var mobile = $("#coApplicant_mobile").val();
													var cincome = currencyToNumbers($("#coApplicant_monthlyIncome").val());
													
													if(!validateMobileNumber(mobile)){
														highlightErrorFieldForLoans("coApplicant_mobile","Required. Valid Indian mobile number !");
													}else{
														removeHighlightOnField("coApplicant_mobile");
													}
													
													if(cincome.length < 4 || !$.isNumeric(cincome)){
														highlightErrorFieldForLoans("coApplicant_monthlyIncomeForm","Required. Greater than 1000 !");
													}
													
													if(!validateMobileNumber(mobile) || cincome.length < 1 || !$.isNumeric(cincome)){
														showPopUp("Error Message","Please fill the mandatory fields highlighted in RED to proceed !!");
													}else{
														// Commented for first version
														//sendProspectsForm();
														var source = $("#source").val();
														if(source === 'home')
															sendProspectAndConfirm();
														else
															sendProspectsForm();
													}
												}else{
													// Commented for first version
													//sendProspectsForm();
													var source = $("#source").val();
														if(source === 'home')
															sendProspectAndConfirm();
														else
															sendProspectsForm();
												}
											}
										}
									});
									
									// Function to serve for lgs initial version. This will skip comparision and take forward.
									function sendProspectAndConfirm(){
									
										var effectiveCommitments = currencyToNumbers($("#commitments").val());
										var effectiveIncome = currencyToNumbers($("#monthlyIncome").val());
										
										if($('#hasCoApplicant').prop('checked')){
											var cIncome = currencyToNumbers($("#coApplicant_monthlyIncome").val());
											var cCommitments = ($.isNumeric($("#coApplicant_commitments").val()))? currencyToNumbers($("#coApplicant_commitments").val()) : 0;
											
											effectiveIncome = effectiveIncome + cIncome;
											effectiveCommitments = effectiveCommitments + cCommitments;
										}
										
										$.when(calculateEmiEligi($("#categoryType").val(), effectiveIncome, effectiveCommitments)).done(function(emiEligi){

											emiEligibility = emiEligi;
											if(emiEligibility < 1){
												showPopUp("Error Message","Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change the values to get a quote.");
											}else{
												//if($("#prospectId").val().length < 1){
												//console.log(JSON.stringify($('#prospectsForm').serializeObject()));
													$.ajax({
														type : "POST",
														url : $('#prospectsForm').attr('action'),
														data : JSON.stringify($('#prospectsForm').serializeObject()),
														contentType : 'application/json',
														dataType : "text",
														success : function(returnVal) {
															 if(returnVal.length > 0)
															 	$("#prospectId").val(returnVal);
															 	
															 // activated step 2
															 $("#step3Tab").removeClass("disabled");
														 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
														 	 $("#step1Tab").addClass("disabled");
														},
														error : function(xhr) {
															if($('#shadow').hasClass('blocker'))
															       $('#shadow').removeClass('blocker');
															redirect2Error(xhr.status);
														}
													});
											};
										});
									
									}
									
									function sendProspectsForm(){
									
										var effectiveCommitments = currencyToNumbers($("#commitments").val());
										var effectiveIncome = currencyToNumbers($("#monthlyIncome").val());
										
										if($('#hasCoApplicant').prop('checked')){
											var cIncome = currencyToNumbers($("#coApplicant_monthlyIncome").val());
											var cCommitments = ($.isNumeric($("#coApplicant_commitments").val()))? currencyToNumbers($("#coApplicant_commitments").val()) : 0;
											
											effectiveIncome = effectiveIncome + cIncome;
											effectiveCommitments = effectiveCommitments + cCommitments;
										}
										
										$.when(calculateEmiEligi($("#categoryType").val(), effectiveIncome, effectiveCommitments)).done(function(emiEligi){

											emiEligibility = emiEligi;
											if(emiEligibility < 1){
												showPopUp("Error Message","Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change the values to get a quote.");
											}else{
												//if($("#prospectId").val().length < 1){
												//console.log(JSON.stringify($('#prospectsForm').serializeObject()));
													$.ajax({
														type : "POST",
														url : $('#prospectsForm').attr('action'),
														data : JSON.stringify($('#prospectsForm').serializeObject()),
														contentType : 'application/json',
														dataType : "text",
														success : function(returnVal) {
															 if(returnVal.length > 0)
															 	$("#prospectId").val(returnVal);
															 	
															 // activated step 2
															 $('ul.stepsTabsList li a[href="#step2"]').trigger('click');
															 // paint compare page
															 paintStep2();
														},
														error : function(xhr) {
															if($('#shadow').hasClass('blocker'))
															       $('#shadow').removeClass('blocker');
															redirect2Error(xhr.status);
														}
													});
												//}else{
													// activated step 2
													//$("#step2Tab").removeClass("disabled");
													//$('ul.stepsTabsList li a[href="#step2"]').trigger('click');
													// paint compare page
													//paintStep2();
												//}
											};
										});
										
									}
									
									$("#search_byFixed").on('click',function(){
										paint(unprocessedData,emiEligibility,2);
										$("#search_byFloat").attr("style","display:block");
										$("#search_byFixed").attr("style","display:none");
									});
									
									$("#search_byFloat").on('click',function(){
										paint(unprocessedData,emiEligibility,1);
										$("#search_byFloat").attr("style","display:none");
										$("#search_byFixed").attr("style","display:block");
									});
									
									// for first version
									$("#confirm_lead").on('click',function(){
										// activated step 3
						              	 $("#step3Tab").removeClass("disabled");
									 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
									 	 $("#step1Tab").addClass("disabled");
									 	 $("#step2Tab").addClass("disabled");
									});
							});
								
								// convert user to lead with selected product
								function saveLead(name, bank, amt, tenure, product, emi, roi) {
								
									 fillProductDetailsOnStep3(bank, amt, tenure, product, emi, roi);
					              	 // activated step 3
					              	 $("#step3Tab").removeClass("disabled");
								 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
								 	 $("#step1Tab").addClass("disabled");
								 	 $("#step2Tab").addClass("disabled");
								 	 
									 var prospectId = $("#prospectId").val();
									 var categoryName = $("#categoryName").val();
									 var data = {"productName": name+' '+categoryName, "categoryName": categoryName, "amount": amt, "tenure": tenure, "productId": product, "prospectId" : prospectId, "entityName" : bank, "emi" : emi, "roi" : roi};
									 //console.log(date);
									 $.ajax({
									        type: "POST",
									        url: "/web/lead/addLead",
									        data: JSON.stringify(data),      // NOTE CHANGE HERE
									        contentType: "application/json; charset=utf-8",
									        dataType: "json",
									        success: function(msg) { 
									        	//nothing to do
									        	//console.log('success');
									        },
									        error: function(msg) { // nothing to do
									        	//console.log('failure');
									        }
									    });
								 }
								 
								 function fillProductDetailsOnStep3(bank, amt, tenure, product, emi, roi){
								 	  $("#topLoanBankStep3").html("");
				        			  $("#topLoanAmountStep3").html("");
									  $("#topTenureStep3").html("");
									  $("#topEmiStep3").html("");
									  $("#topRoiStep3").html("");
									  $("#topLoanBankStep3").append(bank);
									  $("#topLoanAmountStep3").append(round(amt,2)+" <span class='blueSmallText'>Lakhs</span>");
									  $("#topTenureStep3").append(tenure+" <span class='blueSmallText'>Years</span>");
									  $("#topEmiStep3").append(round(emi,0)+" <span class='blueSmallText'>Rs</span>");
									  $("#topRoiStep3").append(roi+" <span class='blueSmallText'>%</span>");
								 }
							</script>

								<li class="coAplicant hideForm formRow">
									<h2 class="subSubHeading">Co Applicant <span>Add
											co-applicant income for higher loan amount</span>
								</h2>
									<ul class="formElementList">
										<li><label for="name">Employment</label> <select
											class="selectBox stepsTextBox" name="coApplicant[occupation]"
											id="coApplicant_occupation">
												<option value="selectNone">Employment Type</option>
												<option value="salaried">Salaried</option>
												<option value="selfEmployed">Self Employed</option>
										</select></li>
										<li><label for="">Company Name</label> <input type="text"
											class="textBox stepsTextBox" name="coApplicant[company]"
											id="coApplicant_company" placeholder="Company Name"></li>
										<li style='clear:both;'><label for="#"><span>Monthly Salary</span></label> <input
											type="text" class="textBox stepsTextBox numberinput"
											id="coApplicant_monthlyIncomeForm" placeholder="" value=""
											maxlength="12"> <input type="hidden"
											class="textBox stepsTextBox" name="coApplicant[monthlyIncome]"
											id="coApplicant_monthlyIncome" placeholder="" value=""></li>
										<li><label for="#">Monthly Commitments</label> <input type="text"
											class="textBox stepsTextBox numberinput"
											id="coApplicant_commitmentsForm" value="" maxlength="12"> <input
											type="hidden" class="textBox stepsTextBox"
											name="coApplicant[commitments]" id="coApplicant_commitments" value=""></li>
										<li><label for="#">Email ID</label> <input type="text"
											class="textBox stepsTextBox" name="coApplicant[email]"
											id="coApplicant_email" value="" placeholder="Email Id"></li>
										<li><label for="#"><span>Mobile Number</span></label> <input
											type="text" value="" class="textBox stepsTextBox numberinput"
											name="coApplicant[mobile]" id="coApplicant_mobile" placeholder=""
											maxlength="10"></li>

								</ul>
							</li>

								<li class="checkBoxBlock"><label for="hasCoApplicant"
									class="coApplication"> <input type="checkbox"
										id="hasCoApplicant" name="hasCoApplicant" class="checkbox"> <span>Add
											a co-applicant</span> <br /> <!-- <input type="checkbox" id="agreeTC" class="checkbox"> 
										<span style='color : #024c93;'>I Agree to the Terms &amp; Conditions</span> -->
								</label> <label for="agreeTC" class="agreeTC"> <input type="checkbox"
										id="agreeTC" class="checkbox"> <span>I Agree to the <a
											href='/web/tc' target="_blank" style='text-decoration:none;font-style: italic;'>Terms &amp; Conditions</a></span>
								</label> <input type="button" value="Get Eligibility" style="margin-top: 26px;"
									class="dbSubmitButton" id='getQuote' name='getQuote'
									disabled='disabled'></li>
								<script>
						
								$("#coApplicant_occupation").on("keyup",function(){
									var val = $("#coApplicant_occupation").val();
									if(val == 'selectNone'){
										//highlightWarningField(this.id,forMandatory,"CoApplicant-Occupation");
									}else{
										removeHighlightOnField(this.id);
									}
								});
								
								$("#coApplicant_company").on("keyup",function(){
									var val = $("#coApplicant_company").val();
									if(val.length < 2){
										//highlightWarningField(this.id,forMandatory,"CoApplicant-Company");
									}else{
										removeHighlightOnField(this.id);
									}
								});
								
								$("#coApplicant_monthlyIncomeForm").on("blur",function(){
									var val = $("#coApplicant_monthlyIncomeForm").val();
									if(val.length < 1)
										//highlightErrorField(this.id,"Please fill valid values for CoApplicant-Income !");
										highlightErrorFieldForLoans(this.id,"Required. Greater than 1000 !");
								});
								
								$("#coApplicant_monthlyIncomeForm").on("keyup",function(){
									var formatted = currencyFormatter($("#coApplicant_monthlyIncomeForm").val());
									if(formatted !== 'Invalid Number')
										$("#coApplicant_monthlyIncomeForm").val(formatted);
									
									var val = currencyToNumbers($("#coApplicant_monthlyIncomeForm").val());
									if($.isNumeric(val) && String(val).length > 3){
										removeHighlightOnField(this.id);
										$("#coApplicant_monthlyIncome").val(val);											
									}else{
										//highlightErrorField(this.id,"Please fill valid values for CoApplicant-Income !");
										highlightErrorFieldForLoans(this.id,"Required. Greater than 1000 !");
									}
								});
								
								/* $("#coApplicant_commitmentsForm").on("blur",function(){
									var val = $("#coApplicant_commitmentsForm").val();
									if(val.length < 1)
										highlightErrorField(this.id,"Please fill valid values for CoApplicant-Commitments !");
								}); */
								
								$("#coApplicant_commitmentsForm").on("keyup",function(){
									var formatted = currencyFormatter($("#coApplicant_commitmentsForm").val());
									if(formatted !== 'Invalid Number')
										$("#coApplicant_commitmentsForm").val(formatted);
									
									var val = currencyToNumbers($("#coApplicant_commitmentsForm").val());
									if($.isNumeric(val) && String(val).length > 2){
										removeHighlightOnField(this.id);
										$("#coApplicant_commitments").val(val);											
									}else{
										//highlightErrorField(this.id,"Please fill valid values for CoApplicant-Commitments !");
									}
								});
								
								$("#coApplicant_email").on("keyup",function(){
									var val = $("#coApplicant_email").val();
									if(!validateEmail(val)){
										//highlightWarningField(this.id,"Please provide a valid email address !");
									}else{
										removeHighlightOnField(this.id);
									}
								});
								
								$("#coApplicant_mobile").on("keyup",function(){
									var val = $("#coApplicant_mobile").val();
									if(!validateMobileNumber(val)){
										//highlightErrorField(this.id,"Please fill a valid mobile number !");
										highlightErrorFieldForLoans(this.id,"Required. Valid Indian mobile number !");
									}else{
										removeHighlightOnField(this.id);
									}
								});
								
							</script>
						</ul>
					</div>
				</div></form>
				<div id="step2_container">
					<div class="stepsTabContent stepTwo" id="step2">
						<div class="stepOneInfo">
							<div class="stepOneNameInfo">Welcome <strong
								id='prospectDisplayName'></strong><br> Your Max HomeLoan
								Eligibility details
						</div>

							<div class="stepOneLoanAmounts">
								<div class="stepOneMaxLoanAmount loanAmountBlck"><span>Max
										Loan Amount</span><br> <span class="loanAmountNumbers"
									id='topLoanAmount'><span class="blueSmallText">Lakhs</span></span></div>

								<div class="stepOneMaxLoanAmount tenureBlck"><span>Tenure</span><br>
									<span class="loanAmountNumbers" id='topTenure'> <span
										class="blueSmallText">Years</span></span></div>

								<div class="stepOneMaxLoanAmount emiBlck"><span>EMI</span><br>
									<span class="loanAmountNumbers" id='topEmi'><span
										class="blueSmallText">Rs</span></span></div>

								<div class="stepOneMaxLoanAmount interestRates"><span>Interest
										Rate</span><br> <span class="loanAmountNumbers" id='topRoi'> <span
										class="blueSmallText">%</span></span></div>
						</div>
					</div>

						<div class="loainAmountGraph">
							<table>
								<tr>
									<td><div id="gaugeAmt" style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeTenure" style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeEmi" style="width: 230px; height: 180px"></div></td>
									<td><div id="gaugeRoi" style="width: 230px; height: 180px"></div></td>
							</tr>
						</table>
					</div>

						<div class="elgibilityTentureBlock">
							<div class="elgibilityBlock">
								<div class="elgibilityText">Eligibility</div>
								<div class="elgibilityProgressBar" id="eligibilitySlider"></div>
								<div class="elgibilityNumber" id="eligibilityAmountForSlider"></div>
						</div>

							<div class="elgibilityBlock">
								<div class="elgibilityText">Tenure</div>
								<div class="elgibilityProgressBar" id="tenureSlider"
								style='float: left;'></div>
								<div class="elgibilityNumber" id="tenureForSlider"></div>
						</div>
					</div>

						<div class="termLoanEMITableBlck">
							<table cellpadding="0" cellspacing="0" width="100%"
							class="termLoanEMITable">
								<tr>
									<th width="20%" valign="middle">Term</th>
									<td width="16%" valign="middle" id='col11'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col21'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col31'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col41'><span
										class="blueSmallText">Yrs</span></td>
									<td width="16%" valign="middle" id='col51'><span
										class="blueSmallText">Yrs</span></td>
							</tr>
								<tr>
									<th>Loan Amount</th>
									<td id='col12'><span class="blueSmallText">Lks</span></td>
									<td id='col22'><span class="blueSmallText">Lks</span></td>
									<td id='col32'><span class="blueSmallText">Lks</span></td>
									<td id='col42'><span class="blueSmallText">Lks</span></td>
									<td id='col52'><span class="blueSmallText">Lks</span></td>
							</tr>
								<tr>
									<th>EMI</th>
									<td id='col13'></td>
									<td id='col23'></td>
									<td id='col33'></td>
									<td id='col43'></td>
									<td id='col53'></td>
							</tr>
						</table>
					</div> <!-- commented for first version --> <!-- <div class="bestDealsBlock">
							<div class="bestDealsTextButton">
								<div class="bestDealsTxt">
									<strong>Best Deals</strong><br> Sorted Based on the Lowest ->
									Highest ROI
								</div>

								<div class="bestDealsSearchButton">
									<a href="javascript:void(0)" id='search_byFixed'>Search for
										fixed tenure</a> <a href="javascript:void(0)" id='search_byFloat'
										style='display: none;'>Search for floating tenure</a>
								</div>
							</div>

							<div class="bestDealsBankTableBlck">
								<table cellpadding="0" cellspacing="0" width="100%"
									class="bestDealsBankTable" id='result_table'>
								</table>
							</div>
						</div> -->
						<div class="bestDealsBlock" style='border-top: 0px solid;'>
							<div class="bestDealsTextButton">
								<div class="bestDealsSearchButton"><a href="javascript:void(0)"
									id='confirm_lead'>Confirm My Request</a></div>
						</div>
					</div>
				</div>
			</div>

				<div class="stepsTabContent stepThree" id="step3">
					<!-- <div class="stepOneInfo">
							<div class="stepOneNameInfo" style="width: 290px;">
								Welcome <strong id='prospectDisplayNameStep3'></strong><br>
								The product that interested you
							</div>

							<div class="stepOneLoanAmounts" style="width: 650px;">
								<div class="stepOneMaxLoanAmount loanAmountBlck">
									<span>Bank</span><br> <span class="loanAmountNumbers"
										id='topLoanBankStep3'></span>
								</div>
								<div class="stepOneMaxLoanAmount loanAmountBlck">
									<span>Max Loan Amount</span><br> <span
										class="loanAmountNumbers" id='topLoanAmountStep3'><span
										class="blueSmallText">Lakhs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount tenureBlck">
									<span>Tenure</span><br> <span class="loanAmountNumbers"
										id='topTenureStep3'> <span class="blueSmallText">Years</span></span>
								</div>

								<div class="stepOneMaxLoanAmount emiBlck">
									<span>EMI</span><br> <span class="loanAmountNumbers"
										id='topEmiStep3'><span class="blueSmallText">Rs</span></span>
								</div>

								<div class="stepOneMaxLoanAmount interestRates">
									<span>Interest Rate</span><br> <span
										class="loanAmountNumbers" id='topRoiStep3'> <span
										class="blueSmallText">%</span></span>
								</div>
							</div>
						</div> -->

					<div class="stepThreeThanksBlock">
						<h3>Thanks for choosing LoanGully !</h3>
						<h4>We will reach out to you soon to process your request.</h4> <span>For
							any further assistance please drop us an email at</span>
						<h3>support@loanGully.com</h3> <!-- <span>For any further assistance please give us a missed call at</span>
							<h3>1800-XXX-XXXX</h3> -->
				</div>
			</div>
		</div>
	</div>

</div>
</div>

<!-- add footer -->
<%@ include file="footer.html"%>

<script>

		// clear all fields
		function clearAll(){
			$("#company").val("");
			$("#designation").val("");
			$('#typeOfCompany').val('selectNone');
			$("#yearOfEstablishment").val("");
			$('#workLocation').val('selectNone');
			$("#yearsInCurrentLocation").val("");
			$("#employedSinceMonths").val("");
			$('#assetLocation').val('selectNone');
			$('#purpose').val('selectNone');
			$("#propertyCost").val("");
			$("#propertyCostForm").val("");
			$("#name").val("");
			removeHighlightOnField("name");
			$("#mobile").val("");
			removeHighlightOnField("mobile");
			$("#emailAddress").val("");
			removeHighlightOnField("emailAddress");
			$("#age").val("");
			removeHighlightOnField("age");
			
			$("#agreeTC").prop('checked', false);
			$("#getQuote").attr("disabled","disabled");
			$("#getQuote").removeClass("stepsSubmitButton");
			$("#getQuote").addClass("dbSubmitButton");
			
			clearCoApplication();
		}
		
		// clear co applicatn fields
		function clearCoApplication(){
			$("#coApplicant_company").val("");
			$("#coApplicant_mobile").val("");
			removeHighlightOnField("coApplicant_mobile");
			$("#coApplicant_email").val("");
			$("#coApplicant_age").val("");
			$("#coApplicant_monthlyIncome").val("");
			$("#coApplicant_commitments").val("");
			$("#coApplicant_monthlyIncomeForm").val("");
			removeHighlightOnField("coApplicant_monthlyIncomeForm");
			$("#coApplicant_commitmentsForm").val("");
			$('#hasCoApplicant').prop('checked',false);
		}
		
		// hide all fields
		function hideAll(){
			$("#for_workLocation").hide();
			$("#for_yearsInCurrentLocation").hide();
			$("#for_companyName").hide();
			$("#for_designation").hide();
			$("#for_typeOfCompany").hide();
			$("#for_yearOfEstablishment").hide();
			$("#for_employedSince").hide();
			$("#for_PropertyLocation").hide();
			$("#for_propertyCost").hide();
			$("#for_purpose").hide();
			$("#for_name").hide();
			$("#for_mobile").hide();
			$("#for_age").hide();
			$("#for_email").hide();
		}
	$(function() {

		$('#applyHomeStepsTabs').tabs({
			heightStyle : 'content'
		});
		
	});
	
</script>
<script>
	$(function(){
		$("#headerHL").addClass('activeTopNav');
	});
</script>
<%@ include file="errorPopup.html"%>
</body>
</html>
