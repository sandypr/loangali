<!-- add header -->
<%@ include file="header.html"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihProfile.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon">
				<span>No dream is </span>either too big or too small !
			</div>
		</div>
	</div>
	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock homeLoansIcon">Profile</div>
			<!-- <div class="subSubLevelNavBlock"></div> -->
		</div>
	</div>


	<div class="profilePage">
		<div class="wrapper">
			<!-- <h1 class="pageHeading" style="padding-bottom:4px;">Process</h1>
			<p style="font-size:13px;">
				LoanGully makes loan process <span style='font-style: italic; font-weight:600; color:#0073be'>simple,
					comprehensive and easy  to avail</span> with innovative 3C Process
			</p> -->
			
			<div class="pageRowBlock yourSearchText">
				<p align="center"> <span style='font-style: bold; font-weight:600; color:#0073be'>"It takes nothing to join the crowd. It takes everything to stand alone."</span> -Hans F Hansen</p>
					<br>
				<h1 class="pageHeading" style="padding-bottom:4px;">Process</h1>
				<p>LoanGully makes loan process <span style='font-style: italic; font-weight:600; color:#0073be'>simple,
					comprehensive and easy  to avail</span> with innovative 3C Process</p>
				<br>
			</div>
			<br>
			<div class="pageRowBlock profileThreeCols">
				<div class="profileCol researchBlock">
					<h2>Check Your eligibility</h2>
					<p>With your financial strengths and commitments</p>
				</div>

				<div class="profileCol investBlock">
					<h2>Compare</h2>
					<p>In the Basket of options available with innovative and
						scientific guiding mechanism</p>
				</div>

				<div class="profileCol achieveBlock">
					<h2>Choose</h2>
					<p>Best product that matches to fulfill your dream</p>
				</div>
			</div>

			<div class="pageRowBlock yourSearchText">
				<h2>Simpler than others</h2>
				<p>LoanGully provides a bouquet of the best possible loan
					options from all major banks at one place for your convenience and
					to guide you with a scientific approach to choose the best product
					based on your financial strengths and commitments. As we have come
					up with a fool proof mechanism to ensure you a happy life along
					with an asset, we would request you to please fill candid data to
					ensure we give you the best possible options.</p>
				<br>
				<h2>Why LoanGully</h2>
				<p>As someone who has taken loans for education, start-up and
					buying homes for ourselves, we understand the beauty of dreams and
					the happiness of living it. And we want to share this happiness
					across to all. Hence happened LoanGully - A company that believes
					no dream is either too big or too small and everyone has the right
					to dream; and live it too.</p>
				<br>
				<p>In this process of building LoanGully we sought the guidance
					from several senior bankers and financial advisers on how and what
					is important in banking and specifically loans - a necessary evil
					(or a boon in disguise we do not understand) today. As we discussed
					more; we realized probably there are many others who are not
					looking at loans in the right sense and hence not using them
					effectively. To put it simply, loans are a very healthy system of
					asset acquisition through EMIs that has proficient steps to test
					your eligibility before loaning, makes sure you have enough for
					survival and some more and also provides insurance for the just in
					case scenario.</p>
			</div>

			<div class="pageRowBlock specializationBlock">
				<h2>Our specialized Services</h2>
				<div class="specializationCol homeLoansBlock">
					<h3>Home loans</h3>
					<ol>
						<li>Purchase of Flat/house</li>
						<li>Resale</li>
						<li>Open Plot</li>
						<li>Composite Loan</li>
						<li>Top-up Loan</li>
						<li>Balance Transfer</li>
					</ol>
					<a href="/web/homeLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol personelLoansBlock">
					<h3>Personal Loans</h3>
					<ol>
						<li>House renovation</li>
						<li>Interior decoration</li>
						<li>Medical Expenses</li>
						<li>Foreign tour with family</li>
						<li>Children's education</li>
						<li>Marriage</li>
					</ol>
					<a href="/web/personalLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol mutualFundsBlock">
					<h3>Mortgage Loans</h3>
					<ol>
						<li>Residential</li>
						<li>Commercial</li>
						<li>Starting of New business</li>
						<li>Working capital</li>
						<li>LRD</li>
						<li>Balance Transfer</li>
					</ol>
					<a href="/web/lapLoan/" class="readMoreButton">Explore...</a>
				</div>

				<div class="specializationCol insurance">
					<h3>Calculators</h3>
					<ol>
						<li>EMI Calculator</li>
						<li>Loan Eligibility</li>
						<li>Interest Payable</li>
						<li>Amortization</li>
						<li>Tax Implications</li>
						<li>Many more</li>
					</ol>
					<a href="/web/calc/" class="readMoreButton">Explore...</a>
				</div>
			</div>
		</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		$("#headerProfile").addClass('activeTopNav');
	});
</script>
</body>
</html>