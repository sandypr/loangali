
<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihDealsOffers.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper" id="bannerText">		    
			<div class="bannerText homeLoanVeryBigIcon">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div> 
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon">Home Loan</div>
			<div id='subsublevelnavblock'><%@ include file="subsublevelnavblock.html"%></div>  
		</div>
	</div>
	<div id='dealsContent'> </div>
	
	
	<!-- add footer -->
	<%@ include file="footer.html"%>

	<script>
		$(function(){
			// making decision based on category what to display
			var cat = localStorage.getItem("sourceCategory");
			var div = $("<div id='subsublevelnavblock'></div>");
			if(cat == "2"){
				$("#dealsContent").load("../dealsOffersHL.jsp");
				$("#headerHL").addClass('activeTopNav');
			}else if(cat == "3"){	
				$("#bannerText").html("");
				$("#subLevelNav").html("");
				$("#bannerText").append('<div class="bannerText personelLoansVeryBigIcon"><span>Celebrate being</span> smarter !	</div>');
				$("#subLevelNav").append('<div class="subLevelNavBlock personelLoanIcon">Personal Loan</div>');
				$("#subLevelNav").append(div);
				$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockPL.html");		
				$("#dealsContent").load("../dealsOffersPL.jsp");
				$("#headerPL").addClass('activeTopNav');
			}else if(cat == "4"){
				$("#bannerText").html("");
				$("#subLevelNav").html("");
				$("#bannerText").append('<div class="bannerText personelLoansVeryBigIcon"><span>Give wings </span>to your dreams !</div>');
				$("#subLevelNav").append('<div class="subLevelNavBlock personelLoanIcon"><span>Mortgage Loan</span></div>');
				$("#subLevelNav").append(div);
				$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockLAP.html");
				$("#dealsContent").load("../dealsOffersLAP.jsp");
				$("#headerLAP").addClass('activeTopNav');
			}
		});
	</script>
</body>
</html>