/**
 * The file contains all required function that are used to paint the ui components
 * @author sandy
 */

var unprocessedData = [];
var maxTenure = 0.0;
var preferredProduct;
var preferredProcessedProduct;
var preferredRate;
var isInitial = true;
var currentRateType = 1;
var isEligibilityRequested = false;
var maxLoanEligibility = 0.0;
var tenureIsSlide = false;
var tenurePrior = 0.0;
var emiEligibility = 0.0;
var canSlide = true;

function paintStep2(){
	
	var income = $("#monthlyIncome").val();
	var commitments = $("#commitments").val();
	
	if($('#hasCoApplicant').prop('checked')){
		var cIncome = currencyToNumbers($("#coApplicant_monthlyIncome").val());
		var cCommitments = ($.isNumeric($("#coApplicant_commitments").val()))? currencyToNumbers($("#coApplicant_commitments").val()) : 0;
		
		income = Number(income) + Number(cIncome);
		commitments = Number(commitments) + Number(cCommitments);
	}
	
	$("#prospectDisplayName").html("");
	$("#prospectDisplayName").append("Mr/Ms "+capitaliseFirstLetter($("#name").val())+",");
	$("#prospectDisplayNameStep3").html("");
	$("#prospectDisplayNameStep3").append("Mr/Ms "+capitaliseFirstLetter($("#name").val())+",");
	
	if(!$.isNumeric(commitments))
		commitments = 0.0;

	$.when(calculateEmiEligi($("#categoryType").val(), income, commitments)).done(function(emiEligi){
		emiEligibility = emiEligi;
		if(emiEligibility < 1){
			alert('Sorry ! Your salary and commitments do not suffice for taking a loan right now. Please change the values to get a quote.');
			window.location.href = '/';
		}else{
			
			$.ajax({
				type : "GET",
				url : "/web/metadata/entities/"+$('#categoryType').val(),
				contentType : 'application/json',
				dataType : "text",
				success : function(data) {
					maxTenure = findMaxTenureForCategory(data);
					//alert("maxTenure : "+maxTenure);
					$.when(calcLoanEligi($('#categoryType').val(),income, commitments, maxTenure)).done(function(maxEligibility){
						maxLoanEligibility = (Math.floor(maxEligibility/1000))/100;
						paint(data,emiEligibility,1);
					});
				},
				error : function(xhr) {
					redirect2Error(xhr.status);
				}
			});
		};
	});
}

function calcLoanEligi(cat, income, commitments, maxTenure){
	return $.ajax({
				type : "GET",
				url : "/web/calc/loanEligibilityAmount/"+cat+"/"+income+"/"+commitments+"/"+maxTenure,
				contentType : 'application/json',
				dataType : "text",
				success : function(eligi) {
					//alert(eligi);
					//return round(eligi/100000,2);
				},
				error : function(xhr) {
					redirect2Error(xhr.status);
				}
			});
}

// actual function to paint the ui components
function paint(data,emiEligibility,rateType){

	currentRateType = rateType;
	unprocessedData = data;
	var comparableObjects = [];
	
	var maxTenureByRateWiseProducts = findMaxTenureForCategory(data, rateType);
	//var tenure = maxTenureByRateWiseProducts;	
	var tenure = $("#defaultTenure").val();	
	if($('#tenure').val() > 0)
		tenure = $('#tenure').val();
	
	var reqEligibility =  $('#reqEligibility').val();
	var rows = 1;

	comparableObjects = processCompareData(unprocessedData,tenure,emiEligibility,maxLoanEligibility);
	//console.log(comparableObjects);
	var isWidgetView = $('#widgetsView').val();
	if(isWidgetView === null ||  isWidgetView === undefined || isWidgetView === 'NaN' || isWidgetView !== 'true'){
		// repaint table : start with heading
		$('#result_table').html("");
		$('#result_table').append("<tr width='80%' id='results_header' bgcolor='#CFECFA'> </tr>");
		$('#results_header').append("<th> Bank </th>");
		$('#results_header').append("<th> Eligibility</th>");
		$('#results_header').append("<th> EMI</th>");
		$('#results_header').append("<th> ROI</th>");
		$('#results_header').append("<th> Tenure</th>");
		$('#results_header').append("<th></th>");
	}
	var matchedIndex = 0;
	var emi = 0.0;
	preferredProcessedProduct = null;
	$.each(comparableObjects, function(index, val) {
		// for the same rate type..
		if(val.rateType == rateType){
			
			matchedIndex = matchedIndex+1;
			if(index == 0 || matchedIndex == 1){
				
				var finalData = jQuery.parseJSON(unprocessedData);
				$.each(finalData, function(prodIndex, product) {
					if(product.id == val.id){
						preferredProduct = product;
						preferredProcessedProduct = val;
						$.each(preferredProduct.rates, function(rateIndex,rate){
							console.log(rate.interest_id+"=="+val.interestId);
							if(rate.interest_id == val.interestId){
								preferredRate = rate;
								return false;
							}
						});
						return false;
					}
				});

				if(Number(reqEligibility) <= 0){
				 	
					//reqEligibility = round(val.eligibleAmount,2);
					//reqEligibility = Math.floor(val.eligibleAmount*100)/100;
					reqEligibility = floor(val.eligibleAmount,2);
				 	if(reqEligibility > maxLoanEligibility)
				 		reqEligibility = maxLoanEligibility;
				 	
				 	emi = Math.ceil(val.emi);
				 }else{
				 	emi = calculateEMI(reqEligibility*100000,val.roi,tenure);
				 }
				 
				 $("#topLoanAmount").html("");
				 $("#topTenure").html("");
				 $("#topEmi").html("");
				 $("#topRoi").html("");
				 
				  $("#topLoanAmount").append(reqEligibility+" <span class='blueSmallText'>Lakhs</span>");
				  $("#topTenure").append(val.tenure+" <span class='blueSmallText'>Years</span>");
				  $("#topEmi").append(currencyFormatter(String(Number(emi)))+" <span class='blueSmallText'>Rs</span>");
				  $("#topRoi").append(val.roi+" <span class='blueSmallText'>%</span>");
				  
				  /*if(maxEligibility == 0.0 || tenureIsSlide == true)
				  	maxEligibility = round(Number(val.eligibleAmount),2);*/
				  	
				  $('#gaugeAmt').html("");
				  var a = new JustGage({
				    id: "gaugeAmt",
				    value : reqEligibility,
				    min: 1,
				    //max: round(Number(maxEligibility)+2,0),
				    max: maxLoanEligibility,
				    title: "Loan Amount",
				    label: "Lakhs",
				    gaugeColor: "",
				    levelColors : [  "#F27C07", "#F27C07",  "#F27C07" ],  
			        startAnimationType: ">",
			        refreshAnimationType: "bounce"
				  });
				
				  $('#gaugeTenure').html("");
				  var b = new JustGage({
				    id: "gaugeTenure",
				    value: val.tenure,
				    min: 1,
				    max: maxTenure,
				    title: "Tenure",
				    label: "Years",
				    gaugeColor: "",
				    levelColors : [  "#a9d70b", "#a9d70b",  "#a9d70b" ],  
			        startAnimationType: ">",
			        refreshAnimationType: "bounce"
				  });
				  
				  $('#gaugeEmi').html("");
				  var c = new JustGage({
				    id: "gaugeEmi",
				    value: emi,
				    min: 1,
				    max: Number(emiEligibility),
				    title: "EMI",
				    label: "Rs",
				    gaugeColor: "",
				    levelColors : [  "#F27C07", "#F27C07",  "#F27C07" ],  
			        startAnimationType: ">",
			        refreshAnimationType: "bounce"
				  });
				
				  $('#gaugeRoi').html("");
				  var d = new JustGage({
				    id: "gaugeRoi",
				    value: val.roi,
				    min: 0,
				    max: 30,
				    title: "ROI",
				    label: "%",
				    gaugeColor: "",
				    levelColors : [  "#a9d70b", "#a9d70b",  "#a9d70b" ],  
			        startAnimationType: ">",
			        refreshAnimationType: "bounce"
				  });
				  
				  //return false;						  
			}
			
			if(isWidgetView !== null &&  isWidgetView !== undefined && isWidgetView !== 'NaN' && isWidgetView == 'true')
				return false;
			else{
				
				$('#result_table').append("<tr id='result"+rows+"' ></tr>");
				
				//console.log(val.name);
				//var prodName = (val.name).replace('\\\"', '');
				//console.log(prodName);
				$('#result'+rows).append("<td align='center'><div class='col-md-offset-1 col-md-10'><img src='data:image/png;base64,"+val.logo+"' width='120' height='50' alt=''/>"+"</div></td>");
				$('#result'+rows).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+reqEligibility+"  <span class='blueSmallText'>Lakhs</span></div></td>");
				$('#result'+rows).append("<td align='right'><div class='col-md-offset-1 col-md-10'><span class='blueSmallText'>Rs. </span>"+currencyFormatter(String(Number(emi)))+"  </div></td>");								
				$('#result'+rows).append("<td align='right'><div class='col-md-offset-1 col-md-10'>"+val.roi+"  <span class='blueSmallText'>%</span></div></td>");
				$('#result'+rows).append("<td align='center'><div class='col-md-offset-1 col-md-10'>"+val.tenure+"  <span class='blueSmallText'>yrs</span></div></td>");
				$('#result'+rows).append("<td align='left'> <button class='applyLoanGreenBtn' onClick=" + "saveLead('"+ val.bank + "','"+ val.bank +"'," +  val.eligibleAmount + ","+  val.tenure + ",'" +val.id +"',"+val.emi+","+val.roi+")" + ">Apply Loan</button></div></td>");

				rows = rows + 1;
			}
			
		}
	}); 
	
	// if preferredRate is available
	if(!$.isEmptyObject(preferredRate)){
		var cat = $("#categoryType").val();
		// for ui widgets
		for (var row = 1; row < 4; row++ ) {
			for (var col = 1; col < 6; col++ ) {
				var yrs = col * 5;
				if(cat == '3')
					yrs = col * 1;
				else if(cat =='4')
					yrs = col * 3;
				
				$('#col'+col+row).html("");
				if(row == 1){
					//console.log(preferredRate.emisPerLakh[yrs-1]);
					$('#col'+col+row).append(preferredRate.emisPerLakh[yrs-1].tenure+" <span class='blueSmallText'>Yrs</span>");
				}
				
				if(row == 2){
					//$('#col'+col+row).append(round(emiEligibility/preferredRate.emisPerLakh[yrs-1].emiPerLakh, 2)+" <span class='blueSmallText'>Lakhs</span>");
					$('#col'+col+row).append(floor(emiEligibility/preferredRate.emisPerLakh[yrs-1].emiPerLakh, 2)+" <span class='blueSmallText'>Lakhs</span>");
				}
				
				if(row == 3){
					//$('#col'+col+row).append(round((emiEligibility/preferredRate.emisPerLakh[yrs-1].emiPerLakh) * (preferredRate.emisPerLakh[yrs-1].emiPerLakh),0)+" <span class='blueSmallText'>Rs</span>");
					$('#col'+col+row).append(currencyFormatter(String(ceil((emiEligibility/preferredRate.emisPerLakh[yrs-1].emiPerLakh) * (preferredRate.emisPerLakh[yrs-1].emiPerLakh),0)))+" <span class='blueSmallText'>Rs</span>");
				}
			}
		}
	}
	paintSliders();
	//console.log(comparableObjects);
	comparableObjects = [];
}

var tenureTooltip = $('<div id="tenureTooltip" class="tooltip"/>').css({
    position: 'absolute',
    top: 25
}).hide();
var tooltip = $('<div id="tooltip" class="tooltip"/>').css({
    position: 'absolute',
    top: -33
}).hide();


function paintSliders(){
	//console.log(preferredProcessedProduct);
	//alert(isInitial+"-"+tenureIsSlide);
	if(isInitial || (!isInitial && tenureIsSlide)){
		if(!$.isEmptyObject(preferredProcessedProduct)) {
		
			// eligibility slider
			var reqEligibility = Number(preferredProcessedProduct.eligibleAmount);
			if(Number($("#reqEligibility").val()) > 0){
				reqEligibility = $("#reqEligibility").val();																				
			}
			
			$("#eligibilitySlider").html("");
			$("#eligibilityAmountForSlider").html("");
			$("#eligibilityAmountForSlider").append(+maxLoanEligibility+" <span class='blueSmallText'>Lakh</span>");
			//console.log(maxLoanEligibility);
			
			// populating tooltip
			tooltip.text(round(reqEligibility,2)+" Lakh");
			
			// painting the eligibility slider
		    $("#eligibilitySlider").slider({ 
		      range: "max",
		      value: round(reqEligibility,2),
		      min: round((Number(preferredProcessedProduct.emi)*4)/100000, 0),
		      //max: maxEligibility,
		      max: maxLoanEligibility,
		      step: 0.01,
		      slide: function(event, ui) {
		    	  if(isSlideAllowed(0,ui.value)){
		    		isInitial = false;
			      	isEligibilityRequested = true;		
			      	tenureIsSlide = false;		
			        $("#reqEligibility").val(ui.value);
			        tooltip.text(ui.value+" Lakh");
			        canSlide = true;
			        paint(unprocessedData,emiEligibility,currentRateType);
		    	  }else{
		    		  canSlide = false;
		    		  showPopUp("Error Message","Sorry ! That move would increase your EMI beyond EMI Eligibility (or) would increase your Loan amount beyond your Max Loan Eligibilty.");
		    		  return false;
		    	  }
		      }
		    }).find(".ui-slider-handle").append(tooltip).hover(function() {
				    tooltip.show();
				}, function() {
				    tooltip.hide();
				});
		   
		   // populating tooltip
		    tenureTooltip.text(Number(preferredProcessedProduct.tenure)+" Yrs");
		    //console.log(Number(preferredProcessedProduct.tenure));
		   // tenure slider
		   	$("#tenureSlider").html("");
		   	$("#tenureForSlider").html("");
		   	$("#tenureForSlider").append(maxTenure+" <span class='blueSmallText'>Yrs</span>");
		   	$("#tenureSlider").slider({
		      range: "max",
		      value: Number(preferredProcessedProduct.tenure),
		      min: 1,
		      max: maxTenure,
		      step: 1,
		      slide: function(event, ui) {
		    	  if(isSlideAllowed(ui.value,0)){
		    		isInitial = false;
			      	tenureIsSlide = true;
			      	tenureTooltip.text(ui.value+" Yrs");
			        $("#tenure").val(ui.value);
			        canSlide = true;
			        paint(unprocessedData,emiEligibility,currentRateType);
		    	  }else{
		    		  canSlide = false;
		    		  showPopUp("Error Message","Sorry ! Sorry ! That move would increase your EMI beyond EMI Eligibility (or) would increase your Loan amount beyond your Max Loan Eligibilty.");
		    		  return false;
		    	  }
		      }
		    }).find(".ui-slider-handle").append(tenureTooltip).hover(function() {
				    tenureTooltip.show();
				}, function() {
				    tenureTooltip.hide();
				});;
		}
	}
}

function findMaxTenureForCategory(data, requestedRateType){
	
	var finalData = jQuery.parseJSON(data);
	var objectsByRateType = [];
	$.each(finalData, function(index, data){
		if(requestedRateType === undefined)
			objectsByRateType.push(data);
		else if(data.rateType == requestedRateType)
				objectsByRateType.push(data);
	});
	
	objectsByRateType.sort(sortByMaxTenure);
	return objectsByRateType[0].maxTenure;
}

function sortByMaxTenure(a,b){
	return  ((a.maxTenure > b.maxTenure) ? -1 : ((a.maxTenure < b.maxTenure) ? 1 : 0));
}

function processCompareData(data, tenure,emiEligibility){
	processCompareData(data, tenure,emiEligibility, 0.0);
}

function processCompareData(data, tenure,emiEligibility, eligibleAmount){
	var comparableObjects = [];
	// to use if the comparableObjects is empty
	var comparableObjectsWorstCase = [];
	var finalData = jQuery.parseJSON(data);
	$.each(finalData, function(prodIndex, product) {
		// look for products that are given for a specific tenure. say 0-20years
		var customersAge = $("#age").val();
		if(customersAge === undefined || customersAge <= 0)
			customersAge = 0;
		
		// TODO open this below line when we ahve age group configured for products in db.
		//if(product.minTenure <= tenure && product.maxTenure >= tenure && product.minAge <= customersAge  && product.maxAge >= customersAge){
		if(product.minTenure <= tenure && product.maxTenure >= tenure){
    			$.each(product.rates, function(rateIndex,rate){
    				$.each(rate.emisPerLakh, function(emiIndex,emiPerLakh){
	    				if(emiPerLakh.tenure == tenure){
	    					// for scenarios where eligibility is specified
	    					//var eligibleLoanAmount = round(emiEligibility/emiPerLakh.emiPerLakh, 4);
	    					var eligibleLoanAmount = round(emiEligibility/emiPerLakh.emiPerLakh, 4);
	    					if(Number(eligibleAmount) > 0){
	    						eligibleLoanAmount = eligibleAmount;
	    					}
	    					eligibleLoanAmount = Number(eligibleLoanAmount * 100000);
	    					
	    					if(Number(rate.rangeLower) <= eligibleLoanAmount && Number(rate.rangeUpper) >= eligibleLoanAmount){
	    						var element=new Object();

		    					element.logo = product.logo;
		    					element.bank = product.entityName;
		    					element.id = product.id;
		    					element.name = product.name;
		    					//element.eligibleAmount = round(emiEligibility/emiPerLakh.emiPerLakh, 4);
		    					element.eligibleAmount = Math.floor((emiEligibility/emiPerLakh.emiPerLakh)*10000)/10000;
		    					//element.emi = round(element.eligibleAmount * emiPerLakh.emiPerLakh,2);
		    					element.emi = Math.ceil(element.eligibleAmount * emiPerLakh.emiPerLakh);
		    					element.roi = rate.roi;
		    					element.interestId = rate.interest_id;
		    					element.tenure = emiPerLakh.tenure;
		    					element.rateType = product.rateType;
		    					comparableObjects.push(element);
	    					};
	    					
	    					//comparableObjectsWorstCase.push(element);
						};			
    				});
    			});
			};
	});
	
	// sort by rate
	//comparableObjects.sort(sortByRate);
	//console.log(comparableObjects.length);
	var finalComparableObjects = (comparableObjects.length > 0)? comparableObjects : comparableObjectsWorstCase;
	// sort by tenure
	finalComparableObjects.sort(sortByTenure);
	//console.log(finalComparableObjects);
	return finalComparableObjects;
}

/*//convert user to lead with selected product
function saveLead(name, bank, amt, tenure, product, emi, roi) {

	 fillProductDetailsOnStep3(bank, amt, tenure, product, emi, roi);
  	 // activated step 2
  	 $("#step3Tab").removeClass("disabled");
 	 $('ul.stepsTabsList li a[href="#step3"]').trigger('click');
 	 $("#step1Tab").addClass("disabled");
 	 $("#step2Tab").addClass("disabled");
 	 
	 var prospectId = $("#prospectId").val();
	 var categoryName = $("#categoryName").val();
	 var data = {"productName": name, "categoryName": categoryName, "amount": amt, "tenure": tenure, "productId": product, "prospectId" : prospectId, "entityName" : bank, "emi" : emi, "roi" : roi};
	 $.ajax({
	        type: "POST",
	        url: "/web/lead/addLead",
	        data: JSON.stringify(data),      // NOTE CHANGE HERE
	        contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function(msg) { 
	        	//nothing to do
	        	//console.log('success');
	        },
	        error: function(msg) { // nothing to do
	        	//console.log('failure');
	        }
	    });
 }*/

function isSlideAllowed(slidTenure,slidEligi){
	 if(slidEligi == 0) slidEligi = $("#reqEligibility").val();
	 if(slidTenure == 0) slidTenure = ($("#tenure").val() <= 0) ? $("#defaultTenure").val() : $("#tenure").val();
	 var newEmi = round(calculateEMI(slidEligi*100000,preferredRate.roi,slidTenure),0);
	 var newEligi = calculatePrincipal((newEmi > 0)?newEmi:emiEligibility,preferredRate.emisPerLakh[slidTenure-1].emiPerLakh);
	 console.log(newEmi+"--"+emiEligibility+"---"+newEligi+"----"+maxLoanEligibility);
	 return ((Number(newEmi) <= Number(emiEligibility)) && Number(newEligi) <= Number(maxLoanEligibility) )? true:false;
}

function calculatePrincipal(emi, emiPerLakh){
	
	var principle = emi/emiPerLakh;
	return principle;
}
