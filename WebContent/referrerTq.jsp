<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Loan Galli - Simpler than others !</title>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/lgutil.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/raphael.2.1.0.min.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/justgage.1.0.1.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/owl.carousel.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/ihApplyHomeLoans.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery-ui-calendar.css">
</head>
<body>

	<!-- add header -->
<body>
	<%@ include file="header.html"%>

	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon">
				<span>L.O.A.N - </span> Lets Own A Nest
			</div>
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper">
			<div class="subLevelNavBlock homeLoansIcon">Home Loans</div>
			<div id='subsublevelnavblock'><%@ include
					file="subsublevelnavblock.html"%></div>
		</div>
	</div>
	<div class="stepThreeThanksBlock">
		<h3>Thanks for writing to us! We value your feedback and
			suggestions/questions would be responded to at the earliest. !</h3>
		<span>for further assistance please give us a missed call</span>
		<h3>+91-98549627757</h3>
	</div>
</body>
</html>