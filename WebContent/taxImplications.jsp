
<!-- add header -->
<%@ include file="header.html"%>
<script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ihTaxImplication.css">
	<div class="innerPageBannerBlock">
		<div class="wrapper">
			<div class="bannerText homeLoanVeryBigIcon" id="bannerText">
				<span>L.O.A.N - </span> L<span>ets</span> O<span>wn </span>A N<span>est</span>
			</div>
		</div>
	</div>

	<div class="innerPageSubNav">
		<div class="wrapper" id="subLevelNav">
			<div class="subLevelNavBlock homeLoansIcon" id='subnavblock_heading'>Home Loan</div>
			<div id='subsublevelnavblock'><%@ include
					file="subsublevelnavblock.html"%></div>
		</div>
	</div>
	
<div id='taxContent'> 
	<div class="taxImplicationPage">
		<div class="wrapper">
			<h1 class="pageHeading">
				Tax Implications<br> <span>All about loans and tax savings for Home Loans.</span>
			</h1>

			<div class="taxImplicationBlock">
				<h2>Tax implications on first Education Loan</h2>
				<p>In case you are opting to stay in the first house purchased
					on loan, the tax exemption is equal to the total interest payable
					or Rs. 2,00,000 whichever is lesser. In case of principal the tax
					exemption amount is equal to the principal payable or Rs. 1,00,000
					whichever is lower.</p>
				<h2>Tax implications on consecutive Education Loans</h2>
				<p>In this case you can chose one house as occupied by you; and
					in the case of the second or third homes you can get a tax
					exemption as follows <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>'Tax exemption = Annual rent on the house - 30%
					of Annual rent on the house (For depreciation) - Interest Payable
					on the loan'</i>. <br>The interesting fact here is that you don't have a
					upper limit on the amount. However principal repaid on N number of
					houses is limited by Rs. 1,50,000</p>

				<h2>Tax implications on the principle portion:</h2>
				<p>The tax exemption on principle portion repaid is restricted
					to Rs. 2,00,000 independent on the number of education loans taken by
					a person.</p>

				<h2>Tax implications on the interest portion</h2>
				<p>In case of interest portion; the tax exemption is limited to
					the lower values of Rs. 2,00,000 or the total interest payable in
					an year (whichever is lower) if its the first education loan.
					However, in case of more than one education loans; <i>'tax exemption =
					70% of annual rent - Interest payable on the loan'</i> with no cap.</p>
				<p>One important thing to note here is that, notional rent from
					the second house should be added to income and due tax has to be
					paid on it.</p>

				<h2>Disclaimer</h2>
				<p>All information shared here is purely for information purpose
					only. LoanGully and and is not a substitute for advice from a tax
					consultant. We strongly advice that you do a self analysis of your
					financial sources and commitments; and ensure you abide by the
					rules of the Indian Government for taxation of income which can be
					checked here (Link to Tax rules). Users are solely held responsible
					for any decision made by them using the information on this site
					and LoanGully shares no liability towards the same.</p>
			</div>

			<div class="taxImplicationBlock" style="text-align: right;">
				<a href="/web/calc/" class="calculateTaxLiability">Know
					your tax liability</a> <a href="/web/homeLoan/"
					class="applyLoanBtn">Apply Loan</a>
			</div>
		</div>
	</div>
	</div>

	<!-- add footer -->
	<%@ include file="footer.html"%>
	<script>
	$(function(){
		// making decision based on category what to display
		var cat = localStorage.getItem("sourceCategory");
		var div = $("<div id='subsublevelnavblock'></div>");
		if(cat == "5"){
			$("#bannerText").html("");
			$("#bannerText").append('<span>Education is</span> LIFE itself !');
			$("#bannerText").removeClass("homeLoanVeryBigIcon");
			$("#bannerText").addClass("eduLoansVeryBigIcon");
			
			
			$("#subnavblock_heading").html("");
			$("#subnavblock_heading").append("Education Loan");
			$("#subnavblock_heading").removeClass("homeLoansIcon");
			$("#subnavblock_heading").addClass("eduLoanIcon");
			
			$("#subsublevelnavblock").load("${pageContext.request.contextPath}/subsublevelnavblockEdu.html");
			$("#taxContent").load("../taxImplicationsEdu.jsp");
			$("#headerEL").addClass('activeTopNav');
		}else if(cat == "2"){
			$("#headerHL").addClass('activeTopNav');
		}
	});
</script>
</body>
</html>