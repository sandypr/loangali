package com.thincovate.ih.common;

import java.util.Random;

import com.thincovate.ih.models.LeadReq;
import com.thincovate.ih.workbench.prospects.model.CoApplicant;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

public class TestUtil {
	 
    public static ProspectLoans createProspect(int category) {
    	ProspectLoans dto = new ProspectLoans();
    	Random rn = new Random();
        
    	dto.setName("test"+rn.nextInt());
    	dto.setCategoryType(category);
    	dto.setHasCoApplicant("off");
    	dto.setCommitments(6000);
        dto.setEligibleSum(49000.0);
        dto.setEmailAddress("test"+rn.nextInt()+"@thincovate.com");
        dto.setMobile("900000000");
        dto.setWorkLocation("hyd");
        dto.setEmploymentType("salaried");
        dto.setMonthlyIncome(100000);
        dto.setAge(32);
 
        return dto;
    }
    
    public static ProspectLoans createProspectWithMssngFlds() {
    	ProspectLoans dto = new ProspectLoans();
    	Random rn = new Random();
        
    	
    	dto.setCategoryType(2);
    	dto.setHasCoApplicant("off");
    
        dto.setEmailAddress("test"+rn.nextInt()+"@thincovate.com");
        dto.setMobile("900000000");
        dto.setWorkLocation("hyd");
        dto.setEmploymentType("salaried");
        dto.setMonthlyIncome(100000);
        dto.setAge(32);
 
        return dto;
    }
    
    public static ProspectLoans createProspectWithCoApplicant(){
    	ProspectLoans dto = new ProspectLoans();
        
    	dto.setName("testCoApplicant");
    	dto.setCategoryType(2);
    	dto.setHasCoApplicant("on");
    	dto.setCommitments(6000);
        dto.setEligibleSum(49000.0);
        dto.setEmailAddress("test@thincovate.com");
        dto.setMobile("900000000");
        dto.setWorkLocation("hyd");
        dto.setEmploymentType("salaried");
        dto.setMonthlyIncome(100000);
        dto.setAge(32);
        
        CoApplicant coApplicant = new CoApplicant();
        
        coApplicant.setCommitments(10000);
        coApplicant.setCompany("test");
        coApplicant.setAge(32);
        coApplicant.setEmail("test2@thincovate.com");
        coApplicant.setMobile("9000000021");
        dto.setCoApplicant(coApplicant);
 
        return dto;
    }
    
    public static LeadReq createLead(String prospectId){
    	
    	LeadReq lead = new LeadReq();
    	lead.setAmount(490000.0);
    	lead.setProductId("CITI_H_FL_2_06");
    	lead.setProspectId(Integer.parseInt(prospectId));
    	lead.setTenure(10);
    	
    	return lead;
    	
    
    }
 
    public static String createStringWithLength(int length) {
        StringBuilder builder = new StringBuilder();
 
        for (int index = 0; index < length; index++) {
            builder.append("a");
        }
 
        return builder.toString();
    }
    
   
}