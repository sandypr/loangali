package com.thincovate.ih.workbench.prospects.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.thincovate.ih.common.IntegrationTestUtil;
import com.thincovate.ih.common.TestUtil;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath:dispatcher-servlet.xml", "classpath:ih-datasource.xml" })
public class ProspectsControllerTest {

	@Autowired
	WebApplicationContext wac;
	@Autowired
	MockHttpSession session;
	@Autowired
	MockHttpServletRequest request;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testAddProspectMissingFlds() throws Exception {
		this.mockMvc.perform(
				post("/addProspect").param("name", "mvcemail@test.com").param("emailAddress", "mvcemail@test.com")
						.param("mobile", "1000000000").param("workLocation", "hyd").param("employmentType", "salaried")
						.param("company", "test").param("monthlyIncome", "100000").param("commitments", "6000")
						.param("categoryType", "2").param("purpose", "buy")).andExpect(
				status().isUnsupportedMediaType());
	}

	@Test
	public void testAddProspect() throws Exception {

		ProspectLoans prospect = TestUtil.createProspect(2);

		MvcResult result = mockMvc.perform(
				post("/addProspect").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8).content(
						IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();

		String content = result.getResponse().getContentAsString();
		assertTrue(Integer.parseInt(content) > 0);
		
		//cleanup
		deleteProspect(Integer.parseInt(content));
	}
	
	@Test
	public void testAddProspectPersonalLoan() throws Exception {

		ProspectLoans prospect = TestUtil.createProspect(3);

		MvcResult result = mockMvc.perform(
				post("/addProspect").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8).content(
						IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();

		String content = result.getResponse().getContentAsString();
		assertTrue(Integer.parseInt(content) > 0);
		
		//cleanup
		deleteProspect(Integer.parseInt(content));
	}
	
	@Test
	public void testAddProspectLAP() throws Exception {

		ProspectLoans prospect = TestUtil.createProspect(4);

		MvcResult result = mockMvc.perform(
				post("/addProspect").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8).content(
						IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();

		String content = result.getResponse().getContentAsString();
		assertTrue(Integer.parseInt(content) > 0);
		
		//cleanup
		deleteProspect(Integer.parseInt(content));
	}

	@Test
	public void testAddProspectAndCoApplicant() throws Exception {
		ProspectLoans prospect = TestUtil.createProspectWithCoApplicant();
		MvcResult result = mockMvc.perform(
				post("/addProspect").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8).content(
						IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();

		String content = result.getResponse().getContentAsString();
		assertTrue(Integer.parseInt(content) > 0);
		
		//cleanup
		deleteProspect(Integer.parseInt(content));
	}
	
	public void deleteProspect(int id) throws Exception{
		this.mockMvc.perform(get("/deleteProspect/"+id))
        .andExpect(status().isOk());
	}
}
