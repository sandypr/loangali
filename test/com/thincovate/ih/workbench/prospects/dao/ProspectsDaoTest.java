package com.thincovate.ih.workbench.prospects.dao;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.thincovate.ih.common.TestUtil;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:dispatcher-servlet.xml", "classpath:ih-datasource.xml" })
public class ProspectsDaoTest {
	
	static Logger log = Logger.getLogger(ProspectsDao.class);
	
	@Autowired
	ProspectsDao prospectsdDao;
	
	@Test
    public void testAddProspectLoan() {
		
		ProspectLoans prospectLoans = TestUtil.createProspect(2);
		long id = prospectsdDao.addProspectLoan(prospectLoans).longValue();
		
		assertNotNull(id);
		assertTrue(id > 0);
		
		//cleanup
		deleteProspect(id);
	}
	
	@Test
    public void testAddProspectLoanThrowError() {
		
		ProspectLoans prospectLoans = TestUtil.createProspectWithMssngFlds();
		try{
			prospectsdDao.addProspectLoan(prospectLoans);
		}catch(DataIntegrityViolationException ex){
			assertNotNull(ex.getMessage());
		}
	}
	
	@Test
    public void testFindProspectByid() {
		
		ProspectLoans prospectLoans = TestUtil.createProspect(2);
		long id = prospectsdDao.addProspectLoan(prospectLoans).longValue();
		
		ProspectLoans prospect = prospectsdDao.findProspectById((int)id);
		
		assertNotNull(prospect);
		assertTrue(prospect.getProspectId() == id);
		
		//cleanup
		deleteProspect(id);
		
	}
	
	@Test
    public void testFindProspectByidNonExistant() {
		
		ProspectLoans prospectLoans = TestUtil.createProspect(2);
		long id = prospectsdDao.addProspectLoan(prospectLoans).longValue();
		
		ProspectLoans prospect = prospectsdDao.findProspectById((int)id);
		
		assertNotNull(prospect);
		assertTrue(prospect.getProspectId() == id);
		
		//cleanup
		deleteProspect(id);
		
		try{
			prospectsdDao.findProspectById((int)id);
		}catch(EmptyResultDataAccessException ex){
			log.error(ex.getMessage());
			assertNotNull(ex.getMessage());
		}
	}
	
	@Test
    public void testAddProspectWithCOApplicant() {
		ProspectLoans prospectLoans = TestUtil.createProspectWithCoApplicant();
		long id = prospectsdDao.addProspectLoan(prospectLoans).longValue();
		
		assertNotNull(id);
		assertTrue(id > 0);
		
		//cleanup
		deleteProspect(id);
	}
	
	public void deleteProspect(long id){
		//cleanup
		prospectsdDao.deleteProspect((int)id);
	}

}
