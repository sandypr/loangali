package com.thincovate.ih.workbench.leads.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.thincovate.ih.common.IntegrationTestUtil;
import com.thincovate.ih.common.TestUtil;
import com.thincovate.ih.models.LeadReq;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath:dispatcher-servlet.xml", "classpath:ih-datasource.xml" })
public class LeadsControllerTest {
	
	
	@Autowired WebApplicationContext wac; 
    @Autowired MockHttpSession session;
    @Autowired MockHttpServletRequest request;

    private MockMvc mockMvc;
    
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test
    public void testAddLead() throws Exception {
    	
    	ProspectLoans prospect = TestUtil.createProspect(2);
    	
    	MvcResult result = mockMvc.perform(post("/addProspect")
    	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
    	         .content(IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();
    	String prospectId = result.getResponse().getContentAsString();
    	
    	LeadReq lead = TestUtil.createLead(prospectId);
     	
    	mockMvc.perform(post("/lead/addLead")
   	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
   	         .content(IntegrationTestUtil.convertObjectToJsonBytes(lead)))
   	         .andExpect(status().isOk());
          	
     	//cleanup
     	deleteLeadNProspect(Integer.parseInt(prospectId));
    }
    
    @Test
    public void testAddPersonalLoansLead() throws Exception {
    	
    	ProspectLoans prospect = TestUtil.createProspect(3);
    	
    	MvcResult result = mockMvc.perform(post("/addProspect")
    	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
    	         .content(IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();
    	String prospectId = result.getResponse().getContentAsString();
    	
    	LeadReq lead = TestUtil.createLead(prospectId);
     	
    	mockMvc.perform(post("/lead/addLead")
   	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
   	         .content(IntegrationTestUtil.convertObjectToJsonBytes(lead)))
   	         .andExpect(status().isOk());
          	
     	//cleanup
     	deleteLeadNProspect(Integer.parseInt(prospectId));
    }
    
    @Test
    public void testAddLAPLead() throws Exception {
    	
    	ProspectLoans prospect = TestUtil.createProspect(4);
    	
    	MvcResult result = mockMvc.perform(post("/addProspect")
    	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
    	         .content(IntegrationTestUtil.convertObjectToJsonBytes(prospect))).andReturn();
    	String prospectId = result.getResponse().getContentAsString();
    	
    	LeadReq lead = TestUtil.createLead(prospectId);
     	
    	mockMvc.perform(post("/lead/addLead")
   	         .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
   	         .content(IntegrationTestUtil.convertObjectToJsonBytes(lead)))
   	         .andExpect(status().isOk());
          	
     	//cleanup
     	deleteLeadNProspect(Integer.parseInt(prospectId));
    }
    
    public void deleteLeadNProspect(int prospectId) throws Exception{
		
    	this.mockMvc.perform(get("/lead/deleteLead/"+prospectId))
        .andExpect(status().isOk());
    	
    	this.mockMvc.perform(get("/deleteProspect/"+prospectId))
        .andExpect(status().isOk());
    	
    	
	}

}
