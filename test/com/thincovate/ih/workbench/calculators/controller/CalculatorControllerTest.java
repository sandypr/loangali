package com.thincovate.ih.workbench.calculators.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath:dispatcher-servlet.xml", "classpath:ih-datasource.xml" })
public class CalculatorControllerTest {
	
	@Autowired WebApplicationContext wac; 
    @Autowired MockHttpSession session;
    @Autowired MockHttpServletRequest request;

    private MockMvc mockMvc;
    
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
        
    @Test
    public void testCalcEmiEligibility() throws Exception {
        this.mockMvc.perform(get("/calc/emiEligibility/2/100000/6000"))
                .andExpect(status().isOk())
                .andExpect(content().string("49000.0"));
    }
    
       
    @Test
    public void testCalcEmiEligibilityPersonalLoan() throws Exception {
        this.mockMvc.perform(get("/calc/emiEligibility/3/100000/6000"))
                .andExpect(status().isOk())
                .andExpect(content().string("49000.0"));
    }
    
    @Test
    public void testCalcLoanEligibilityAmountPersonalLoan() throws Exception {
        this.mockMvc.perform(get("/calc/loanEligibilityAmount/3/100000/6000/10"))
                .andExpect(status().isOk())
                .andExpect(content().string("3008546.5"));
    }
    
    @Test
    public void testCalcEmiEligibilityLAP() throws Exception {
        this.mockMvc.perform(get("/calc/emiEligibility/4/100000/6000"))
                .andExpect(status().isOk())
                .andExpect(content().string("49000.0"));
    }
    
    @Test
    public void testCalcLoanEligibilityAmountLAP() throws Exception {
        this.mockMvc.perform(get("/calc/loanEligibilityAmount/4/100000/6000/10"))
                .andExpect(status().isOk())
                .andExpect(content().string("3008546.5"));
    }

}
