@echo off

rem ***** Builds the InvestHunt project *****

@setlocal

set BUILD_ROOT=%cd%/..
rem  Verify that all required variables are set
echo %APPSERVER%
if "%APPSERVER%" == "" set APPSERVER=tomcat
if "%APPSERVER%" == "tomcat" goto appserver_done
goto invalid_appserver

:appserver_done

if "%DATABASE%" == "" set DATABASE=mysql
if "%DATABASE%" == "mysql" goto database_done
goto invalid_database

:database_done

if "%JAVA_HOME%"=="" goto usage
if "%BUILD_ROOT%"=="" goto usage
rem if "%BUILD_PROPS%"=="" goto usage

if "%BUILD_VERSION%"=="" set BUILD_VERSION=Dev_Build
if "%APPSERVER%" == "tomcat" set TOMCAT_HOME=%TOMCAT_HOME%

REM Set path to include ant & java
SET PATH=%ANT_HOME%\bin;%PATH%
SET PATH=%JAVA_HOME%\bin;%PATH%

set ANT_OPTS=-Xmx1024M -Dcom.sun.xml.namespace.QName.useCompatibleSerialVersionUID=1.0

if "%APPSERVER%" == "tomcat" goto was_ant
set ANT_OPTS=%ANT_OPTS% -Djava.endorsed.dirs=%TOMCAT_HOME%\endorsed
goto :ANT

:was_ant
set ANT_OPTS=%ANT_OPTS% -Djava.endorsed.dirs=%WAS_HOME%\endorsed_apis
goto :ANT

:ANT
set CLASSPATH=%CLASSPATH%
echo %ANT_HOME%\bin\ant -emacs %ANT_VARS% -buildfile %BUILD_ROOT%\build\build.xml %*
call %ANT_HOME%\bin\ant -emacs %ANT_VARS% -buildfile %BUILD_ROOT%\build\build.xml %*
goto finish


:usage
echo.
echo One or more required environment variables not set:
echo.
if "%APPSERVER%" == "" set APPSERVER
if "%DATABASE%" == "" set DATABASE
if "%JAVA_HOME%"=="" set JAVA_HOME
if "%BUILD_ROOT%"=="" set BUILD_ROOT
rem if "%BUILD_PROPS%"=="" set BUILD_PROPS
goto finish

:invalid_appserver
echo.
echo Invalid APPSERVER environment variable: %APPSERVER%
echo.
echo APPSERVER must be set to "tomcat"
goto finish

:invalid_database
echo.
echo Invalid DATABASE environment variable: %DATABASE%
echo.
echo DATABASE must be set to "mysql"
goto finish

:finish
@endlocal
