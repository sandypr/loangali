/**
 * 
 */
package com.thincovate.ih.workbench.admin.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thincovate.ih.models.Category;
import com.thincovate.ih.models.Entity;
import com.thincovate.ih.models.EntityType;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.User;
import com.thincovate.ih.models.UserType;
import com.thincovate.ih.workbench.admin.dao.AdminDao;
import com.thincovate.ih.workbench.admin.services.AdminServiceI;
import com.thincovate.ih.models.Rates;

/**
 * @author Sandy
 *
 */
@Service
public class AdminServiceImpl implements AdminServiceI {

	@Autowired
	private AdminDao adminDao;
	static Logger log = Logger.getLogger(AdminServiceImpl.class);

	@Override
	public boolean updateCategory(Category category) {
		boolean status = false;

		status = adminDao.updateCategory(category);

		return status;
	}

	@Override
	public boolean updateEntityTypes(EntityType entityType) {
		boolean status = false;

		status = adminDao.updateEntityType(entityType);

		return status;
	}

	@Override
	public boolean updateEntities(Entity entity) {
		boolean status = false;

		status = adminDao.updateEntity(entity);

		return status;
	}

	@Override
	public boolean updateUser(User user) {
		boolean status = false;

		status = adminDao.updateUser(user);

		return status;
	}

	@Override
	public boolean updateUserTypes(UserType userType) {
		boolean status = false;

		status = adminDao.updateUserType(userType);

		return status;
	}

	@Override
	public boolean updateRateTypes() {
		return false;
	}

	@Override
	public boolean updateProduct(Product product) {
		boolean status = false;

		status = adminDao.updateProduct(product);

		return status;
	}

	@Override
	public boolean updateProductInterestRates(Rates rates) {
		boolean status = false;

		status = adminDao.updateProductInterestRates(rates);

		return status;
	}

	@Override
	public boolean updateROI() {
		// TODO Auto-generated method stub
		return false;
	}

}
