/**
 * 
 */
package com.thincovate.ih.workbench.admin.services;

import com.thincovate.ih.models.Category;
import com.thincovate.ih.models.Entity;
import com.thincovate.ih.models.EntityType;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;
import com.thincovate.ih.models.User;
import com.thincovate.ih.models.UserType;

/**
 * @author Sandy
 *
 */
public interface AdminServiceI {

	boolean updateCategory(Category category);
	boolean updateEntityTypes(EntityType entityType);
	boolean updateEntities(Entity entity);
	boolean updateProduct(Product product);
	boolean updateProductInterestRates(Rates rates);
	boolean updateROI();
	boolean updateUser(User user);
	boolean updateUserTypes(UserType userType);
	boolean updateRateTypes();
}
