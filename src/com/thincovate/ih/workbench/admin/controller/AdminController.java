/**
 * 
 */
package com.thincovate.ih.workbench.admin.controller;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.thincovate.ih.models.Category;
import com.thincovate.ih.models.Entity;
import com.thincovate.ih.models.EntityType;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;
import com.thincovate.ih.models.User;
import com.thincovate.ih.models.UserType;
import com.thincovate.ih.workbench.admin.services.AdminServiceI;

/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminServiceI adminService;
	static Logger log = Logger.getLogger(AdminController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/category", consumes = "application/json")
	public boolean updateCategory(@RequestBody Category category, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating category - starts");

		status = adminService.updateCategory(category);

		log.info("Creating/Updating category - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/entityType", consumes = "application/json")
	public boolean updateEntityType(@RequestBody EntityType entityType, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating entityType - starts");

		status = adminService.updateEntityTypes(entityType);

		log.info("Creating/Updating entityType - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/entity", consumes = "application/json")
	public boolean updateEntity(@RequestBody Entity entity, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating entity - starts");

		status = adminService.updateEntities(entity);

		log.info("Creating/Updating entity - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/userType", consumes = "application/json")
	public boolean updateUserType(@RequestBody UserType userType, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating userType - starts");

		status = adminService.updateUserTypes(userType);

		log.info("Creating/Updating userType - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/user", consumes = "application/json")
	public boolean updateUser(@RequestBody User user, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating user - starts");

		status = adminService.updateUser(user);

		log.info("Creating/Updating user - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/product", consumes = "application/json")
	public boolean updateProduct(@RequestBody Product product, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating product - starts");

		status = adminService.updateProduct(product);

		log.info("Creating/Updating product - ends");
		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/interests", consumes = "application/json")
	public boolean updateProductInterestRates(@RequestBody Rates rates, HttpServletRequest req) throws ParseException {

		boolean status = false;
		log.info("Creating/Updating user - starts");

		status = adminService.updateProductInterestRates(rates);

		log.info("Creating/Updating user - ends");
		return status;
	}
}
