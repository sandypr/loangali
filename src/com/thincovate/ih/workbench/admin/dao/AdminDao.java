/**
 * 
 */
package com.thincovate.ih.workbench.admin.dao;

import java.sql.Types;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.models.Category;
import com.thincovate.ih.models.Entity;
import com.thincovate.ih.models.EntityType;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;
import com.thincovate.ih.models.User;
import com.thincovate.ih.models.UserType;

/**
 * @author Sandy
 *
 */
@Service
public class AdminDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	static Logger log = Logger.getLogger(AdminDao.class);

	private static String CREATE_CATEGORY = "insert into CATEGORIES(name,description,parent_category) values(?,?,?)";
	private static String UPDATE_CATEGORY = "update table CATEGORIES set name=?, description=?, parent_category=? where category_type=?";
	private static String CREATE_ENTITYTYPE = "insert into ENTITY_TYPES(name,description) values(?,?)";
	private static String UPDATE_ENTITYTYPE = "update table ENTITY_TYPES set name=?, description=? where type_id=?";
	private static String CREATE_ENTITY = "insert into ENTITIES(name, entity_type_id, logo, status, is_partner, date_created) values(?,?,?,?,?,?)";
	private static String UPDATE_ENTITY = "update table ENTITYIES set name=?, entity_type_id=?, logo=?, status=?, is_partner=? ,date_modified=? where entity_id=?";
	private static String CREATE_USERTYPE = "insert into USER_TYPES(name,description) values(?,?)";
	private static String UPDATE_USERTYPE = "update table USER_TYPES set name=?, description=? where type=?";
	private static String CREATE_USER = "insert into USERS(type, name, dob, email_address, mobile, landline, is_on_mailing_list, status, date_created) values(?,?,?,?,?,?,?,?,?)";
	private static String UPDATE_USER = "update table USERS set type=?, name=?, dob=?, email_address=?, mobile=?, landline=?, is_on_mailing_list=?, status=? where user_id=?";
	private static String CREATE_PRODUCT = "insert into Product(name, category_type, entity_id, rate_type_id, min_tenure, max_tenure, min_age, max_age, status, is_tied_up, date_created, modified_by) values(?,?,?,?,?,?,?,?,?,?)";
	private static String UPDATE_PRODUCT = "update table Product set name=?, category_type=?, entity_id=?, rate_type_id=?, min_tenure=?, max_tenure=?, min_age= ?, max_age= ?, status=?, is_tied_up=?, date_modified=?, modified_by=? where product_id=?";
	private static String CREATE_PRODUCT_INTEREST_RATES = "insert into product_interest_rates(interest_id, product_id, min_amount, max_amount, rate_of_interest, start_date, end_date, status, date_created,  modified_by) values(?,?,?,?,?,?,?,?,?,?,?)";
	private static String UPDATE_PRODUCT_INTEREST_RATES = "update table product_interest_rates set product_id=?, min_amount=?, max_amount=?, rate_of_interest=?, start_date=?, end_date=?, status=?,  date_modified=?, modified_by=? where interest_id=?";

	/**
	 * @param cat
	 * @return
	 */
	public boolean updateCategory(Category cat) {

		if(cat != null){
			if (cat.getCategoryId() > 0) {
				// update
				jdbcTemplate.update(UPDATE_CATEGORY, cat.getCategoryName(), cat.getCategoryDesc(), cat.getParentCategory(),
						cat.getCategoryId());
			} else {
				// create
				jdbcTemplate.update(CREATE_CATEGORY,
						new Object[] { cat.getCategoryName(), cat.getCategoryDesc(), cat.getParentCategory() }, new int[] {
								Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
			}
		}
		

		return true;
	}

	/**
	 * @param entityType
	 * @return
	 */
	public boolean updateEntityType(EntityType entityType) {

		if(entityType != null){
			if (entityType.getTypeId() > 0) {
				// update
				jdbcTemplate.update(UPDATE_ENTITYTYPE, entityType.getName(), entityType.getDescription(),
						entityType.getTypeId());
			} else {
				// create
				jdbcTemplate.update(CREATE_ENTITYTYPE, new Object[] { entityType.getName(), entityType.getDescription() },
						new int[] { Types.VARCHAR, Types.VARCHAR });
			}
		}
		
		return true;
	}

	/**
	 * @param entity
	 * @return
	 */
	public boolean updateEntity(Entity entity) {

		if (entity != null){
			if (entity.getEntityId() > 0) {
				// update
				jdbcTemplate.update(UPDATE_ENTITY,
						new Object[] { entity.getName(), entity.getEntityTypeId(), entity.getLogo(), entity.getStatus(),
								entity.getIsPartner(), CommonUtils.currentDateAsString(), entity.getEntityId() },
						new int[] { Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.VARCHAR, Types.VARCHAR,
								Types.DATE, Types.INTEGER });
			} else {
				// create
				jdbcTemplate.update(CREATE_ENTITY,
						new Object[] { entity.getName(), entity.getEntityTypeId(), entity.getLogo(), entity.getStatus(),
								entity.getIsPartner(), CommonUtils.currentDateAsString() }, new int[] { Types.VARCHAR,
								Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.VARCHAR, Types.VARCHAR, Types.DATE });
			}
		}

		return true;
	}

	/**
	 * @param userType
	 * @return
	 */
	public boolean updateUserType(UserType userType) {

		if (userType != null){
			if (userType.getType() > 0) {
				// update
				jdbcTemplate.update(UPDATE_USERTYPE,
						new Object[] { userType.getName(), userType.getDescription(), userType.getType() }, new int[] {
								Types.VARCHAR, Types.VARCHAR, Types.INTEGER });
			} else {
				// create
				jdbcTemplate.update(CREATE_USERTYPE, new Object[] { userType.getName(), userType.getDescription() },
						new int[] { Types.VARCHAR, Types.VARCHAR });
			}
		}

		return true;
	}

	/**
	 * @param user
	 * @return
	 */
	public boolean updateUser(User user) {

		if (user != null){
			if(user.getUserId() > 0) {
				// update
				jdbcTemplate.update(
						UPDATE_USER,
						new Object[] { user.getType(), user.getName(), user.getDob(), user.getEmailAddress(),
								user.getMobile(), user.getLandLine(), "Y", "ACTIVE", user.getUserId() }, new int[] {
								Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
								Types.VARCHAR, Types.INTEGER });
			} else {
				// create
				jdbcTemplate.update(
						CREATE_USER,
						new Object[] { user.getType(), user.getName(), user.getDob(), user.getEmailAddress(),
								user.getMobile(), user.getLandLine(), "Y", "ACTIVE", CommonUtils.currentDateAsString() },
						new int[] { Types.INTEGER, Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
								Types.VARCHAR, Types.VARCHAR, Types.DATE });
			}
		}

		return true;
	}

	/**
	 * @param product
	 * @return
	 */
	public boolean updateProduct(Product product) {

		if (product != null){
			if (product != null && Integer.parseInt(product.getId()) > 0) {
				// update
				jdbcTemplate.update(
						UPDATE_PRODUCT,
						new Object[] { product.getName(), product.getCatId(), product.getEntityId(), product.getRateType(),
								product.getMinTenure(), product.getMaxTenure(), product.getMinAge(), product.getMaxAge(), 
								product.getStatus(), "Y", CommonUtils.currentDateAsString(), "ADMIN", product.getId() }, 
								new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.DOUBLE, 
								Types.DOUBLE, Types.INTEGER, Types.INTEGER, Types.VARCHAR,
								Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.INTEGER });
			} else {
				// create
				jdbcTemplate.update(
						CREATE_PRODUCT,
						new Object[] { product.getName(), product.getCatId(), product.getEntityId(), product.getRateType(),
								product.getMinTenure(), product.getMaxTenure(), product.getMinAge(), product.getMaxAge(), 
								product.getStatus(), "Y", CommonUtils.currentDateAsString(), "ADMIN" }, 
								new int[] { Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.DOUBLE, 
								Types.DOUBLE, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
								Types.DATE, Types.VARCHAR });
			}
		}

		return true;
	}

	/**
	 * @param product
	 * @return
	 */
	public boolean updateProductInterestRates(Rates rates) {

		if (rates != null){
			if (rates != null && rates.getInterest_id() > 0) {
				// update
				jdbcTemplate.update(
						UPDATE_PRODUCT_INTEREST_RATES,
						new Object[] { rates.getProductId(), rates.getRangeLower(), rates.getRangeUpper(), rates.getRoi(),
								rates.getStartDate(), rates.getEndDate(), rates.getStatus(),
								CommonUtils.currentDateAsString(), "ADMIN", rates.getInterest_id() }, new int[] {
								Types.VARCHAR, Types.DECIMAL, Types.DECIMAL, Types.DECIMAL, Types.DATE, Types.DATE,
								Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.INTEGER });
			} else {
				// create
				jdbcTemplate.update(
						CREATE_PRODUCT_INTEREST_RATES,
						new Object[] { rates.getInterest_id(), rates.getProductId(), rates.getRangeLower(),
								rates.getRangeUpper(), rates.getRoi(), rates.getStartDate(), rates.getEndDate(),
								rates.getStatus(), CommonUtils.currentDateAsString(), "ADMIN" }, new int[] { Types.INTEGER,
								Types.VARCHAR, Types.DECIMAL, Types.DECIMAL, Types.DECIMAL, Types.DATE, Types.DATE,
								Types.VARCHAR, Types.DATE, Types.VARCHAR });
			}
		}

		return true;
	}

}
