package com.thincovate.ih.workbench.leads.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thincovate.ih.models.LeadReq;
import com.thincovate.ih.workbench.leads.services.LeadsServiceI;

@Controller
@RequestMapping("/lead")
public class LeadsController {
	static Logger log = Logger.getLogger(LeadsController.class);
	
	@Autowired
	private LeadsServiceI leadsSvc;
	
	@RequestMapping(value="/addLead", method= RequestMethod.POST)
    public @ResponseBody void  addLead(@RequestBody LeadReq lead, HttpServletRequest request) {
	      log.info("adding lead details start..");
	      leadsSvc.addLead(lead);
	      log.info("adding lead details end..");
	   }
	
	@RequestMapping(value="/deleteLead/{id}", method= RequestMethod.GET)
    public @ResponseBody void  deleteLead(@PathVariable("id") int id,HttpServletRequest request) {
	      log.info("deleting lead  start..");
	      leadsSvc.deleteLead(id);
	      log.info("deleting lead end..");
	   }
	
}
