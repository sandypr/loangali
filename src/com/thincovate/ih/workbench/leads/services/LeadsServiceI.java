/**
 * 
 */
package com.thincovate.ih.workbench.leads.services;

import com.thincovate.ih.models.LeadReq;

/**
 * @author Sandy
 *
 */
public interface LeadsServiceI {

	public void addLead(LeadReq lead);
	
	public void deleteLead(int prospectId);
}
