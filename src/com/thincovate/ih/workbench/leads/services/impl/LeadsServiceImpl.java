/**
 * 
 */
package com.thincovate.ih.workbench.leads.services.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.notifications.Notify;
import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.models.LeadReq;
import com.thincovate.ih.workbench.leads.dao.LeadsDao;
import com.thincovate.ih.workbench.leads.services.LeadsServiceI;
import com.thincovate.ih.workbench.prospects.controller.ProspectsController;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;
import com.thincovate.ih.workbench.prospects.services.ProspectsServiceI;

/**
 * @author sandy
 *
 */
@Service
public class LeadsServiceImpl implements LeadsServiceI {
	static Logger log = Logger.getLogger(ProspectsController.class);
	
	@Autowired
	private LeadsDao leadsDao;
	@Autowired
	private ProspectsServiceI prospectsService;
	@Autowired
	private NotificationServiceI notifyService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thincovate.ih.workbench.leads.services.LeadsServiceI#addLead(java
	 * .lang.Object)
	 */
	@Override
	public void addLead(final LeadReq lead) {

		leadsDao.addLead(lead);

		final ProspectLoans prospect;
		prospect = prospectsService.findProspectById(lead.getProspectId());

		Thread thread = new Thread() {
			public void run() {
				log.info("Running thread for email notification..");
				try {
					String finalAttachmentContent = prepareEmailContent(prospect, lead, Notify.getInstance().getProperty("mail.email.confirmation_content"));
					String finalCoverContent = prepareEmailContent(prospect, lead, Notify.getInstance().getProperty("mail.email.confirmation_coverLetter"));
					notifyService.sendEligiEmailNotification(prospect.getEmailAddress(), finalCoverContent, finalAttachmentContent, Notify.getInstance().getProperty("mail.email.confirmation_applyloan"));
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		};
		// TODO:
		// change notification methods to take prospect object for name,email,fields or pass them seperately
		thread.start();

	}

	@Override
	public void deleteLead(int prospectId) {
		leadsDao.deleteLead(prospectId);
	}
	
	private String prepareEmailContent(ProspectLoans prospect, LeadReq lead, String content){
		
		String NAME_STRING = "###NAME_STRING###";
		String DATE_STRING = "###DATE_STRING###";
		String MOBILE_STRING = "###MOBILE_STRING###";
		String EMAIL_STRING = "###EMAIL_STRING###";
		String CITY_STRING = "###CITY_STRING###";
		String CATEGORY_STRING = "###CATEGORY_STRING###";
		String PRODUCTNAME_STRING = "###PRODUCTNAME_STRING###";
		String BANK_STRING = "###BANK_STRING###";
		String ELIGIBILITY_STRING = "###ELIGIBILITY_STRING###";
		String EMI_STRING = "###EMI_STRING###";
		String ROI_STRING = "###ROI_STRING###";
		String TENURE_STRING = "###TENURE_STRING###";
		String PROSPECT_NUMBER = "###PROSPECT_NUMBER###";
		
		StringBuilder string = new StringBuilder(content);
		
		string = string.replace(string.indexOf(NAME_STRING), string.indexOf(NAME_STRING)+NAME_STRING.length(), prospect.getName());
		string = string.replace(string.indexOf(DATE_STRING), string.indexOf(DATE_STRING)+DATE_STRING.length(), String.valueOf(CommonUtils.currentDateLocalFormat()));
		string = string.replace(string.indexOf(MOBILE_STRING), string.indexOf(MOBILE_STRING)+MOBILE_STRING.length(), prospect.getMobile());
		string = string.replace(string.indexOf(EMAIL_STRING), string.indexOf(EMAIL_STRING)+EMAIL_STRING.length(), prospect.getEmailAddress());
		string = string.replace(string.indexOf(CITY_STRING), string.indexOf(CITY_STRING)+CITY_STRING.length(), prospect.getWorkLocation());
		string = string.replace(string.indexOf(PROSPECT_NUMBER), string.indexOf(PROSPECT_NUMBER)+PROSPECT_NUMBER.length(), String.valueOf(prospect.getProspectId()));
		if(string.indexOf(CATEGORY_STRING) > 0)
			string = string.replace(string.indexOf(CATEGORY_STRING), string.indexOf(CATEGORY_STRING)+CATEGORY_STRING.length(), String.valueOf(lead.getCategoryName()));
		if(string.indexOf(PRODUCTNAME_STRING) > 0)
			string = string.replace(string.indexOf(PRODUCTNAME_STRING), string.indexOf(PRODUCTNAME_STRING)+PRODUCTNAME_STRING.length(), lead.getProductName());
		if(string.indexOf(BANK_STRING) > 0)
			string = string.replace(string.indexOf(BANK_STRING), string.indexOf(BANK_STRING)+BANK_STRING.length(), lead.getEntityName());
		if(string.indexOf(ELIGIBILITY_STRING) > 0)
			string = string.replace(string.indexOf(ELIGIBILITY_STRING), string.indexOf(ELIGIBILITY_STRING)+ELIGIBILITY_STRING.length(), String.valueOf(lead.getAmount()));
		if(string.indexOf(EMI_STRING) > 0)
			string = string.replace(string.indexOf(EMI_STRING), string.indexOf(EMI_STRING)+EMI_STRING.length(), String.valueOf(lead.getEmi()));
		if(string.indexOf(ROI_STRING) > 0)
			string = string.replace(string.indexOf(ROI_STRING), string.indexOf(ROI_STRING)+ROI_STRING.length(), String.valueOf(lead.getRoi()));
		if(string.indexOf(TENURE_STRING) > 0)
			string = string.replace(string.indexOf(TENURE_STRING), string.indexOf(TENURE_STRING)+TENURE_STRING.length(), String.valueOf(lead.getTenure()));
		
		System.out.println(string);
		return string.toString();
	}
	
	public static void main(String[] args){
		
		String content = "hii, ###NAME_STRING### !!  welcome to our space.";
		StringBuilder string = new StringBuilder(content);
		System.out.println(string.toString());
		int start = content.indexOf("###NAME_STRING###");
		System.out.println(start+"--"+"###NAME_STRING###".length());
		string = string.replace(start, "###NAME_STRING###".length()+start, "Sandeep P");
		System.out.println(string.toString());
	}

}
