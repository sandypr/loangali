/**
 * 
 */
package com.thincovate.ih.workbench.leads.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.thincovate.ih.models.LeadReq;


/**
 * @author rajani
 *
 */

@Service
public class LeadsDao {

	static Logger log = Logger.getLogger(LeadsDao.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static String COL_PROSPECT_ID = "prospect_id";
	private static String COL_AMOUNT = "amount";
	private static String COL_TENURE = "tenure";
	private static String COL_PRODUCT_ID = "product_id";
	private static String COL_STATUS = "status";
	private static String COL_DATE_CREATED = "date_created";
	private static String DELETE_QUERY_LEADS = "Delete from Leads where prospect_id= ?" ;

	public void addLead(final LeadReq lead) {

		log.info("Inserting Lead...");

		// insert
		final String sql = "INSERT INTO leads (" + COL_PROSPECT_ID + "," + COL_AMOUNT + "," + COL_TENURE + ","
				+ COL_PRODUCT_ID + "," + COL_STATUS + "," + COL_DATE_CREATED + ")" + " VALUES (?, ?, ?, ?, ?, ?)";

		jdbcTemplate.update(sql, lead.getProspectId(), lead.getAmount(), lead.getTenure(), lead.getProductId(), "NEW",
				new java.sql.Date(System.currentTimeMillis()));

		log.info("Inserting lead...done");

	}
	
	public void deleteLead(int id){
		
		 Object[] params = {id};
		 int rows = jdbcTemplate.update(DELETE_QUERY_LEADS, params);
		 log.info(rows + " row(s) deleted from prospect-loans.");
	}

}
