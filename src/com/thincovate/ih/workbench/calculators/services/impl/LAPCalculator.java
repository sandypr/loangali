/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import org.apache.log4j.Logger;

import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;

/**
 * @author Sandy
 *
 */
public class LAPCalculator extends AbstractLoanCalculator {
	static Logger log = Logger.getLogger(LAPCalculator.class);
	public LAPCalculator(CalculatorDao calcDao) {
		super.loanType = InvestHuntConstants.LAP_LOAN_TYPE;
		this.calcDao = calcDao;
	}

}
