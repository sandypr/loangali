/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import org.apache.log4j.Logger;

import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;

/**
 * @author Sandy
 *
 */
public class HomeLoanCalculator extends AbstractLoanCalculator {

	static Logger log = Logger.getLogger(HomeLoanCalculator.class);
	
	/**
	 * constructor
	 */
	public HomeLoanCalculator(CalculatorDao calcDao) {
		super.loanType = InvestHuntConstants.HOME_LOAN_TYPE;
		this.calcDao = calcDao;
	}

}
