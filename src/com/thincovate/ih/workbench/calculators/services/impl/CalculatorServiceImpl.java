/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;
import com.thincovate.ih.workbench.calculators.services.CalculatorServiceI;

/**
 * @author Sandy
 *
 */
@Service
public class CalculatorServiceImpl implements CalculatorServiceI {

	@Autowired
	private CalculatorDao calcDao;
	private AbstractLoanCalculator loanCalc = null;

	static Logger log = Logger.getLogger(CalculatorServiceImpl.class);

	@Override
	public double calculateEMIEligibility(int type, double salary, double commitments) {

		loanCalc = CalculatorFactory.getLoanCalcInstange(type, calcDao);
		log.info("Acquired AbstractorCalculator instance");
		double emiEligibility = 0.0;

		emiEligibility = loanCalc.calculateEMIEligibility(salary, commitments);
		emiEligibility = (emiEligibility > 0) ? emiEligibility : 0.0;

		return Math.ceil(emiEligibility);
	}

	@Override
	public double calculateLoanEligibilityAmount(int type, double salary, double commitments, int tenure) {

		loanCalc = CalculatorFactory.getLoanCalcInstange(type, calcDao);
		log.info("Acquired AbstractorCalculator instance");
		double eligibleLoanAmount = 0.0;

		eligibleLoanAmount = loanCalc.calculateLoanEligibilityAmount(salary, commitments, tenure);
		eligibleLoanAmount = (eligibleLoanAmount > 0) ? eligibleLoanAmount : 0.0;

		return eligibleLoanAmount;
	}

	/*@Override
	public Object retrieveLoanComparisions(int type, double salary, double commitments) {
		log.info("Pulling out comparisions..");
		double eligibleLoanAmount = 0.0;
		List<Product> prods = null;

		eligibleLoanAmount = calculateLoanEligibilityAmount(type, salary, commitments, LOAN_TENURE_DEFAULT);
		prods = mdService.findAllProductsForEligibility();

		return null;
	}*/

}
