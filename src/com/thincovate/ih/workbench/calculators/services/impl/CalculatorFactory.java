/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;

/**
 * @author Sandy
 *
 */
public class CalculatorFactory {

	/**
	 * @param type
	 * @return
	 */
	public static AbstractLoanCalculator getLoanCalcInstange(int type, CalculatorDao dao){
		
		AbstractLoanCalculator calcInstance = null;
		
		// create an instance of loan calculator
		if(type == 2)
			calcInstance = new HomeLoanCalculator(dao);
		else if(type == 3)
			calcInstance = new PersonalLoanCalculator(dao);
		else if(type == 4)
			calcInstance = new PersonalLoanCalculator(dao);
		
		return calcInstance;
	}

}
