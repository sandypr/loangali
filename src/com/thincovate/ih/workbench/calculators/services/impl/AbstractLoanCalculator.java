/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;

/**
 * @author Sandy
 *
 */
public abstract class AbstractLoanCalculator {

	@Autowired
	protected CalculatorDao calcDao;
	protected String loanType = null;
	static Logger log = Logger.getLogger(AbstractLoanCalculator.class);

	/**
	 * Calculates the emi eligibility figure for give income and commitments
	 * 
	 * @param income
	 * @param commitments
	 * @return
	 */
	protected double calculateEMIEligibility(double income, double commitments) {

		double emiEligibility = 0.0;

		Map<String, Object> eligibilityObject = null;

		eligibilityObject = retrieveROInEligibility(income, 0, loanType);
		log.info(eligibilityObject.get(InvestHuntConstants.EMI_ELIGIBILITY_OUTPARAM));
		if (!eligibilityObject.isEmpty())
			emiEligibility = Double.parseDouble(eligibilityObject.get(InvestHuntConstants.EMI_ELIGIBILITY_OUTPARAM)
					.toString());
		
		double finalEmiEligibility = emiEligibility - commitments;
		log.info("Calculated emi eligibility : " + finalEmiEligibility);

		return finalEmiEligibility;
	}

	/**
	 * Calculates the loanEligibility figure for a given income, commitments and
	 * expected tenure.
	 * 
	 * @param income
	 * @param commitments
	 * @param tenure
	 * @return
	 */
	protected double calculateLoanEligibilityAmount(double income, double commitments, int tenure) {
		double eligibleLoanAmount = 0.0;

		// pull emi eligibility from income and commitments
		Map<String, Object> emiEligibility = null;

		emiEligibility = retrieveROInEligibility(income, 0, loanType);
		log.info("Retrieved emi eligibility from FOIR structure");

		// pull interest rate(s) - min_rate_of_interest
		double interestRate = 0.0;
		interestRate = Double.parseDouble(emiEligibility.get(InvestHuntConstants.MIN_RATE_OF_INTEREST_OUTPARAM)
				.toString());

		double monthlyIR = interestRate / 100 / 12;
		double nume = Math.pow((1 + monthlyIR), tenure * 12);
		double emiForOneLakh = (nume / (nume - 1)) * monthlyIR;

		eligibleLoanAmount = (Double.parseDouble(emiEligibility.get(InvestHuntConstants.EMI_ELIGIBILITY_OUTPARAM)
				.toString()) - commitments) / emiForOneLakh;
		eligibleLoanAmount = (double) Math.round(eligibleLoanAmount * 10000) / 10000;

		return eligibleLoanAmount;
	}

	/**
	 * Finds out the product with least rate_of_interest
	 * 
	 * @param typeOfLoan
	 * @return
	 */
	protected String findLeastRateOfInterest(String typeOfLoan) {
		return "";
	}

	/**
	 * @param typeOfLoan
	 * @param salary
	 * @return
	 */
	protected double getFOIRData(String typeOfLoan, double salary) {
		return 0.0;
	}

	/**
	 * Pulls out the eligibility object for a type of loan
	 * 
	 * @param income
	 * @param commitments
	 * @param type
	 * @return
	 */
	private Map<String, Object> retrieveROInEligibility(double income, double commitments, String type) {
		Map<String, Object> eligibilityObject = null;

		eligibilityObject = calcDao.findEMIEligibility(type, income, commitments);
		log.info("Retrived Roi and eligibility details : " + eligibilityObject);

		return eligibilityObject;
	}

}
