/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services.impl;

import org.apache.log4j.Logger;

import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.workbench.calculators.dao.CalculatorDao;

/**
 * @author Sandy
 *
 */
public class PersonalLoanCalculator extends AbstractLoanCalculator {

	static Logger log = Logger.getLogger(PersonalLoanCalculator.class);
	
	/**
	 * constructor
	 */
	public PersonalLoanCalculator(CalculatorDao calcDao) {
		super.loanType = InvestHuntConstants.PERSONAL_LOAN_TYPE;
		this.calcDao = calcDao;
	}

}
