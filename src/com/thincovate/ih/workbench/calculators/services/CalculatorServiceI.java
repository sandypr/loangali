/**
 * 
 */
package com.thincovate.ih.workbench.calculators.services;

/**
 * @author Sandy
 *
 */
public interface CalculatorServiceI {
	public double calculateEMIEligibility(int type, double salary, double commitments);
	public double calculateLoanEligibilityAmount(int type, double salary, double commitments, int tenure );
	//public Object retrieveLoanComparisions(int type, double salary, double commitments);
}
