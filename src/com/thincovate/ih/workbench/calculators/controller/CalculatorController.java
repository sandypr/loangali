/**
 * 
 */
package com.thincovate.ih.workbench.calculators.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.workbench.calculators.services.CalculatorServiceI;


/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/calc")
public class CalculatorController {
	
	static Logger log = Logger.getLogger(CalculatorController.class);
	
	@Autowired
	private CalculatorServiceI calcService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/emiEligibility/{type}/{salary}/{commitments}", produces = "application/json")
	public @ResponseBody Object calcEmiEligibility(@PathVariable("type") int type, @PathVariable("salary") double salary, @PathVariable("commitments") double commitments){
		
		log.info("Calculating EMI Eligibility for "+type+" : Start "+commitments);
		
		double eligibileEmi = 0.0;
		String errorMsg;
		
		errorMsg = CommonUtils.validIncome(salary, commitments);
		
		if(errorMsg != null){
			return eligibileEmi;
		}
		else{
			eligibileEmi = calcService.calculateEMIEligibility(type, salary, commitments);
		}
				
		log.info("Calculating EMI Eligibility for "+type+" : Done");
		return eligibileEmi;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/loanEligibilityAmount/{type}/{salary}/{commitments}/{tenure}", produces = "application/json")
	public @ResponseBody Object calcLoanEligibilityAmount(@PathVariable("type") int type,
													@PathVariable("salary") double salary, 
													@PathVariable("commitments") double commitments, 
													@PathVariable("tenure") int tenure, HttpServletRequest req){
		
		log.info("Calculating Loan Eligibility for "+type+" : Start");
		double eligibileLoan = 0.0;
		String errorMsg;
		
		errorMsg = CommonUtils.validIncome(salary, commitments);
		
		if(errorMsg != null){
			return errorMsg;
		}
		else{
			eligibileLoan = calcService.calculateLoanEligibilityAmount(type, salary, commitments, tenure );
		}
		
		log.info("Calculating Loan Eligibility for "+type+" : Done");
		return eligibileLoan;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getCal() {
		return "calculators";
	}
	
	@RequestMapping(value = "/PL", method = RequestMethod.GET)
	public String getCalPL() {
		return "calculatorsPL";
	}
	
	@RequestMapping(value = "/LAP", method = RequestMethod.GET)
	public String getCalLAP() {
		return "calculatorsLAP";
	}
	
}
