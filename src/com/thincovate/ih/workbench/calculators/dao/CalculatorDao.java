/**
 * 
 */
package com.thincovate.ih.workbench.calculators.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;


/**
 * @author Sandy
 *
 */
@Service
public class CalculatorDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static String PROC_EMI_ELIGIBILITY = "calcEmiEligibility";

	private static String INTEREST_RATES_FOR_GENERIC_CALC = "SELECT min(rate_of_interest) FROM product_interest_rates WHERE status = 'Active' AND product_id = 1";

	/**
	 * Pulls out FOIR data for an income amount
	 * 
	 * @param income
	 * @return
	 */
	public Map<String, Object> findEMIEligibility(String type, double income, double commitments) {

		String PARAM_INCOME = "income";
		String PARAM_COMMITMENTS = "commitments";
		String PARAM_CATEGORY = "cat_type";

		Map<String, Object> eligibilityMap = null;

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put(PARAM_INCOME, income);
		inParamMap.put(PARAM_COMMITMENTS, commitments);
		inParamMap.put(PARAM_CATEGORY, type);

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(PROC_EMI_ELIGIBILITY);

		eligibilityMap = simpleJdbcCall.execute(in);
        System.out.println("eligibilityMap: " +eligibilityMap);
		return eligibilityMap;
	}

	/**
	 * Pulls out Interest Rate for a loan amount
	 * 
	 * @param loanAmount
	 * @return
	 */
	public double getInterestRateForCalc() {

		double eligibleRate = 0.0;

		eligibleRate = jdbcTemplate.queryForObject(INTEREST_RATES_FOR_GENERIC_CALC, Double.class);

		return eligibleRate;
	}

}
