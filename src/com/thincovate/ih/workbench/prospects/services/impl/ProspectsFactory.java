/**
 * 
 */
package com.thincovate.ih.workbench.prospects.services.impl;

import com.thincovate.ih.workbench.prospects.dao.ProspectsDao;


/**
 * @author rajani
 *
 */
public class ProspectsFactory {

	public static AbstractProspectsService getProspectInstance(int type, ProspectsDao prospectsDao) {

		AbstractProspectsService prospectsLoanSvc = null;
		// create an instance of loan calculator
		if (type == 2 || type == 3 || type == 4  || type == 5 )
			prospectsLoanSvc = new ProspectsLoanService(prospectsDao);

		return prospectsLoanSvc;
	}

}
