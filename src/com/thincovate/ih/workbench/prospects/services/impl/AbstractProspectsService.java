/**
 * 
 */
package com.thincovate.ih.workbench.prospects.services.impl;

import com.thincovate.ih.workbench.prospects.dao.ProspectsDao;


/**
 * @author rajani
 *
 */
public abstract class AbstractProspectsService {
	
	ProspectsDao prospectsDao;

	protected abstract long addProspect(Object prospect);
}
