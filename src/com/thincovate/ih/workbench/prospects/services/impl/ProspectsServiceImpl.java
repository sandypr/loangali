/**
 * 
 */
package com.thincovate.ih.workbench.prospects.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.notifications.Notify;
import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.workbench.prospects.dao.ProspectsDao;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;
import com.thincovate.ih.workbench.prospects.services.ProspectsServiceI;

/**
 * @author Sandy
 *
 */
@Service
public class ProspectsServiceImpl implements ProspectsServiceI {

	@Autowired
	ProspectsDao prospectsDao;
	@Autowired
	private NotificationServiceI notifyService;

	private AbstractProspectsService prospectsSvc = null;

	static Logger log = Logger.getLogger(ProspectsServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thincovate.ih.workbench.prospects.services.ProspectsServiceI#addProspect
	 * (java.lang.Object)
	 */
	@Override
	public long addProspect(Object prospect, String type) {
		log.info("Acquiring prospect Instace for type : " + type);

		prospectsSvc = ProspectsFactory.getProspectInstance(Integer.parseInt(type), prospectsDao);
		log.info("Acquired prospect instance :" + prospectsSvc);
		
		// following changes are for first version of loangully.
		ProspectLoans prospectObj = (ProspectLoans) prospect;
		long prospectId;
		if(prospectObj.getProspectId() <= 0) {
			prospectId = prospectsSvc.addProspect(prospect);
			final ProspectLoans prospectLoans = (ProspectLoans) prospect;
			Thread thread = new Thread() {
				public void run() {
					log.info("Running thread for email notification..");
					try {
						String emailContent = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.confirmation_coverLetter"), prepareProspectStringsMap(prospectLoans));
						notifyService.notifyConfirmationByEmail(prospectLoans.getEmailAddress()+","+Notify.getInstance().getProperty("mail.email.support.emaillist"), emailContent, Notify.getInstance().getProperty("mail.email.confirmation_applyloan"));
					} catch (IOException e) {
						e.printStackTrace();
					} 
				}
			};
			log.info("Starting thread for email notification..");
			thread.start();
		}else{
			prospectId = prospectsSvc.addProspect(prospect);
		}
		
		return prospectId;
		//return prospectsSvc.addProspect(prospect);
	}
	
	@Override
	public ProspectLoans findProspectById(int id){
		return prospectsDao.findProspectById(id);
	}

	@Override
	public void deleteProspect(int id) {
		prospectsDao.deleteProspect(id);
	}
	
	// private methods
	private Map<String,String> prepareProspectStringsMap(ProspectLoans prospect){
		//log.info("preparing strings - "+prospect.getProspectId());
		Map<String,String> map = new HashMap<>();
		map.put("###DATE_STRING###", String.valueOf(CommonUtils.currentDateLocalFormat()));
		map.put("###NAME_STRING###", (prospect.getName()==null)? "":prospect.getName());
		map.put("###CITY_STRING###", (prospect.getWorkLocation()==null)? "":prospect.getWorkLocation());
		map.put("###MOBILE_STRING###", (prospect.getMobile()==null)? "":prospect.getMobile());
		map.put("###EMAIL_STRING###", (prospect.getEmailAddress()==null)? "":prospect.getEmailAddress());
		map.put("###PROSPECT_NUMBER###", ((Integer)prospect.getProspectId() <= 0)? "":CommonUtils.currentDateInCharacters()+"-"+String.valueOf(prospect.getProspectId()));
	
		return map;
	}

}
