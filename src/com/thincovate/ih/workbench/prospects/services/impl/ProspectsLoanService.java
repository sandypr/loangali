/**
 * 
 */
package com.thincovate.ih.workbench.prospects.services.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.thincovate.ih.workbench.calculators.services.CalculatorServiceI;
import com.thincovate.ih.workbench.prospects.dao.ProspectsDao;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

/**
 * @author rajani
 *
 */
public class ProspectsLoanService extends AbstractProspectsService {

	@Autowired
	private CalculatorServiceI calcService;

	ProspectsLoanService(ProspectsDao prospectLoanDao) {
		this.prospectsDao = prospectLoanDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.thincovate.ih.workbench.prospects.services.impl.AbstractProspectsService
	 * #addProspect(java.lang.Object)
	 */
	@Override
	protected long addProspect(Object prospect) {
		ProspectLoans prospectLoans = (ProspectLoans) prospect;
		long prospectId = prospectsDao.addProspectLoan(prospectLoans);
		if(prospectLoans.getProspectId() > 0){
			return prospectLoans.getProspectId();
		}else{
			prospectLoans.setProspectId((int) prospectId);
			return prospectId;
		}
	}

}
