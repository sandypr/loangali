/**
 * 
 */
package com.thincovate.ih.workbench.prospects.services;

import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

/**
 * @author Sandy
 *
 */
public interface ProspectsServiceI {

	long addProspect(Object prospect, String type);
	ProspectLoans findProspectById(int id);
	void deleteProspect(int id);
}
