package com.thincovate.ih.workbench.prospects.controller;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thincovate.ih.workbench.calculators.services.CalculatorServiceI;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;
import com.thincovate.ih.workbench.prospects.services.ProspectsServiceI;

@Controller
public class ProspectsController {
	static Logger log = Logger.getLogger(ProspectsController.class);

	@Autowired
	private ProspectsServiceI prospectsService;

	@Autowired
	private CalculatorServiceI calcService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/addProspect", produces = "application/json")
	public @ResponseBody long addProspect(@RequestBody ProspectLoans prospect) throws ParseException {
		
		long id;
		
		if(prospect != null){
			if(prospect.getProspectId() < 1){
				log.info("Adding a new prospect " + " : Start"+prospect.getCategoryType()+":"+prospect.getAge());
			}else{
				log.info("Updating prospect " + " : Start for prospect id"+ ":"+prospect.getProspectId());
			}
			
			double eligibilitySum = calcService.calculateLoanEligibilityAmount(prospect.getCategoryType(),
					prospect.getMonthlyIncome(), prospect.getCommitments(), 10);
			prospect.setEligibleSum(eligibilitySum);
			
			id = prospectsService.addProspect(prospect, String.valueOf(prospect.getCategoryType()));
			log.info("Adding/updating prospect " + " : Done and Id is:" + id);
		}else
			throw new DataIntegrityViolationException("400 : Bad Request. Data Integrity Violation");
		
		return id;
	}
	
	@RequestMapping(value="/deleteProspect/{id}", method= RequestMethod.GET)
    public @ResponseBody void  deleteProspect(@PathVariable("id") int id,HttpServletRequest request) {
	      log.info("deleting prospect  start..");
	      prospectsService.deleteProspect(id);
	      log.info("deleting prospect end..");
	   }
	
	@RequestMapping(method = RequestMethod.POST, value = "/addEduProspect", produces = "application/json")
	public @ResponseBody long addELProspect(@RequestBody ProspectLoans prospect) throws ParseException {
		
		long id;
		
		if(prospect != null){
			if(prospect.getProspectId() < 1){
				log.info("Adding a new education loan prospect " + " : Start"+prospect.getCategoryType()+":"+prospect.getAge());
			}else{
				log.info("Updating education loan prospect " + " : Start for prospect id"+ ":"+prospect.getProspectId());
			}
			
			id = prospectsService.addProspect(prospect, String.valueOf(prospect.getCategoryType()));
			log.info("Adding/updating education loan prospect " + " : Done and Id is:" + id);
		}else
			throw new DataIntegrityViolationException("400 : Bad Request. Data Integrity Violation");
		
		return id;
		
	}

}
