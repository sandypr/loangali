/**
 * 
 */
package com.thincovate.ih.workbench.prospects.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.workbench.prospects.model.CoApplicant;
import com.thincovate.ih.workbench.prospects.model.CourseDetails;
import com.thincovate.ih.workbench.prospects.model.ProspectLoans;

/**
 * @author rajani
 *
 */
@Service
public class ProspectsDao {

	static Logger log = Logger.getLogger(ProspectsDao.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static String COL_NAME = "name";
	private static String COL_EMAIL_ADDRESS = "email_address";
	private static String COL_EMAIL = "email";
	private static String COL_MOBILE = "mobile";
	private static String COL_DOB = "dob";
	private static String COL_AGE = "age";
	private static String COL_COMPANY = "company";
	private static String COL_WORK_LOCATION = "work_location";
	private static String COL_COMMITMENTS = "commitments";
	private static String COL_CATEGORY_TYPE = "category_type";
	private static String COL_MONTHLY_INCOME = "monthly_income";
	private static String COL_DATE_CREATED = "date_created";
	private static String COL_ELIGIBLE_SUM = "eligible_sum";
	private static String COL_HAS_CO_APPLICANT = "has_co_applicant";
	private static String COL_IS_ON_MAILING_LIST = "is_on_mailing_list";
	private static String COL_EMPLOYMENT_TYPE = "employment_type";
	private static String COL_OCCUPATION = "occupation";
	private static String COL_PROSPECT_ID = "prospect_id";
	private static String COL_RELATION_WITH_PROSPECT = "relation_with_prospect";
	private static String COL_PURPOSE = "purpose";
	private static String COL_DESIGNATION = "designation";
	private static String COL_TYPE_OF_COMPANY = "type_of_company";
	private static String COL_YEAR_OF_ESTABLISHMENT = "year_of_establishment";
	//private static String COL_YEARS_IN_CURRENT_LOCATION = "years_in_current_location";
	private static String COL_RESIDENTIAL_STATUS = "residential_status";
	private static String COL_EMPLOYED_SINCE = "employed_since";
	private static String COL_ASSEST_LOCATION = "asset_location";
	public static final String DELETE_QUERY_PROSPECT_LOANS = "DELETE FROM prospect_loans WHERE prospect_id = ?";
	public static final String DELETE_QUERY_CO_APPLCANT = "DELETE FROM prospect_co_applicants WHERE prospect_id = ?";
	
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

	private static final String COL_COURSE_TYPE = "course_type";
	private static final String COL_COURSE_NAME = "course_name";
	private static final String COL_COURSE_DURATION = "course_duration";
	private static final String COL_UNIVERSITY = "university_college";
	private static final String COL_COURSE_COUNTRY = "course_country";

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	public Long addProspectLoan(final ProspectLoans prospect) {

		log.info("Inserting prospect..." + prospect.getName());

		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		if(prospect.getEmploymentType() == null){
			prospect.setEmploymentType("STUDENT");
		}else if(prospect.getEmploymentType().equals("salaried")){
			prospect.setEmploymentType("SALARIED");
		}else if(prospect.getEmploymentType().equals("selfEmployed")){
			prospect.setEmploymentType("SELF EMPLOYED");
		}

		if (prospect.getProspectId() > 0) {
			
			if (prospect.getHasCoApplicant() != null && prospect.getHasCoApplicant().equalsIgnoreCase("on"))
				prospect.setHasCoApplicant("Y");
			else
				prospect.setHasCoApplicant("N");
			
			// update
			String sql = "UPDATE prospect_loans SET " + COL_NAME + "=?," + COL_EMAIL_ADDRESS + "=?," + COL_MOBILE
					+ "=?, " + COL_AGE + "=?," + COL_COMPANY + "=?," + COL_WORK_LOCATION + "=?," + COL_COMMITMENTS
					+ "=?," + COL_CATEGORY_TYPE + "=?," + COL_MONTHLY_INCOME + "=?," +  COL_ELIGIBLE_SUM + "=?,"
					+ COL_PURPOSE + "=?," + COL_DESIGNATION + "=?,"+ COL_TYPE_OF_COMPANY + "=?," + COL_YEAR_OF_ESTABLISHMENT + "=?," 
					+ COL_EMPLOYED_SINCE + "=?," + COL_RESIDENTIAL_STATUS + "=?," + COL_EMPLOYMENT_TYPE + "=?," + COL_HAS_CO_APPLICANT + "=?,"
					+ COL_IS_ON_MAILING_LIST + "=?,"	+ COL_ASSEST_LOCATION + "=?"
					+ " WHERE prospect_id=?";

			jdbcTemplate.update(sql, prospect.getName(), prospect.getEmailAddress(), prospect.getMobile(),
					prospect.getAge(), prospect.getCompany(), prospect.getWorkLocation(),
					prospect.getCommitments(), prospect.getCategoryType(), prospect.getMonthlyIncome(), prospect.getEligibleSum(),
					prospect.getPurpose(), prospect.getDesignation(), prospect.getTypeOfCompany(), prospect.getYearOfEstablishment(),
					prospect.getEmployedSinceMonths(), prospect.getResStatus(), prospect.getEmploymentType(), prospect.getHasCoApplicant(),
					"Y", prospect.getAssetLocation(), prospect.getProspectId());
		} else {
			// insert
			final String sql = "INSERT INTO prospect_loans (" + COL_NAME + "," + COL_EMAIL_ADDRESS + "," + COL_MOBILE
					+ ","  + COL_AGE + "," + COL_COMPANY + "," + COL_WORK_LOCATION + "," + COL_COMMITMENTS + ","
					+ COL_CATEGORY_TYPE + "," + COL_MONTHLY_INCOME + "," + COL_DATE_CREATED + "," + COL_ELIGIBLE_SUM
					+ "," + COL_PURPOSE + "," + COL_DESIGNATION + "," + COL_TYPE_OF_COMPANY + ","
					+ COL_YEAR_OF_ESTABLISHMENT + "," + COL_EMPLOYED_SINCE + "," + COL_RESIDENTIAL_STATUS
					+ "," + COL_EMPLOYMENT_TYPE + "," + COL_HAS_CO_APPLICANT + "," + COL_IS_ON_MAILING_LIST + "," + COL_ASSEST_LOCATION + ")"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?)";

			jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" });
					ps.setString(1, prospect.getName());
					ps.setString(2, prospect.getEmailAddress());
					ps.setString(3, prospect.getMobile());
					ps.setDouble(4, prospect.getAge());
					ps.setString(5, prospect.getCompany());
					ps.setString(6, prospect.getWorkLocation());
					ps.setDouble(7, prospect.getCommitments());
					ps.setInt(8, prospect.getCategoryType());
					ps.setDouble(9, prospect.getMonthlyIncome());
					ps.setDate(10, new java.sql.Date(System.currentTimeMillis()));
					ps.setDouble(11, prospect.getEligibleSum());

					ps.setString(12, prospect.getPurpose());
					ps.setString(13, prospect.getDesignation());
					ps.setString(14, prospect.getTypeOfCompany());
					ps.setInt(15, prospect.getYearOfEstablishment());
					ps.setDouble(16, prospect.getEmployedSinceMonths());
					ps.setString(17, prospect.getResStatus());

					ps.setString(18, prospect.getEmploymentType());
					if (prospect.getHasCoApplicant() != null && prospect.getHasCoApplicant().equalsIgnoreCase("on"))
						ps.setString(19, "Y");
					else
						ps.setString(19, "N");
					ps.setString(20, "Y");
					ps.setString(21, prospect.getAssetLocation());
					return ps;
				}
			}, keyHolder);

		}

		if (prospect.getHasCoApplicant() != null && prospect.getHasCoApplicant().equalsIgnoreCase("on")) {
			log.info("Inserted prospect id :" +((Long) keyHolder.getKey()).intValue());
			prospect.getCoApplicant().setProspectId(((Long) keyHolder.getKey()).intValue());
			addCoApplicant(prospect.getCoApplicant());
			
			if(prospect.getCategoryType() == 5)
				addCourseDetails(prospect.getCoApplicant().getProspectId(), prospect.getCourse());
		}

		log.info("Inserting prospect...done");
		
		return (Long) keyHolder.getKey();
	}
	
	private void addCourseDetails(int prospectId, CourseDetails course){
		
		log.info("Inserting Course Details..");
		
		String sql = "INSERT INTO course_details VALUES (?,?,?,?,?,?)";

		jdbcTemplate.update( sql, new Object[] {prospectId, course.getCourseType(), course.getCourseName(), course.getCourseDuration(), course.getUniversity(), course.getCountry()},
									new int[] {Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});
		
		log.info("Inserting Course Details..Done");
		
	}

	private Long addCoApplicant(final CoApplicant coApplicant) {

		log.info("Inserting co-applicant..");

		KeyHolder keyHolder = new GeneratedKeyHolder();

		if (coApplicant.getCoApplicantId() > 0) {
			// update
			String sql = "UPDATE prospect_co_applicants SET " + COL_PROSPECT_ID + "=?," + COL_NAME + "=?," + COL_EMAIL
					+ "=?," + COL_MOBILE + "=?, " + COL_DOB + "=?," + COL_AGE + "=?," + COL_COMMITMENTS + "=?," + COL_MONTHLY_INCOME
					+ "=?," + COL_OCCUPATION + "=?," + COL_RELATION_WITH_PROSPECT + "=?" + " WHERE prospect_id=?";

			jdbcTemplate.update(sql, coApplicant.getProspectId(), coApplicant.getName(), coApplicant.getEmail(),
					coApplicant.getMobile(), coApplicant.getDob(), coApplicant.getAge(), coApplicant.getCommitments(),
					coApplicant.getMonthlyIncome(), coApplicant.getOccupation(), coApplicant.getRelationWithProspect(),
					coApplicant.getProspectId());
		} else {
			// insert
			final String sql = "INSERT INTO prospect_co_applicants (" + COL_PROSPECT_ID + "," + COL_NAME + ","
					+ COL_EMAIL + "," + COL_MOBILE + "," + COL_DOB + "," + COL_AGE + "," + COL_COMMITMENTS + "," + COL_MONTHLY_INCOME
					+ "," + COL_OCCUPATION + "," + COL_RELATION_WITH_PROSPECT + ")"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" });
					ps.setInt(1, coApplicant.getProspectId());
					ps.setString(2, coApplicant.getName());
					ps.setString(3, coApplicant.getEmail());
					ps.setString(4, coApplicant.getMobile());
					ps.setDate(5, new java.sql.Date(System.currentTimeMillis()));
					ps.setDouble(6, coApplicant.getAge());
					ps.setDouble(7, coApplicant.getCommitments());
					ps.setDouble(8, coApplicant.getMonthlyIncome());
					ps.setString(9, coApplicant.getOccupation());
					ps.setString(10, coApplicant.getRelationWithProspect());
					return ps;
				}
			}, keyHolder);

		}

		coApplicant.setCoApplicantId(((Long) keyHolder.getKey()).intValue());
		log.info("Inserting co-applicant...done");
		return (Long) keyHolder.getKey();
	}

	/**
	 * @param id
	 * @return
	 */
	public ProspectLoans findProspectById(int id) {

		StringBuffer query = new StringBuffer();
		query.append("SELECT * FROM prospect_loans WHERE prospect_id = " + id);

		Map<String, Object> cols = new HashMap<>();
		ProspectLoans prospect;

		cols = jdbcTemplate.queryForMap(query.toString());

		prospect = new ProspectLoans();
		prospect.setProspectId(id);
		prospect.setEmailAddress(cols.get(InvestHuntConstants.EMAIL).toString());
		prospect.setName(cols.get(InvestHuntConstants.NAME).toString());
		prospect.setAge(Double.parseDouble(cols.get(InvestHuntConstants.AGE).toString()));
		prospect.setMobile(cols.get(InvestHuntConstants.MOBILE).toString());
		prospect.setCategoryType(Integer.parseInt(cols.get(InvestHuntConstants.CATEGORY_TYPE).toString()));
		prospect.setMonthlyIncome(Double.parseDouble(cols.get(InvestHuntConstants.MONTHLY_INCOME).toString()));
		prospect.setCommitments(Double.parseDouble(cols.get(InvestHuntConstants.COMMITMENTS).toString()));
		prospect.setWorkLocation(cols.get(InvestHuntConstants.WORK_LOCATION).toString());
		/*prospect.setAssetLocation(cols.get(InvestHuntConstants.ASSET_LOCATION).toString());*/
		prospect.setCompany(cols.get(InvestHuntConstants.COMPANY).toString());
		prospect.setHasCoApplicant(cols.get(COL_HAS_CO_APPLICANT).toString().equals("Y") ? "on" : "off");
		
		// TODO remaining fields of prospects to follow.

		return prospect;
	}
	
	public void deleteProspect(int id){
		log.info(" deleting prospect start...");
		ProspectLoans prospect = findProspectById(id);
	
		if(prospect != null){
			Object[] params = {id};
			if(prospect.getHasCoApplicant() == "on"){
				int rows = jdbcTemplate.update(DELETE_QUERY_CO_APPLCANT, params);
				log.info(rows + " row(s) deleted from co-applicant.");
			}
	        int rows = jdbcTemplate.update(DELETE_QUERY_PROSPECT_LOANS, params);
	        log.info(rows + " row(s) deleted from prospect-loans.");
		}
	}
}
