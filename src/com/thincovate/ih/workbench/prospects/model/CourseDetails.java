/**
 * 
 */
package com.thincovate.ih.workbench.prospects.model;

/**
 * @author Sandy
 *
 */
public class CourseDetails {
	
	private String courseName;
	private String courseType;
	private String courseDuration;
	private String university;
	private String expectedSum;
	private String country;
	
	
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseType() {
		return courseType;
	}
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	public String getCourseDuration() {
		return courseDuration;
	}
	public void setCourseDuration(String courseDuration) {
		this.courseDuration = courseDuration;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getExpectedSum() {
		return expectedSum;
	}
	public void setExpectedSum(String expectedSum) {
		this.expectedSum = expectedSum;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

}
