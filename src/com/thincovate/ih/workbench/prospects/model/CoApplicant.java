/**
 * 
 */
package com.thincovate.ih.workbench.prospects.model;

import java.util.Date;

/**
 * @author Sandy
 *
 */
public class CoApplicant {

	private int coApplicantId;
	private int prospectId;
	private String name;
	private Date dob;
	private double age;
	private String mobile;
	private String email;
	private String occupation;
	private int employedSinceMonth;
	private int employedSinceYear;
	private double monthlyIncome;
	private double commitments;
	private String company;
	private String relationWithProspect;
	private String city;
	private String resStatus;
	

	/**
	 * @return the coApplicantId
	 */
	public int getCoApplicantId() {
		return coApplicantId;
	}
	/**
	 * @param coApplicantId the coApplicantId to set
	 */
	public void setCoApplicantId(int coApplicantId) {
		this.coApplicantId = coApplicantId;
	}
	/**
	 * @return the prospectId
	 */
	public int getProspectId() {
		return prospectId;
	}
	/**
	 * @param prospectId the prospectId to set
	 */
	public void setProspectId(int prospectId) {
		this.prospectId = prospectId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @return the employedSinceMonth
	 */
	public int getEmployedSinceMonth() {
		return employedSinceMonth;
	}
	/**
	 * @param employedSinceMonth the employedSinceMonth to set
	 */
	public void setEmployedSinceMonth(int employedSinceMonth) {
		this.employedSinceMonth = employedSinceMonth;
	}
	/**
	 * @return the employedSinceYear
	 */
	public int getEmployedSinceYear() {
		return employedSinceYear;
	}
	/**
	 * @param employedSinceYear the employedSinceYear to set
	 */
	public void setEmployedSinceYear(int employedSinceYear) {
		this.employedSinceYear = employedSinceYear;
	}
	/**
	 * @return the monthlyIncome
	 */
	public double getMonthlyIncome() {
		return monthlyIncome;
	}
	/**
	 * @param monthlyIncome the monthlyIncome to set
	 */
	public void setMonthlyIncome(double monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}
	/**
	 * @return the commitments
	 */
	public double getCommitments() {
		return commitments;
	}
	/**
	 * @param commitments the commitments to set
	 */
	public void setCommitments(double commitments) {
		this.commitments = commitments;
	}
	/**
	 * @return the relationWithProspect
	 */
	public String getRelationWithProspect() {
		return relationWithProspect;
	}
	/**
	 * @param relationWithProspect the relationWithProspect to set
	 */
	public void setRelationWithProspect(String relationWithProspect) {
		this.relationWithProspect = relationWithProspect;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the age
	 */
	public double getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(double age) {
		this.age = age;
	}
	
	public String getResStatus() {
		return resStatus;
	}
	public void setResStatus(String resStatus) {
		this.resStatus = resStatus;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}
