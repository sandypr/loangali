/**
 * 
 */
package com.thincovate.ih.workbench.prospects.model;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.thincovate.ih.common.utils.JsonDateDeserializer;
import com.thincovate.ih.common.utils.JsonDateSerializer;

/**
 * @author rajani
 *
 */
public class ProspectLoans {
	
	private int prospectId;
	private String name;
	private String emailAddress;
	private Date dateOfBirth;
	private double age;
	private String mobile;
	private String workLocation;
	private String employmentType;
	private String company;
	private String typeOfCompany;
	private String designation;
	private int yearOfEstablishment;
	private int yearsInCurrentLocation;
	private int employedSinceMonth;
	private int employedSinceYear;
	private double monthlyIncome;
	private double commitments;
	private int categoryType;
	private String purpose;
	private String assetLocation;
	private int expectedSum;
	private int propertyCost;
	private double eligibleSum;
	private Date dateCreated;
	private String resStatus;
	private String hasCoApplicant; 
	private double employedSinceMonths;
	

	private CoApplicant coApplicant;
	private CourseDetails course;
	

	/**
	 * @return the prospectId
	 */
	public int getProspectId() {
		return prospectId;
	}

	/**
	 * @param prospectId the prospectId to set
	 */
	public void setProspectId(int prospectId) {
		this.prospectId = prospectId;
	}

	public enum employmentType {
			SALARIED, SELFEMPLOYED, STUDENT
		};
	

	/**
	 * @return the monthlyIncome
	 */
	public double getMonthlyIncome() {
		return monthlyIncome;
	}

	/**
	 * @param monthlyIncome the monthlyIncome to set
	 */
	public void setMonthlyIncome(double monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the workLocation
	 */
	public String getWorkLocation() {
		return workLocation;
	}

	/**
	 * @param workLocation the workLocation to set
	 */
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

		
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the dateOfBirth
	 */
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	@JsonDeserialize(using=JsonDateDeserializer.class)
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the commitments
	 */
	public double getCommitments() {
		return commitments;
	}

	/**
	 * @param commitments the commitments to set
	 */
	public void setCommitments(double commitments) {
		this.commitments = commitments;
	}

	/**
	 * @return the categoryType
	 */
	public int getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(int categoryType) {
		this.categoryType = categoryType;
	}

	/**
	 * @return the expectedSum
	 */
	public int getExpectedSum() {
		return expectedSum;
	}

	/**
	 * @param expectedSum the expectedSum to set
	 */
	public void setExpectedSum(int expectedSum) {
		this.expectedSum = expectedSum;
	}

	/**
	 * @return the eligibleSum
	 */
	public double getEligibleSum() {
		return eligibleSum;
	}

	/**
	 * @param eligibleSum the eligibleSum to set
	 */
	public void setEligibleSum(double eligibleSum) {
		this.eligibleSum = eligibleSum;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the employmentType
	 */
	public String getEmploymentType() {
		return employmentType;
	}

	/**
	 * @param employmentType the employmentType to set
	 */
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	/**
	 * @return the employedSinceMonth
	 */
	public int getEmployedSinceMonth() {
		return employedSinceMonth;
	}

	/**
	 * @param employedSinceMonth the employedSinceMonth to set
	 */
	public void setEmployedSinceMonth(int employedSinceMonth) {
		this.employedSinceMonth = employedSinceMonth;
	}

	/**
	 * @return the employedSinceYear
	 */
	public int getEmployedSinceYear() {
		return employedSinceYear;
	}

	/**
	 * @param employedSinceYear the employedSinceYear to set
	 */
	public void setEmployedSinceYear(int employedSinceYear) {
		this.employedSinceYear = employedSinceYear;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 * @return the assetLocation
	 */
	public String getAssetLocation() {
		return assetLocation;
	}

	/**
	 * @param assetLocation the assetLocation to set
	 */
	public void setAssetLocation(String assetLocation) {
		this.assetLocation = assetLocation;
	}

	/**
	 * @return the typeOfCompany
	 */
	public String getTypeOfCompany() {
		return typeOfCompany;
	}

	/**
	 * @param typeOfCompany the typeOfCompany to set
	 */
	public void setTypeOfCompany(String typeOfCompany) {
		this.typeOfCompany = typeOfCompany;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the yearOfEstablishment
	 */
	public int getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	/**
	 * @param yearOfEstablishment the yearOfEstablishment to set
	 */
	public void setYearOfEstablishment(int yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	/**
	 * @return the yearsInCurrentLocation
	 */
	public int getYearsInCurrentLocation() {
		return yearsInCurrentLocation;
	}

	/**
	 * @param yearsInCurrentLocation the yearsInCurrentLocation to set
	 */
	public void setYearsInCurrentLocation(int yearsInCurrentLocation) {
		this.yearsInCurrentLocation = yearsInCurrentLocation;
	}

	/**
	 * @return the propertyCost
	 */
	public int getPropertyCost() {
		return propertyCost;
	}

	/**
	 * @param propertyCost the propertyCost to set
	 */
	public void setPropertyCost(int propertyCost) {
		this.propertyCost = propertyCost;
	}

	/**
	 * @return the coApplicant
	 */
	public CoApplicant getCoApplicant() {
		return coApplicant;
	}

	/**
	 * @param coApplicant the coApplicant to set
	 */
	public void setCoApplicant(CoApplicant coApplicant) {
		this.coApplicant = coApplicant;
	}

	/**
	 * @return the resStatus
	 */
	public String getResStatus() {
		return resStatus;
	}

	/**
	 * @param resStatus the resStatus to set
	 */
	public void setResStatus(String resStatus) {
		this.resStatus = resStatus;
	}

	/**
	 * @return the hasCoApplicant
	 */
	public String getHasCoApplicant() {
		return hasCoApplicant;
	}

	/**
	 * @param hasCoApplicant the hasCoApplicant to set
	 */
	public void setHasCoApplicant(String hasCoApplicant) {
		this.hasCoApplicant = hasCoApplicant;
	}

	/**
	 * @return the age
	 */
	public double getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(double age) {
		this.age = age;
	}
	
	/**
	 * @return number of months employed
	 */
	
	public double getEmployedSinceMonths() {
		return employedSinceMonths;
	}

	/**
	 * @return number of months employed to set
	 */
	public void setEmployedSinceMonths(double employedSinceMonths) {
		this.employedSinceMonths = employedSinceMonths;
	}

	public CourseDetails getCourse() {
		return course;
	}

	public void setCourse(CourseDetails course) {
		this.course = course;
	}
	
	
}
