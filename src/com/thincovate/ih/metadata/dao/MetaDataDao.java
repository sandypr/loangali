/**
 * 
 */
package com.thincovate.ih.metadata.dao;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.utils.InvestHuntConstants;
import com.thincovate.ih.models.EmiPerLakh;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;

/**
 * @author Sandy
 *
 */
@Service
public class MetaDataDao {

	private static final String FIND_ALL_PRODUCTS = "SELECT p.product_id, p.name,p.entity_id, p.category_type, p.rate_type_id, p.min_tenure, p.max_tenure, p.min_age, p.max_age, p.is_tied_up, p.status, e.name bank, e.logo FROM products p, entities e WHERE p.status = 'Active' AND p.category_type = ? AND p.entity_id = e.entity_id";
	private static final String FIND_ALL_RATES = "SELECT * FROM product_interest_rates WHERE status = 'Active' AND product_id in (select product_id from products where category_type = ?)";
	private static final String FIND_ALL_EMIS_PER_LAKH = "SELECT * FROM emiperlakh";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	static Logger log = Logger.getLogger(MetaDataDao.class);

	/**
	 * Pulls all products
	 * 
	 * @return
	 */
	public List<Product> findAllProducts(int category) {

		List<Product> products = new ArrayList<>();
		List<Map<String, Object>> rows;

		rows = jdbcTemplate.queryForList(FIND_ALL_PRODUCTS,new Object[]{category}, new int[] {Types.INTEGER});

		for (Map<String, Object> row : rows) {

			Product product = new Product();

			product.setCatId(row.get(InvestHuntConstants.CATEGORY_TYPE).toString());
			product.setEntityId(row.get(InvestHuntConstants.ENTITY_TYPE).toString());
			product.setEntityName(row.get(InvestHuntConstants.ENTITY_NAME).toString());
			product.setId(row.get(InvestHuntConstants.PRODUCT_ID).toString());
			product.setName(row.get(InvestHuntConstants.NAME).toString());
			product.setMaxTenure(Double.parseDouble(row.get(InvestHuntConstants.MAX_TENURE).toString()));
			product.setMinTenure(Double.parseDouble(row.get(InvestHuntConstants.MIN_TENURE).toString()));
			product.setMinAge(Integer.parseInt(row.get(InvestHuntConstants.MIN_AGE).toString()));
			product.setMaxAge(Integer.parseInt(row.get(InvestHuntConstants.MAX_AGE).toString()));
			product.setName(row.get(InvestHuntConstants.PRODUCT_NAME).toString());
			product.setRateType(Integer.parseInt(row.get(InvestHuntConstants.RATE_TYPE_ID).toString()));
			product.setStatus(row.get(InvestHuntConstants.STATUS).toString());
			product.setTiedUp(Boolean.parseBoolean(row.get(InvestHuntConstants.IS_TIED_UP).toString()));

			byte[] blobAsBytes = (byte[]) row.get(InvestHuntConstants.ENTITY_LOGO);
			product.setLogo(blobAsBytes);

			products.add(product);
		}

		return products;
	}

	/**
	 * @return
	 */
	public List<Rates> findAllRates(int category) {

		List<Rates> rates = new ArrayList<>();
		List<Map<String, Object>> rows;

		rows = jdbcTemplate.queryForList(FIND_ALL_RATES,new Object[]{category}, new int[] {Types.INTEGER});

		for (Map<String, Object> row : rows) {

			Rates rate = new Rates();

			rate.setInterest_id(Integer.parseInt(row.get(InvestHuntConstants.ROI_ID).toString()));
			rate.setProductId(row.get(InvestHuntConstants.PRODUCT_ID).toString());
			rate.setRangeLower(Double.parseDouble(row.get(InvestHuntConstants.MIN_AMOUNT).toString()));
			rate.setRangeUpper(Double.parseDouble(row.get(InvestHuntConstants.MAX_AMOUNT).toString()));
			rate.setRoi(Double.parseDouble(row.get(InvestHuntConstants.RATE_OF_INTEREST).toString()));

			rates.add(rate);
		}

		return rates;
	}

	/**
	 * I can get rid of this once i have a method in service layer
	 * 
	 * @return
	 */
	public void mergeProductsAndRates(List<Product> products, List<Rates> rates) {

		for (Rates rate : rates) {
			for (Product product : products) {
				if (product.getId().equalsIgnoreCase(rate.getProductId())) {
					product.getRates().add(rate);
					break;
				}
			}
		}

	}

	public void addEmiPerLakh(List<Rates> rates) {

		List<Map<String, Object>> rows;

		rows = jdbcTemplate.queryForList(FIND_ALL_EMIS_PER_LAKH);

		for (Map<String, Object> row : rows) {
			double roi = Double.parseDouble(row.get(InvestHuntConstants.ROI).toString());
			for (Rates rate : rates) {
				if (rate.getRoi() == roi) {
					for (int i = 1; i <= 25; i++) {
						EmiPerLakh emi = new EmiPerLakh();
						emi.setInterestId(rate.getInterest_id());
						emi.setTenure(i);
						emi.setEmiPerLakh(Math.round(Double.parseDouble(row.get(String.valueOf(i)).toString())));
						rate.getEmisPerLakh().add(emi);
					}
				}
			}
		}

	}

}
