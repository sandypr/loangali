/**
 * 
 */
package com.thincovate.ih.metadata.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thincovate.ih.metadata.services.MetadataServiceI;
import com.thincovate.ih.models.Product;

/**
 * @author Sandy
 *
 */
@Controller
@RequestMapping("/metadata")
public class MetadataController {
	
	@Autowired
	private MetadataServiceI mdService;
	
	static Logger log = Logger.getLogger(MetadataController.class);

	// temporary service which wouldnt be part of final version :(
	@RequestMapping(method = RequestMethod.GET, value = "/entities/{category}", produces = "application/json")
	public @ResponseBody List<Product> prodsForLoanEligibility(@PathVariable("category") int category, HttpServletRequest req) throws ParseException{
		
		log.info("Getting products for loan eligibility : Start");
		
		List<Product> products = null;
		products = mdService.findAllProductsForEligibility(category);
		
		log.info("Getting products for loan eligibility : Done");
		return products;
	}
}
