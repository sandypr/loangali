/**
 * 
 */
package com.thincovate.ih.metadata.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thincovate.ih.metadata.dao.MetaDataDao;
import com.thincovate.ih.metadata.services.MetadataServiceI;
import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;

/**
 * @author Sandy
 *
 */
@Service
public class MetadataServiceImpl implements MetadataServiceI {

	@Autowired
	MetaDataDao mdDao;

	@Override
	public List<Product> findAllProducts(int category) {
		List<Product> prods = null;

		prods = mdDao.findAllProducts(category);

		return prods;
	}

	@Override
	public List<Rates> findAllRates(int category) {
		List<Rates> rates = null;

		rates = mdDao.findAllRates(category);

		return rates;
	}

	/**
	 * @return
	 */
	@Override
	public List<Product> findAllProductsForEligibility(int category) {
		List<Product> prods = null;
		List<Rates> rates = null;

		prods = mdDao.findAllProducts(category);
		rates = mdDao.findAllRates(category);

		mdDao.addEmiPerLakh(rates);
		mdDao.mergeProductsAndRates(prods, rates);

		return prods;
	}

}
