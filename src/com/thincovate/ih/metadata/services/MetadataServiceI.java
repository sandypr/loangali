/**
 * 
 */
package com.thincovate.ih.metadata.services;

import java.util.List;

import com.thincovate.ih.models.Product;
import com.thincovate.ih.models.Rates;

/**
 * @author Sandy
 *
 */
public interface MetadataServiceI {
	
	List<Product> findAllProducts(int category);
	List<Rates> findAllRates(int category);
	List<Product> findAllProductsForEligibility(int category);

}
