package com.thincovate.ih.models;

public class Referrals {

	private String referredName;
	private String referredMobile;
	private String referredEmail;
	private String referredLocation;
	private String categoryType;
	private String referrer;
	private String referrerMobile;
	private String referrerEmail;
	private String referrerLocation;
	private String referrerAddress;
	
	public String getReferredName() {
		return referredName;
	}
	public void setReferredName(String referredName) {
		this.referredName = referredName;
	}
	public String getReferredMobile() {
		return referredMobile;
	}
	public void setReferredMobile(String referredMobile) {
		this.referredMobile = referredMobile;
	}
	public String getReferredEmail() {
		return referredEmail;
	}
	public void setReferredEmail(String referredEmail) {
		this.referredEmail = referredEmail;
	}
	public String getReferredLocation() {
		return referredLocation;
	}
	public void setReferredLocation(String referredLocation) {
		this.referredLocation = referredLocation;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getReferrer() {
		return referrer;
	}
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
	public String getReferrerMobile() {
		return referrerMobile;
	}
	public void setReferrerMobile(String referrerMobile) {
		this.referrerMobile = referrerMobile;
	}
	public String getReferrerEmail() {
		return referrerEmail;
	}
	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}
	public String getReferrerLocation() {
		return referrerLocation;
	}
	public void setReferrerLocation(String referrerLocation) {
		this.referrerLocation = referrerLocation;
	}
	public String getReferrerAddress() {
		return referrerAddress;
	}
	public void setReferrerAddress(String referrerAddress) {
		this.referrerAddress = referrerAddress;
	}
	

	

}
