/**
 * 
 */
package com.thincovate.ih.models;

/**
 * @author Sandy
 *
 */
public class EmiPerLakh {
	
	private int interestId;
	private int tenure;
	private double emiPerLakh;
	/**
	 * 
	 */
	public EmiPerLakh() {
	}
	/**
	 * @return the interestId
	 */
	public int getInterestId() {
		return interestId;
	}
	/**
	 * @param interestId the interestId to set
	 */
	public void setInterestId(int interestId) {
		this.interestId = interestId;
	}
	/**
	 * @return the tenure
	 */
	public int getTenure() {
		return tenure;
	}
	/**
	 * @param tenure the tenure to set
	 */
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	/**
	 * @return the emiPerLakh
	 */
	public double getEmiPerLakh() {
		return emiPerLakh;
	}
	/**
	 * @param emiPerLakh the emiPerLakh to set
	 */
	public void setEmiPerLakh(double emiPerLakh) {
		this.emiPerLakh = emiPerLakh;
	}

}
