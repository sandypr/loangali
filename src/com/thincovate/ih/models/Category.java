/**
 * 
 */
package com.thincovate.ih.models;

/**
 * @author Sandy
 *
 */
public class Category {

	private int categoryId;
	private String categoryName;
	private String categoryDesc;
	private int parentCategory;
	
	/**
	 * @return the categoryType
	 */
	public int getCategoryId() {
		return categoryId;
	}
	
	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	/**
	 * @return the categoryDesc
	 */
	public String getCategoryDesc() {
		return categoryDesc;
	}
	
	/**
	 * @param categoryDesc the categoryDesc to set
	 */
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	
	/**
	 * @return the parentCategory
	 */
	public int getParentCategory() {
		return parentCategory;
	}
	
	/**
	 * @param parentCategory the parentCategory to set
	 */
	public void setParentCategory(int parentCategory) {
		this.parentCategory = parentCategory;
	}
}
