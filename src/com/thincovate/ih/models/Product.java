/**
 * 
 */
package com.thincovate.ih.models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sandy
 *
 */
public class Product {

	private String name;
	private String id;
	private String catId;
	private String entityId;
	private byte[] logo;
	private String entityName;
	private int rateType;
	private double minTenure;
	private double maxTenure;
	private int minAge;
	private int maxAge;
	private String status;
	private boolean isTiedUp;

	private List<Rates> rates = new ArrayList<>();
	
	/**
	 * Constructor
	 */
	public Product() {}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the catId
	 */
	public String getCatId() {
		return catId;
	}
	/**
	 * @param catId the catId to set
	 */
	public void setCatId(String catId) {
		this.catId = catId;
	}
	/**
	 * @return the entityId
	 */
	public String getEntityId() {
		return entityId;
	}
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	/**
	 * @return the rateType
	 */
	public int getRateType() {
		return rateType;
	}
	/**
	 * @param rateType the rateType to set
	 */
	public void setRateType(int rateType) {
		this.rateType = rateType;
	}
	/**
	 * @return the minTenure
	 */
	public double getMinTenure() {
		return minTenure;
	}
	/**
	 * @param minTenure the minTenure to set
	 */
	public void setMinTenure(double minTenure) {
		this.minTenure = minTenure;
	}
	/**
	 * @return the maxTenure
	 */
	public double getMaxTenure() {
		return maxTenure;
	}
	/**
	 * @param d the maxTenure to set
	 */
	public void setMaxTenure(double d) {
		this.maxTenure = d;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the isTiedUp
	 */
	public boolean isTiedUp() {
		return isTiedUp;
	}
	/**
	 * @param isTiedUp the isTiedUp to set
	 */
	public void setTiedUp(boolean isTiedUp) {
		this.isTiedUp = isTiedUp;
	}
	/**
	 * @return the rates
	 */
	public List<Rates> getRates() {
		return rates;
	}
	/**
	 * @param rates the rates to set
	 */
	public void setRates(List<Rates> rates) {
		this.rates = rates;
	}

	/**
	 * @return the entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName the entityName to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the logo
	 */
	public byte[] getLogo() {
		return logo;
	}

	/**
	 * @param logo the logo to set
	 */
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	/**
	 * @return the minAge
	 */
	public int getMinAge() {
		return minAge;
	}

	/**
	 * @param minAge the minAge to set
	 */
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	/**
	 * @return the maxAge
	 */
	public int getMaxAge() {
		return maxAge;
	}

	/**
	 * @param maxAge the maxAge to set
	 */
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	
}
