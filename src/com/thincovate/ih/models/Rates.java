/**
 * 
 */
package com.thincovate.ih.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sandy
 *
 */
public class Rates {

	private int interest_id;
	private String productId;
	private double rangeLower;
	private double rangeUpper;
	private double roi;;
	private Date endDate;
	private Date startDate;
	private String status;
	

	private List<EmiPerLakh> emisPerLakh = new ArrayList<>();
	
	private Map<Integer, Double> tenureWiseEmiPerLakh = new HashMap<>();
	
	/**
	 * Constructor
	 */
	public Rates() {}
	
	/**
	 * @return the interest_id
	 */
	public int getInterest_id() {
		return interest_id;
	}

	/**
	 * @param interest_id the interest_id to set
	 */
	public void setInterest_id(int interest_id) {
		this.interest_id = interest_id;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	/**
	 * @return the range_lower
	 */
	public double getRangeLower() {
		return rangeLower;
	}
	
	/**
	 * @param range_lower the range_lower to set
	 */
	public void setRangeLower(double rangeLower) {
		this.rangeLower = rangeLower;
	}
	
	/**
	 * @return the range_upper
	 */
	public double getRangeUpper() {
		return rangeUpper;
	}
	
	/**
	 * @param range_upper the range_upper to set
	 */
	public void setRangeUpper(double rangeUpper) {
		this.rangeUpper = rangeUpper;
	}
	
	/**
	 * @return the roi
	 */
	public double getRoi() {
		return roi;
	}
	
	/**
	 * @param roi the roi to set
	 */
	public void setRoi(double roi) {
		this.roi = roi;
	}
	
	/**
	 * @return the tenureWiseEmiPerLakh
	 */
	public Map<Integer, Double> getTenureWiseEmiPerLakh() {
		return tenureWiseEmiPerLakh;
	}
	
	/**
	 * @param tenureWiseEmiPerLakh the tenureWiseEmiPerLakh to set
	 */
	public void setTenureWiseEmiPerLakh(Map<Integer, Double> tenureWiseEmiPerLakh) {
		this.tenureWiseEmiPerLakh = tenureWiseEmiPerLakh;
	}

	/**
	 * @return the emisPerLakh
	 */
	public List<EmiPerLakh> getEmisPerLakh() {
		return emisPerLakh;
	}

	/**
	 * @param emisPerLakh the emisPerLakh to set
	 */
	public void setEmisPerLakh(List<EmiPerLakh> emisPerLakh) {
		this.emisPerLakh = emisPerLakh;
	}
	
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
