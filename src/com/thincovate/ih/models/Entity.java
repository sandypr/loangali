/**
 * 
 */
package com.thincovate.ih.models;

import java.sql.Blob;

/**
 * @author Sandy
 *
 */
public class Entity {

	private int entityId;
	private String name;
	private int entityTypeId;
	private Blob logo;
	private String status;
	private String isPartner;
	private int modifiedBy;
	
	/**
	 * @return the entityId
	 */
	public int getEntityId() {
		return entityId;
	}
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the entityTypeId
	 */
	public int getEntityTypeId() {
		return entityTypeId;
	}
	/**
	 * @param entityTypeId the entityTypeId to set
	 */
	public void setEntityTypeId(int entityTypeId) {
		this.entityTypeId = entityTypeId;
	}
	/**
	 * @return the logo
	 */
	public Blob getLogo() {
		return logo;
	}
	/**
	 * @param logo the logo to set
	 */
	public void setLogo(Blob logo) {
		this.logo = logo;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the isPartner
	 */
	public String getIsPartner() {
		return isPartner;
	}
	/**
	 * @param isPartner the isPartner to set
	 */
	public void setIsPartner(String isPartner) {
		this.isPartner = isPartner;
	}
	/**
	 * @return the modifiedBy
	 */
	public int getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
