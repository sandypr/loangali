/**
 * 
 */
package com.thincovate.ih.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * @author Sandy
 *
 */
public class CommonUtils {

	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

	public static String currentDateAsString() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
	
	public static String currentDateLocalFormat() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(cal.getTime());
	}
	
	public static String currentDateInCharacters() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MMMddyy");
		return sdf.format(cal.getTime());
	}

	public static String validIncome(double salary, double commitments) {

		String errorMsg = null;
		
		if (salary < commitments) {
			errorMsg = "Sorry, your commitments are too high to take a new loan now!";
		} else if (salary < 1000) {
			errorMsg = "Sorry, you are not eligible for a loan with this salary! Please visit later";
		} else if (salary > 999999999999L) {
			errorMsg = "Wohooo! That's quite a salary. Are you sure you entered the values right?";
		}

		return errorMsg;
	}

	public static String validTenure(int type, double salary, double commitments, int tenure) {
		String valid = validIncome(salary, commitments);
		String errorMsg = null;

		if (valid == null) {
			if (type == 2 && (tenure < 1 &&tenure > 30)) {
				return errorMsg = "Sorry, home loans  are limited to 30 years! Please enter a tenure between 1-30";
			} else if ((type == 3 || type == 4) && (tenure < 1 &&tenure > 5)) {
				return errorMsg = "Sorry, Personal/Lap loans  are limited to 5 years! Please enter a tenure between 1-5";
			}
		}

		return errorMsg;
	}

	public static String validLoanAmountROINTenure(double roi, int tenure, double loanAmt, int cat) {
		String errorMsg = null;

		if (roi == 0) {
			errorMsg = "please enter a valid value for ROI";
		}

		if ((cat == 3 || cat == 4) && (tenure < 1 &&tenure > 5)) {
			errorMsg = "Sorry, home loans  are limited to 30 years! Please enter a tenure between 1-30";
		}

		if (roi > 50) {
			errorMsg = "Sorry, no banks charge > 50%. Please enter a value between 1-50";

		}

		if ((cat == 2) && (loanAmt < 1000 && loanAmt > 10000000)) {
			errorMsg = "Sorry, loan amount < 100 is too less for further analysis. Please enter a value >=100! !!";

		} else if (((cat == 3 || cat == 4) && (loanAmt < 1000 && loanAmt > 3000000))) {
			errorMsg = "Sorry, loan amount < 100 is too less for further analysis. Please enter a value >=100!";

		}
		return errorMsg;
	}
	
	/**
	 * Prepares content strings for email notifications by replacing placeholders with values
	 * @param content
	 * @param map
	 * @return
	 */
	public static String prepareEmailString(String content, Map<String,String> map){
		
		StringBuilder string = new StringBuilder(content);
		for(Map.Entry<String,String> entry : map.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			//string = string.replace(string.indexOf(key), string.indexOf(key)+key.length(), value);
			replaceAll(string, key,value);
		}
		
		return string.toString();
	}
	
	private static void replaceAll(StringBuilder builder, String key, String value)
	{
	    int index = builder.indexOf(key);
	    while (index != -1)
	    {
	        builder.replace(index, index + key.length(), value);
	        index += value.length();
	        index = builder.indexOf(key, index);
	    }
	}
}
