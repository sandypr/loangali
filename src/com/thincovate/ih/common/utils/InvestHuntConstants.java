/**
 * 
 */
package com.thincovate.ih.common.utils;

/**
 * @author Sandy
 *
 */
public interface InvestHuntConstants {
	
	// Loan types
	static String PERSONAL_LOAN_TYPE = "PERSONAL_LOAN";
	static String HOME_LOAN_TYPE = "HOME_LOAN";
	static String LAP_LOAN_TYPE = "LAP_LOAN";
	
	// stored proc params
	static String EMI_ELIGIBILITY_OUTPARAM = "emi_eligibility";
	static String MIN_RATE_OF_INTEREST_OUTPARAM = "min_rate_of_interest";
	static String PRODUCT_ID_OUTPARAM = "product";
	
	// database keys
	static String CATEGORY_TYPE = "category_type";
	static String ENTITY_TYPE = "entity_id";
	static String PRODUCT_ID = "product_id";
	static String MAX_TENURE = "max_tenure";
	static String MIN_TENURE = "min_tenure";
	static String MIN_AGE = "min_age";
	static String MAX_AGE = "max_age";
	static String PRODUCT_NAME = "name";
	static String RATE_TYPE_ID = "rate_type_id";
	static String STATUS = "status";
	static String IS_TIED_UP = "is_tied_up";
	static String MAX_AMOUNT = "max_amount";
	static String MIN_AMOUNT = "min_amount";
	static String RATE_OF_INTEREST = "rate_of_interest";
	static String ROI = "roi";
	static String ROI_ID = "interest_id";
	static String TENURE = "tenure";
	static String EMI_PER_LAKH = "emi_per_lakh";
	static String ENTITY_NAME = "bank";
	static String ENTITY_LOGO = "logo";
	static String NAME = "name";
	static String EMAIL = "email_address";
	static String MOBILE = "mobile";
	static String AGE = "age";
	static String EMPLOYMENT_TYPE = "employment_type";
	static String COMPANY = "company";
	static String WORK_LOCATION = "work_location";
	static String EMPLOYED_SINCE = "employed_since";
	static String MONTHLY_INCOME = "monthly_income";
	static String COMMITMENTS = "commitments";
	static String ASSET_LOCATION = "asset_location";
	static String HAS_COAPPLICANT = "has_co_applicant";
	static String PURPOSE = "purpose";
	static String DESIGNATION = "designation";
	static String TYPE_OF_COMPANY = "type_of_company";
	static String ESTABLISHMENT_YEAR = "year_of_establishment";
	static String YEARS_IN_CURR_LOC = "years_in_current_location";
	
	
	// status
	
}
