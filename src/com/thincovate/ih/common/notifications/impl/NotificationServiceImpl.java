/**
 * 
 */
package com.thincovate.ih.common.notifications.impl;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.stereotype.Service;

import com.thincovate.ih.common.exceptions.ServiceLayerException;
import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.notifications.Notify;

/**
 * @author Sandy
 *
 */
@Service
public class NotificationServiceImpl implements NotificationServiceI{
	
	@Override
	public void notifyConfirmationByEmail(String emailId, String emailContent, String subject){
		try{
			Notify notify = Notify.getInstance();
			notify.createNotificationMessage(emailId, emailContent, subject);
			notify.pushEmailNotification();
		}catch(IOException | MessagingException ioe){
			throw new ServiceLayerException(ioe.getMessage());
		}
	}
	
	@Override
	public void sendEligiEmailNotification(String emailId, String coverContent, String emailContent, String subject) {
		try{
			Notify notify = Notify.getInstance();
			notify.createNotificationMessageWithPDF(emailId, coverContent, emailContent, subject);
			notify.pushEmailNotification();
		}catch(Exception ioe){
			ioe.printStackTrace();
			throw new ServiceLayerException(ioe.getMessage());
		}
	}

}
