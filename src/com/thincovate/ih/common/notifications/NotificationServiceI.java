/**
 * 
 */
package com.thincovate.ih.common.notifications;


/**
 * @author Sandy
 *
 */
public interface NotificationServiceI {

	void notifyConfirmationByEmail(String emailId, String emailContent, String subject);
	void sendEligiEmailNotification(String emailId, String coverContent, String emailContent, String subject);

}
