/**
 * 
 */
package com.thincovate.ih.common.notifications;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.SAXException;

import com.lowagie.text.DocumentException;

/**
 * @author Sandy
 *
 */
public class Notify {

	static Logger log = Logger.getLogger(Notify.class);
	
	private static Notify instance;
	private Properties emailProperties;
	private Session mailSession;
	private MimeMessage emailMessage;
	
	/**
	 * Private constructor
	 * @throws IOException 
	 */
	private Notify() throws IOException{
		init();
	}
	
	/**
	 * Singleton instance
	 * @return
	 * @throws IOException 
	 */
	public static Notify getInstance() throws IOException{
		
		if (instance == null)
			instance = new Notify();
		
		return instance;
	}
	
	/**
	 * Initialize server properties
	 * @throws IOException 
	 */
	private void init() throws IOException {

		emailProperties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("investhunt.properties");
			emailProperties.load(input);
		} catch (IOException e) {
			//e.printStackTrace();
			input = Notify.class.getClassLoader().getResourceAsStream("investhunt.properties");
			//log.info(input);
			try {
				emailProperties.load(input);
			} catch (IOException ioe) {
				log.error("Error loading properties file");
				ioe.printStackTrace();
				throw ioe;
			}
			System.out.println("test");
		}/*finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
					throw ioe;
				}
			}
		}*/
		
	}
	
	/**
	 * Creating email message
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public void createNotificationMessage(String emailId, String content, String subject)
			throws AddressException, MessagingException {
		
		mailSession = Session.getDefaultInstance(emailProperties, null);
		emailMessage = new MimeMessage(mailSession);
		
		String[] toList = (emailId.toString()).split(",");
		
		for (int i = 0; i < toList.length; i++) {
			log.info(toList[i]);
			emailMessage.addRecipient(Message.RecipientType.TO,
					new InternetAddress(toList[i]));
		}
		
		/*String[] bccList = (emailProperties.getProperty("mail.email.support.emaillist.bcc").toString()).split(",");
		
		for (int i = 0; i < toList.length; i++) {
			emailMessage.addRecipient(Message.RecipientType.BCC,
					new InternetAddress(toList[i]));
		}*/

		Address[] from = InternetAddress.parse(emailProperties.getProperty("mail.email.support.autogenerate.from"));
		emailMessage.addFrom(from);
	    
		emailMessage.setSubject(subject);
		emailMessage.setContent(content, "text/html"); // for a html email
		
		// emailMessage.setText(emailBody); // for a text email

	}
	
	public void createNotificationMessageWithPDF(String emailId, String coverContent, String content, String subject)
			throws AddressException, MessagingException, ParserConfigurationException, IOException, SAXException{
		
		mailSession = Session.getDefaultInstance(emailProperties, null);
		
	    // parse the markup into an xml Document
	    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    byte[] bytes              = content.getBytes("iso-8859-1");
	    Document doc = builder.parse(new ByteArrayInputStream(bytes));

	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocument(doc, null);

	    ByteArrayOutputStream os = new ByteArrayOutputStream();
	    renderer.layout();
	    try {
			renderer.createPDF(os);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    byte[] outputBytes = os.toByteArray();
	    os.close();
	    
	    //email
        emailMessage = new MimeMessage(mailSession);
        emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(emailId));
        Multipart multipart = new MimeMultipart();
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        
        //Cover letter
        messageBodyPart.setContent(coverContent, "text/html");
        multipart.addBodyPart(messageBodyPart);

        messageBodyPart = new MimeBodyPart();
        //construct the pdf body part
        DataSource dataSource = new ByteArrayDataSource(outputBytes, "application/pdf");
        messageBodyPart.setHeader("Content-Transfer-Encoding", "base64");
        messageBodyPart.setDataHandler(new DataHandler(dataSource));
        messageBodyPart.setFileName(emailProperties.getProperty("mail.email.confirmation_fileName"));

        multipart.addBodyPart(messageBodyPart);
        
        Address[] from = InternetAddress.parse(emailProperties.getProperty("mail.email.support.autogenerate.from"));
        //construct message
        emailMessage.addFrom(from);
       
        emailMessage.setSubject(subject);
        
        emailMessage.setContent(multipart, "multipart/mixed");
	}

	/*
	 * Push email
	 */
	public void pushEmailNotification() throws AddressException, MessagingException {

		Transport transport = mailSession.getTransport("smtp");
		System.out.println(emailProperties.getProperty("mail.from.user")+"---"+emailProperties.getProperty("mail.from.user.pwd")+"---"+emailProperties.getProperty("mail.email.host")+"---"+emailProperties.getProperty("mail.smtp.port")+"---"+emailProperties.getProperty("mail.smtp.starttls.enable"));
		transport.connect(emailProperties.getProperty("mail.email.host"), Integer.parseInt(emailProperties.getProperty("mail.smtp.port")),emailProperties.getProperty("mail.from.user"), emailProperties.getProperty("mail.from.user.pwd"));
		transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
		transport.close();
		System.out.println("Email sent successfully.");
	}
	
	/**
	 * This is to retrieve the email properties
	 * @param key
	 * @return
	 */
	public String getProperty(String key){
		String val = emailProperties.getProperty(key);
		if(val != null)
			return val;
		else
			return "";
	}
	
	
	private static String prepareEmailContent(){
		
		String NAME_STRING = "###NAME_STRING###";
		String DATE_STRING = "###DATE_STRING###";
		String MOBILE_STRING = "###MOBILE_STRING###";
		String EMAIL_STRING = "###EMAIL_STRING###";
		String CITY_STRING = "###CITY_STRING###";
		String CATEGORY_STRING = "###CATEGORY_STRING###";
		String PRODUCTNAME_STRING = "###PRODUCTNAME_STRING###";
		String BANK_STRING = "###BANK_STRING###";
		String ELIGIBILITY_STRING = "###ELIGIBILITY_STRING###";
		String EMI_STRING = "###EMI_STRING###";
		String ROI_STRING = "###ROI_STRING###";
		String TENURE_STRING = "###TENURE_STRING###";
		String PROSPECT_NUMBER = "###PROSPECT_NUMBER###";
		
		StringBuilder string = null;
		try {
			string = new StringBuilder(Notify.getInstance().getProperty("mail.email.confirmation_content"));
			string = string.replace(string.indexOf(NAME_STRING), string.indexOf(NAME_STRING)+NAME_STRING.length(), "Rajani");
			string = string.replace(string.indexOf(DATE_STRING), string.indexOf(DATE_STRING)+DATE_STRING.length(), String.valueOf(new java.sql.Date(System.currentTimeMillis())));
			string = string.replace(string.indexOf(MOBILE_STRING), string.indexOf(MOBILE_STRING)+MOBILE_STRING.length(), "9898989899");
			string = string.replace(string.indexOf(EMAIL_STRING), string.indexOf(EMAIL_STRING)+EMAIL_STRING.length(), "test@test.com");
			string = string.replace(string.indexOf(CITY_STRING), string.indexOf(CITY_STRING)+CITY_STRING.length(), "Hyderabad");
			string = string.replace(string.indexOf(PROSPECT_NUMBER), string.indexOf(PROSPECT_NUMBER)+PROSPECT_NUMBER.length(), String.valueOf(2));
			string = string.replace(string.indexOf(CATEGORY_STRING), string.indexOf(CATEGORY_STRING)+CATEGORY_STRING.length(), String.valueOf(2));
			string = string.replace(string.indexOf(PRODUCTNAME_STRING), string.indexOf(PRODUCTNAME_STRING)+PRODUCTNAME_STRING.length(), "2");
			string = string.replace(string.indexOf(BANK_STRING), string.indexOf(BANK_STRING)+BANK_STRING.length(), "HDFC");
			string = string.replace(string.indexOf(ELIGIBILITY_STRING), string.indexOf(ELIGIBILITY_STRING)+ELIGIBILITY_STRING.length(), String.valueOf(2300000));
			string = string.replace(string.indexOf(EMI_STRING), string.indexOf(EMI_STRING)+EMI_STRING.length(), String.valueOf(20000));
			string = string.replace(string.indexOf(ROI_STRING), string.indexOf(ROI_STRING)+ROI_STRING.length(), String.valueOf(10));
			string = string.replace(string.indexOf(TENURE_STRING), string.indexOf(TENURE_STRING)+TENURE_STRING.length(), String.valueOf(10));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (string == null)? "":string.toString();
	}
	
	/**
	 * test
	 * @param args
	 */
	public static void main(String[] args){
		Notify notify;
		StringBuffer buf = new StringBuffer();
	    buf.append("&lt;html&gt;");
	    
	    buf.append("&lt;/html&gt;");
		try {
			notify = new Notify();
			//notify.createNotificationMessage("kisan.pr@gmail.com", "test", "test") ;
			notify.createNotificationMessageWithPDF("arjula.rajani@gmail.com", "", prepareEmailContent(), "test") ;
			notify.pushEmailNotification();
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
