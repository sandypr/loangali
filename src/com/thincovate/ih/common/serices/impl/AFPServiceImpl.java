package com.thincovate.ih.common.serices.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thincovate.ih.common.dao.AFPDao;
import com.thincovate.ih.common.services.AFPServiceI;
import com.thincovate.ih.models.AFP;

@Service
public class AFPServiceImpl implements AFPServiceI {

	@Autowired
	private AFPDao afpDao;
	static Logger log = Logger.getLogger(AFPServiceImpl.class);

	@Override
	public void addAFP(AFP afp) {
		afpDao.addAFP(afp);
	}
}
