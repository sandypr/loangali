package com.thincovate.ih.common.serices.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.dao.SubScriptionDao;
import com.thincovate.ih.common.services.SubscriptionServiceI;
import com.thincovate.ih.models.Subscription;

@Service
public class SubscriptionServiceImpl implements SubscriptionServiceI {

	@Autowired
	private SubScriptionDao subscriptionDao;
	static Logger log = Logger.getLogger(SubscriptionServiceImpl.class);

	@Override
	public long subscribe(Subscription subscription) {
		long id = subscriptionDao.addSubscription(subscription);
		return id;
	}

}
