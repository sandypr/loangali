package com.thincovate.ih.common.serices.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.dao.RefferalsDao;
import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.notifications.Notify;
import com.thincovate.ih.common.services.ReferralsServiceI;
import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.models.Referrals;

@Service
public class ReferralsServiceImpl implements ReferralsServiceI {

	@Autowired
	private RefferalsDao referralsDao;
	@Autowired
	private NotificationServiceI notifyService;
	
	static Logger log = Logger.getLogger(ReferralsServiceImpl.class);

	@Override
	public void addReferrals(Referrals referrals) {

		try{
			//notifyService.notifyConfirmationByEmail("info@thincovate.com", content, "referrer");
			
			// notify referrer
			if(referrals.getReferrerEmail() != null){
				String content = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.referral.tocustomer"),prepareReferrerStringsMap(referrals));
				String subject = Notify.getInstance().getProperty("mail.email.referral.tocustomer.subject");
				notifyService.notifyConfirmationByEmail(referrals.getReferrerEmail(), content, subject);
			}
			
			// notify support
			String supportSubject = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.referral.tosupport.subject"),prepareSupportSubjStringsMap(referrals));
			String supportContent = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.referral.tosupport"),prepareSupportStringsMap(referrals));
			notifyService.notifyConfirmationByEmail(Notify.getInstance().getProperty("mail.email.support.emaillist"), supportContent, supportSubject);
			
		}catch(Exception ex){
			log.error("Exception caught : "+ ex);
			//TODO: throw error page currently truncating it until email issue is fixed.
		}
		
		// add referrals to db
		referralsDao.addReferrals(referrals);
	}
	
	private Map<String,String> prepareReferrerStringsMap(Referrals referrals){
		
		Map<String,String> map = new HashMap<>();
		map.put("###REFERRER_NAME###", (referrals.getReferrer()==null)? "":referrals.getReferrer());
		map.put("###REFERRED_NAME###", (referrals.getReferredName()==null)? "":referrals.getReferredName());
		
		return map;
	}
	
	private Map<String,String> prepareSupportSubjStringsMap(Referrals referrals){
		
		Map<String,String> map = new HashMap<>();
		map.put("###REFERRED_MOBILE###", (referrals.getReferredMobile()==null)? "":referrals.getReferredMobile());
		map.put("###REFERRED_NAME###", (referrals.getReferredName()==null)? "":referrals.getReferredName());
		
		return map;
	}
	private Map<String,String> prepareSupportStringsMap(Referrals referrals){
		
		Map<String,String> map = new HashMap<>();
		map.put("###REFERRER_NAME###", (referrals.getReferrer()==null)? "":referrals.getReferrer());
		map.put("###REFERRER_MOBILE###", (referrals.getReferrerMobile()==null)? "":referrals.getReferrerMobile());
		map.put("###REFERRER_EMAIL###", (referrals.getReferrerEmail()==null)? "":referrals.getReferrerEmail());
		map.put("###REFERRER_LOCATION###", (referrals.getReferrerLocation()==null)? "":referrals.getReferrerLocation());
		map.put("###REFERRED_NAME###", (referrals.getReferredName()==null)? "":referrals.getReferredName());
		map.put("###REFERRED_MOBILE###", (referrals.getReferredMobile()==null)? "":referrals.getReferredMobile());
		map.put("###REFERRED_EMAIL###", (referrals.getReferredEmail()==null)? "":referrals.getReferredEmail());
		map.put("###REFERRED_LOCATION###", (referrals.getReferredLocation()==null)? "":referrals.getReferredLocation());
		
		return map;
	}

}
