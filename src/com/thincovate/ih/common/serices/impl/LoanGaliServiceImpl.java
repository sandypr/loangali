/**
 * 
 */
package com.thincovate.ih.common.serices.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.dao.AFPDao;
import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.notifications.Notify;
import com.thincovate.ih.common.services.LoanGaliServiceI;
import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.models.AFP;
import com.thincovate.ih.models.Feedback;

/**
 * @author Sandy
 *
 */
@Service
public class LoanGaliServiceImpl implements LoanGaliServiceI {
	
	@Autowired
	private NotificationServiceI notifyService;
	@Autowired
	private AFPDao afpDao;
	static Logger log = Logger.getLogger(LoanGaliServiceImpl.class);
	public LoanGaliServiceImpl() {}
	
	@Override
	public void registerFeedback(Feedback feedback) {
		try{
			// notify support
			String supportSubject = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.feedback.tosupport.subject"),prepareFeedbackSubjStringsMap(feedback));
			String supportContent = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.feedback.tosupport"),prepareFeedbackStringsMap(feedback));
			notifyService.notifyConfirmationByEmail(Notify.getInstance().getProperty("mail.email.support.emaillist"), supportContent, supportSubject);
			
		}catch(Exception ex){
			log.error("Exception caught : "+ ex);
			//TODO: throw error page currently truncating it until email issue is fixed.
		}
		
		//TODO introduce a dao and push the feedback into database when enhancing
	}
	
	@Override
	public void registerAFPRequest(AFP afpReq) {
		try{
			if(afpReq.getEmail() != null){
				// notify customer
				String subject = Notify.getInstance().getProperty("mail.email.finplanning.tocustomer.subject");
				String content = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.finplanning.tocustomer"),prepareAFPStringsMap(afpReq));
				notifyService.notifyConfirmationByEmail(afpReq.getEmail(), content, subject);
			}
			
			// notify support
			String supportSubject = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.finplanning.tosupport.subject"),prepareAFPSupportSubjStringsMap(afpReq));
			String supportContent = CommonUtils.prepareEmailString(Notify.getInstance().getProperty("mail.email.finplanning.tosupport"),prepareAFPSupportStringsMap(afpReq));
			notifyService.notifyConfirmationByEmail(Notify.getInstance().getProperty("mail.email.support.emaillist"), supportContent, supportSubject);
			
		}catch(Exception ex){
			log.error("Exception caught : "+ ex);
			//TODO: throw error page currently truncating it until email issue is fixed.
		}
		
		afpDao.addAFP(afpReq);
	}
	
	/***
	 * private methods
	 */
	
	/**
	 * 
	 * @param feedback
	 * @return
	 */
	private Map<String,String> prepareFeedbackStringsMap(Feedback feedback){
		
		Map<String,String> map = new HashMap<>();
		map.put("###COMMENTS_STRING###", (feedback.getComments()==null)? "":feedback.getComments());
		
		return map;
	}
	
	/**
	 * 
	 * @param feedback
	 * @return
	 */
	private Map<String,String> prepareFeedbackSubjStringsMap(Feedback feedback){
		
		Map<String,String> map = new HashMap<>();
		map.put("###NAME_STRING###", (feedback.getName()==null)? "":feedback.getName());
		
		return map;
	}
	
	/**
	 * 
	 * @param afp
	 * @return
	 */
    private Map<String,String> prepareAFPStringsMap(AFP afp){
		
		Map<String,String> map = new HashMap<>();
		map.put("###AFP_NAME###", (afp.getName()==null)? "":afp.getName());
		
		return map;
	}
	
    /**
     * 
     * @param afp
     * @return
     */
	private Map<String,String> prepareAFPSupportSubjStringsMap(AFP afp){
		
		Map<String,String> map = new HashMap<>();
		map.put("###AFP_NAME###", (afp.getName()==null)? "":afp.getName());
		map.put("###AFP_MOBILE###", (afp.getMobile()==null)? "":afp.getMobile());
		
		return map;
	}
	
	/**
	 * 
	 * @param afp
	 * @return
	 */
	private Map<String,String> prepareAFPSupportStringsMap(AFP afp){
		
		Map<String,String> map = new HashMap<>();
		map.put("###AFP_NAME###", (afp.getName()==null)? "":afp.getName());
		map.put("###AFP_MOBILE###", (afp.getMobile()==null)? "":afp.getMobile());
		map.put("###AFP_EMAIL###", (afp.getEmail()==null)? "":afp.getEmail());
		map.put("###AFP_QUESTION###", (afp.getQns()==null)? "":afp.getQns());
		
		return map;
	}

}
