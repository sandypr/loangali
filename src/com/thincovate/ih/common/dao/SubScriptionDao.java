package com.thincovate.ih.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import com.thincovate.ih.models.Subscription;

@Service
public class SubScriptionDao {
	
	static Logger log = Logger.getLogger(SubScriptionDao.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String COL_NAME = "name";
	private static String COL_MOBILE = "mobile";
	private static String COL_EMAIL = "email";
	private static String COL_SUBSCRIPTION_TYPE = "subscription_type";
	
	public long addSubscription(final Subscription subscription){
		
		KeyHolder keyHolder = new GeneratedKeyHolder();

		log.info("Inserting subscription..."+subscription.getName());
		
		final String sql = "INSERT INTO subscriptions (" + COL_NAME + "," + COL_MOBILE + "," 
				+ COL_EMAIL + "," + COL_SUBSCRIPTION_TYPE + ")"
				+ " VALUES (?, ?, ?, ?)";
		
		
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps =
			                connection.prepareStatement(sql, new String[] {"id"});
					ps.setString(1, subscription.getName());
					ps.setString(2, subscription.getMobile());
					ps.setString(3, subscription.getEmail());
					ps.setString(4, "Monthly");
				
					return ps;
				}
    	    },
    	    keyHolder);
    	
    	log.info("Inserting subscription;...done");
    	return (Long) keyHolder.getKey();
	}

}
