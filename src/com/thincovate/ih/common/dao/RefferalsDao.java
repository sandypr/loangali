package com.thincovate.ih.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thincovate.ih.common.utils.CommonUtils;
import com.thincovate.ih.models.Referrals;

@Service
public class RefferalsDao {
	
	static Logger log = Logger.getLogger(RefferalsDao.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String COL_REFERRED_NAME = "referred_name";
	private static String COL_REFERRED_MOBILE = "referred_mobile";
	private static String COL_REFERRED_EMAIL = "referred_email";
	private static String COL_CATEGORY_TYPE = "category_type";
	private static String COL_REFERRED_LOCATION = "referred_location";
	private static String COL_REFERRER_LOCATION = "referrer_location";

	private static String COL_REFERRER = "referrer";
	private static String COL_DATE_REFERRED = "date_referred";
	private static String COL_REFERRER_MOBILE = "referrer_mobile";
	private static String COL_REFERRER_EMAIL = "referrer_email";
	private static String COL_REFERRER_ADDRESS = "referrer_address";
	
	
	public void addReferrals(final Referrals referrals) {
		
		KeyHolder keyHolder = new GeneratedKeyHolder();

		log.info("Inserting referrals..."+referrals.getReferredName());
		
		final String sql = "INSERT INTO referrals (" + COL_REFERRED_NAME + "," + COL_REFERRED_MOBILE + "," 
				+ COL_REFERRED_EMAIL + "," + COL_REFERRED_LOCATION  + "," + COL_REFERRER + "," + COL_REFERRER_MOBILE + "," + COL_REFERRER_EMAIL + ","
				+ COL_REFERRER_LOCATION + "," + COL_REFERRER_ADDRESS + "," + COL_CATEGORY_TYPE + "," +   COL_DATE_REFERRED + ")"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)";
		
		
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps =
			                connection.prepareStatement(sql, new String[] {"id"});
					ps.setString(1, referrals.getReferredName());
					ps.setString(2, referrals.getReferredMobile());
					ps.setString(3, referrals.getReferredEmail());
					ps.setString(4, referrals.getReferredLocation());
					ps.setString(5, referrals.getReferrer());
					ps.setString(6, referrals.getReferrerMobile());
					ps.setString(7, referrals.getReferrerEmail());
					ps.setString(8, referrals.getReferrerLocation());
					ps.setString(9, referrals.getReferrerAddress());
					ps.setString(10, referrals.getCategoryType());
					ps.setString(11, CommonUtils.currentDateAsString());
					
					return ps;
				}
    	    },
    	    keyHolder);
    	
    	log.info("Inserting referrals done");
	}

}
