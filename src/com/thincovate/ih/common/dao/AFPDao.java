package com.thincovate.ih.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thincovate.ih.models.AFP;


@Service
public class AFPDao {
	
	static Logger log = Logger.getLogger(AFPDao.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String COL_NAME = "name";
	private static String COL_MOBILE = "mobile";
	private static String COL_EMAIL = "email";
	private static String COL_QNS = "qns";
	
	public long addAFP(final AFP afp){
		
		KeyHolder keyHolder = new GeneratedKeyHolder();

		log.info("Inserting AFP..."+afp.getName());
		
		final String sql = "INSERT INTO afp (" + COL_NAME + "," + COL_MOBILE + "," 
				+ COL_EMAIL + "," + COL_QNS + ")"
				+ " VALUES (?, ?, ?, ?)";
		
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps =
			                connection.prepareStatement(sql, new String[] {"id"});
					ps.setString(1, afp.getName());
					ps.setString(2, afp.getMobile());
					ps.setString(3, afp.getEmail());
					ps.setString(4, afp.getQns());
				
					return ps;
				}
    	    },
    	    keyHolder);
    	
    	log.info("Inserting AFP;...done");
    	return (Long) keyHolder.getKey();
	}
}
