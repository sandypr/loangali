package com.thincovate.ih.common.services;

import org.springframework.stereotype.Service;

import com.thincovate.ih.models.Referrals;

@Service
public interface ReferralsServiceI {
	
	 void addReferrals(Referrals referrals);

}
