package com.thincovate.ih.common.services;

import org.springframework.stereotype.Service;

import com.thincovate.ih.models.AFP;

@Service
public interface AFPServiceI {
	
	 void addAFP(AFP afp);

}
