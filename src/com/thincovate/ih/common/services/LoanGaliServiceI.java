/**
 * 
 */
package com.thincovate.ih.common.services;

import com.thincovate.ih.models.AFP;
import com.thincovate.ih.models.Feedback;

/**
 * @author Sandy
 *
 */
public interface LoanGaliServiceI {
	
	public void registerFeedback(Feedback feedback);
	public void registerAFPRequest(AFP afpReq);

}
