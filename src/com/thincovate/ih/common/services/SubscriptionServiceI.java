package com.thincovate.ih.common.services;

import org.springframework.stereotype.Service;

import com.thincovate.ih.models.Subscription;

@Service
public interface SubscriptionServiceI {

	long subscribe(Subscription subscription);
}
