package com.thincovate.ih.common.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.thincovate.ih.common.notifications.NotificationServiceI;
import com.thincovate.ih.common.services.AFPServiceI;
import com.thincovate.ih.common.services.LoanGaliServiceI;
import com.thincovate.ih.common.services.ReferralsServiceI;
import com.thincovate.ih.common.services.SubscriptionServiceI;
import com.thincovate.ih.models.AFP;
import com.thincovate.ih.models.Feedback;
import com.thincovate.ih.models.Referrals;
import com.thincovate.ih.models.Subscription;


@Controller
public class LoanGaliController {

	@Autowired
	private NotificationServiceI notifyService;

	@Autowired
	private ReferralsServiceI referralsService;
	
	@Autowired
	private LoanGaliServiceI loangaliService;
	
	@Autowired
	private AFPServiceI afpServiceI;
	
	@Autowired
	private SubscriptionServiceI subscriptionSvc;

	static Logger log = Logger.getLogger(LoanGaliController.class);

	@RequestMapping(value = "/homeLoan", method = RequestMethod.GET)
	public String homeloan() {
		return "HomeLoanForm";
	}

	@RequestMapping(value = "/personalLoan", method = RequestMethod.GET)
	public String personalLoan() {
		return "personalLoanForm";
	}

	@RequestMapping(value = "/lapLoan", method = RequestMethod.GET)
	public String lapLoan() {
		return "lapLoanForm";
	}
	
	@RequestMapping(value = "/eduLoan", method = RequestMethod.GET)
	public String eduLoan() {
		return "educationLoanForm";
	}

	@RequestMapping(value = "/widgetsView", method = RequestMethod.GET)
	public String widgetsView() {
		return "loanEligibilityWidgets";
	}

	/*
	 * @RequestMapping(value = "/feedback", method = RequestMethod.GET) public
	 * String feedback() { return "feedback"; }
	 */

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile() {
		return "profile";
	}

	@RequestMapping(value = "/dealOffers", method = RequestMethod.GET)
	public String dealOffers() {
		return "dealsOffers";
	}

	@RequestMapping(value = "/dealOffersPL", method = RequestMethod.GET)
	public String dealOffersPL() {
		return "dealsOffersPL";
	}

	@RequestMapping(value = "/dealOffersLAP", method = RequestMethod.GET)
	public String dealOffersLAP() {
		return "dealsOffersLAP";
	}

	@RequestMapping(value = "/taxImplications", method = RequestMethod.GET)
	public String taxImplications() {
		return "taxImplications";
	}

	@RequestMapping(value = "/taxImplicationsPL", method = RequestMethod.GET)
	public String taxImplicationsPL() {
		return "taxImplicationsPL";
	}

	@RequestMapping(value = "/taxImplicationsLAP", method = RequestMethod.GET)
	public String taxImplicationsLAP() {
		return "taxImplicationsLAP";
	}

	@RequestMapping(value = "/checkList", method = RequestMethod.GET)
	public String checkList() {
		return "checkList";
	}

	@RequestMapping(value = "/checkListPL", method = RequestMethod.GET)
	public String checkListPL() {
		return "checkListPL";
	}

	@RequestMapping(value = "/checkListLAP", method = RequestMethod.GET)
	public String checkListLAP() {
		return "checkListLAP";
	}

	@RequestMapping(value = "/faqs", method = RequestMethod.GET)
	public String faqs() {
		return "faqs";
	}

	@RequestMapping(value = "/faqsPL", method = RequestMethod.GET)
	public String faqsPL() {
		return "faqsPL";
	}

	@RequestMapping(value = "/faqsLAP", method = RequestMethod.GET)
	public String faqsLAP() {
		return "faqsLAP";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public ModelAndView feedback() {
		return new ModelAndView("feedback", "command", new Feedback());
	}

	@RequestMapping(value = "/submitFeedback", method = RequestMethod.POST, produces = "application/json")
	public String submitFeedback(@RequestBody Feedback feedback) {
		log.info("Entering submitFeedback: "+ feedback.getName());
		loangaliService.registerFeedback(feedback);
		return "feedbackTq";
	}
	
	@RequestMapping(value = "/referrals", method = RequestMethod.GET)
	public ModelAndView referrals() {
		return new ModelAndView("referrals", "command", new Referrals());
	}

	@RequestMapping(value = "/submitReferrals", method = RequestMethod.POST, produces = "application/json")
	public String submitReferrals(@RequestBody Referrals referrals) {
		log.info("Entering submitReferrals: "+ referrals.getCategoryType());
		referralsService.addReferrals(referrals);
		return "referrerTq";
	}
	
	@RequestMapping(value = "/submitAFP", method = RequestMethod.POST, produces = "application/json")
	public String submitAFP(@RequestBody AFP afp) {
		log.info("Entering submitAFP: "+ afp.getName());
		loangaliService.registerAFPRequest(afp);
		afpServiceI.addAFP(afp);
		return "referrerTq";
	}

	@RequestMapping(value = "/subscribe", method = RequestMethod.POST)
	public @ResponseBody long subscribe(@RequestBody Subscription subscription, ModelMap model) {
		log.info("Entering subscription: "+ subscription.getName());
		long id = subscriptionSvc.subscribe(subscription);
		log.info(" subscribed id: "+ id);
		return id;
	}

	@RequestMapping(value = "/error404", method = RequestMethod.GET)
	public String errorPage404() {
		return "errorGeneric404";
	}

	@RequestMapping(value = "/error500", method = RequestMethod.GET)
	public String errorPage500() {
		return "errorGeneric500";
	}
	
	@RequestMapping(value = "/finPlanning", method = RequestMethod.GET)
	public ModelAndView finPlanning() {
		return new ModelAndView("assistedFinancialPlanning");
	}
	
	@RequestMapping(value = "/tc", method = RequestMethod.GET)
	public ModelAndView termsAndConditions() {
		return new ModelAndView("tc");
	}

}
